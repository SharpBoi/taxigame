class MyMain{

    constructor(){
        console.log("MY: Main init");
        
        /**
         * @type {MyCanvasScaler}
         */
        this.canvasScaler;
        document.addEventListener("DOMContentLoaded", () => {
            // getting main canvas
            var canvasCheckerId = setInterval(()=>{
                var canvas = document.getElementById("#canvas");
                var gameCont = document.getElementById("gameContainer");
                console.log("MY: try to get canvas...");

                if (canvas !== undefined && gameCont !== undefined){
                    console.log("MY: canvas found !");

                    gameCont.style.position = "absolute";
                    gameCont.style.top = "50%";
                    gameCont.style.left = "50%";
                    gameCont.style.transform = "translateX(-50%) translateY(-50%)";

                    canvas.style.display = "absolute";
                    canvas.style.width = "100%";
                    canvas.style.height = "100%";

                    this.canvasScaler = new MyCanvasScaler(canvas, gameCont, 1920, 1080);
                    clearInterval(canvasCheckerId);
                }
            }, 
            250);
        });

        
    }
}