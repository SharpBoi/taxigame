/**
 * 
 * @param gameInstance 
 * @param {number} progress 
 * @param {MyMain} main 
 */
function UnityProgress(gameInstance, progress, main) {
    if (!gameInstance.Module)
        return;
    if (!gameInstance.logo) {
        gameInstance.logo = document.createElement("div");
        gameInstance.logo.className = "logo " + gameInstance.Module.splashScreenStyle;
        gameInstance.container.appendChild(gameInstance.logo);
    }
    if (gameInstance.loader === undefined) {

        // DECLARE ===================================
        var loader;
        var logoCont, logoImg;
        var progress;
        var barFull, barImgCont, barImg;
        var barEmpty;

        // header
        var header, btnFullscreen, imgFullscreen;

        // splash overlay
        var splashCont;

        // INIT ===================================
        
        // init splash overlay
        splashCont = document.createElement("div");
        splashCont.className = "splash-overlay";

        // init footer
        header = document.createElement("div");
        header.className = "header";
        {
            btnFullscreen = document.createElement("div");
            btnFullscreen.className = "fullscreen-button";
            // btnFullscreen.addEventListener("click", () => { gameInstance.SetFullscreen(1); });
            btnFullscreen.addEventListener("click", () => { main.canvasScaler.SetFullscreen(true); });
            {
                imgFullscreen = document.createElement("img");
                imgFullscreen.src = "TemplateData/fullscreen.png";
            }
        }
        
        // init loader progress components
        loader = document.createElement("div");
        loader.className = "loader";
        {
            logoCont = document.createElement("div");
            logoCont.className = "logo-cont";
            {
                logoImg = document.createElement("img");
                logoImg.className = "logo-img";
                logoImg.src = "TemplateData/Header.png";
            }
            
            progress = document.createElement("div");
            progress.className = "progress Dark";
            {
                barFull = document.createElement("div");
                barFull.className = "bar full";
                {
                    barImgCont = document.createElement("div");
                    barImgCont.className = "bar-img-cont";
                    {
                        barImg = document.createElement("img");
                        barImg.className = "bar-img";
                        barImg.src = "TemplateData/car_white.png";
                    }
                }
                
                barEmpty = document.createElement("div");
                barEmpty.className = "bar empty";
            }
        }

        // DOM HIERARCHY ===================================
        // loader, progress
        loader.appendChild(logoCont);
        loader.appendChild(progress);
        logoCont.appendChild(logoImg);
        progress.appendChild(barFull);
        progress.appendChild(barEmpty);
        barFull.appendChild(barImgCont);
        barImgCont.appendChild(barImg);
        gameInstance.container.appendChild(loader);

        // header
        btnFullscreen.appendChild(imgFullscreen);
        header.appendChild(btnFullscreen);
        gameInstance.container.appendChild(header);

        // splash overlay
        gameInstance.container.appendChild(splashCont);
        
        // JS HIERARCHY ===================================
        // header
        gameInstance.header = header;

        // loader, progress
        gameInstance.loader = loader;
        gameInstance.loader.logoCont = logoCont;
        gameInstance.loader.progress = progress;
        gameInstance.loader.progress.barFull = barFull;
        gameInstance.loader.progress.barEmpty = barEmpty;

        // splash
        gameInstance.splashCont = splashCont;
    }

    console.log("Loading progress: " + progress);
    gameInstance.loader.progress.barFull.style.width = (100 * progress) + "%";
    gameInstance.loader.progress.barEmpty.style.width = (100 * (1 - progress)) + "%";
    if (progress == 1) {

        gameInstance.SetFullscreen(1);
        
        setTimeout(() => {
            gameInstance.loader.style.display = "none";
        }, 2200);
    }
}
