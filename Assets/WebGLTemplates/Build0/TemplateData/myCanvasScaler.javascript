class MyCanvasScaler{

    /**
     * 
     * @param {HTMLCanvasElement} canvas
     * @param {HTMLDivElement} gameCont 
     * @param {number} rootWidth 
     * @param {number} rootHeight 
     */
    constructor(canvas, gameCont, rootWidth, rootHeight){
        
        this.canvas = canvas;
        this.gameCont = gameCont;
        
        // some calculations
        this.ar = rootWidth / rootHeight;

        this.scaleCanvas();
        window.addEventListener("resize", ()=>{
            this.scaleCanvas();
        });
    }
    
    scaleCanvas(){

        this.canvas.style.position = "absolute";
        this.canvas.style.width = "100%";
        this.canvas.style.height = "100%";

        var w = window.innerWidth;
        var h = window.innerHeight;

        if(w / h < this.ar){
            this.gameCont.style.width = w + "px";
            this.gameCont.style.height = (w * (1 / this.ar)) + "px";
        }
        else{

            this.gameCont.style.height = h + "px";
            this.gameCont.style.width = (h * this.ar) + "px";
        }
    }

    /**
     * 
     * @param {boolean} val 
     */
    SetFullscreen(val){
        if(val){
            gameInstance.SetFullscreen(1);

            this.canvas.width = screen.width;
            this.canvas.height = screen.height;
        }
        else {
            gameInstance.SetFullscreen(0);
        }
    }
}
