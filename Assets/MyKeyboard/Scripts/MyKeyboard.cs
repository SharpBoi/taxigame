﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[ExecuteInEditMode]
public class MyKeyboard : MonoBehaviour {

    #region FIELDS
    [Header("Float")]
    public float ShowedHeight;
    public float HidedHeight;

    [Header("Input field")]
    public Selectable InputField;

    [Header("Keys")]
    public List<GameObject> KeyRows = new List<GameObject>();
    public MyKeyboardKey KeyExample;

    public MyKeyboardKey BtnSpace;
    public MyKeyboardKey BtnRemove;
    public MyKeyboardKey BtnEnter;

    [Header("Keyboard generation")]
    public bool TESTGenKeyboard;
    public int KeysPerRow = 10;
    public int PerRowKeysCntDecreasement = 1;

    [Header("Events")]
    public UnityEvent OnEnter;

    private string keyChars = "qwertyuiopasdfghjklzxcvbnm";
    private List<MyKeyboardKey> qwertyKeys = new List<MyKeyboardKey>();
    private Selectable lastFocused;
    private Selectable focused;

    private Animation anim;
    #endregion

    void Start () {
        KeyExample.gameObject.SetActive(false);

        getKeys();
        setKeysEvents();

        BtnSpace.OnClick += Key_OnClick;
        BtnRemove.OnClick += Key_OnClick;
        BtnEnter.OnClick += Key_OnClick;

        anim = GetComponent<Animation>();
    }
	
	// Update is called once per frame
	void Update () {

        if (TESTGenKeyboard) {
            TESTGenKeyboard = false;

            genKeys();
        }
	}

    #region PROPS
    public string InputText {
        get {
            if (InputField.GetType() == typeof(InputField))
                return (InputField as InputField).text;

            else if (InputField.GetType() == typeof(TMPro.TMP_InputField))
                return (InputField as TMPro.TMP_InputField).text;

            return null;
        }
    }
    #endregion

    #region FUNCS
    private void genKeys () {
        
        clearKeyRows();
        qwertyKeys.Clear();

        int crntKeysCntDecreasement = 0;
        int crntKeysInRow = 0;
        int rowIndex = 0;

        for (int i = 0; i < keyChars.Length; i++) {

            GameObject keyInst = Instantiate(KeyExample.gameObject);
            keyInst.gameObject.SetActive(true);
            keyInst.transform.SetParent(KeyRows[rowIndex].transform);
            MyKeyboardKey key = keyInst.GetComponent<MyKeyboardKey>();

            key.MySetTitle(keyChars[i].ToString().ToUpper());
            key.MySetChar(keyChars[i].ToString());

            crntKeysInRow++;
            if(crntKeysInRow + crntKeysCntDecreasement == KeysPerRow) {
                crntKeysCntDecreasement += PerRowKeysCntDecreasement;
                rowIndex++;
                crntKeysInRow = 0;
            }
            qwertyKeys.Add(key);
        }

        setKeysEvents();
    }

    private void clearKeyRows () {
        for (int i = 0; i < qwertyKeys.Count; i++) {
            if (qwertyKeys[i] != null)
                DestroyImmediate(qwertyKeys[i].gameObject);
        }
    }
    private void getKeys () {
        qwertyKeys.Clear();
        for (int i = 0; i < KeyRows.Count; i++) {
            qwertyKeys.AddRange(KeyRows[i].GetComponentsInChildren<MyKeyboardKey>());
        }
    }
    private void setKeysEvents () {
        for (int i = 0; i < qwertyKeys.Count; i++)
            qwertyKeys[i].OnClick += Key_OnClick;
    }

    private Selectable getFocusedInput () {

        List<Selectable> selectables = Selectable.allSelectables;
        for (int i = 0; i < selectables.Count; i++) {

            if (selectables[i].GetType() == typeof(InputField))
                if ((selectables[i] as InputField).isFocused)
                    return selectables[i];

            if (selectables[i].GetType() == typeof(TMPro.TMP_InputField))
                if ((selectables[i] as TMPro.TMP_InputField).isFocused)
                    return selectables[i];
        }

        return null;
    }
    private void applyCharToFocused (string chr) {

        lastFocused = InputField;
        if (lastFocused != null) {
            if (lastFocused.GetType() == typeof(InputField))
                (lastFocused as InputField).text += chr;

            else if (lastFocused.GetType() == typeof(TMPro.TMP_InputField))
                (lastFocused as TMPro.TMP_InputField).text += chr;
        }
    }
    private void removeCharToFocused () {

        lastFocused = InputField;
        if (lastFocused != null) {
            if (lastFocused.GetType() == typeof(InputField)) {
                InputField field = (lastFocused as InputField);
                if (field.text.Length > 0)
                    field.text = field.text.Substring(0, field.text.Length - 1);
            }

            else if (lastFocused.GetType() == typeof(TMPro.TMP_InputField)) {
                TMPro.TMP_InputField field = (lastFocused as TMPro.TMP_InputField);
                if (field.text.Length > 0)
                    field.text = field.text.Substring(0, field.text.Length - 1);
            }
        }
    }

    private void Key_OnClick (MyKeyboardKey arg0) {
        Debug.Log(arg0.MyChar);

        if (qwertyKeys.Contains(arg0))
            applyCharToFocused(arg0.MyChar);
        else {

            if (arg0 == BtnSpace)
                applyCharToFocused(" ");

            else if (arg0 == BtnRemove)
                removeCharToFocused();

            else if (arg0 == BtnEnter)
                OnEnter.Invoke();
        }

        Debug.Log(lastFocused);
    }

    public void Show () {
        anim[anim.clip.name].speed = 1;
        Util.PlayAnimation(anim, anim.clip.name, 1, false);
    }
    public void Hide () {
        anim[anim.clip.name].speed = -1;
        Util.PlayAnimation(anim, anim.clip.name, 1, true);
    }
    #endregion
}
