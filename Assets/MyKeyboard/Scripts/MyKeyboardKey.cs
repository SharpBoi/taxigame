﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MyKeyboardKey : MonoBehaviour, IPointerClickHandler {

    #region FIELDS
    public string MyTitle;
    public string MyChar;

    public Text MyTitleText;

    public event UnityAction<MyKeyboardKey> OnClick;



    #endregion

    #region UNITY FUNCS
    private void Start () {
    }
    #endregion

    #region FUNCS
    public void MySetTitle (string title) {
        MyTitle = title;
        MyTitleText.text = title;
    }
    public void MySetChar(string chr) {
        MyChar = chr;
    }

    public void OnPointerDown (PointerEventData eventData) {


    }

    public void OnPointerClick (PointerEventData eventData) {
        OnClick?.Invoke(this);
    }
    #endregion
}
