﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehiclePathRider : MonoBehaviour {


    #region FIELDS


    private Vehicle veh;
    private PathBuilder crntPath;
    private float walkCoef = 0;
    private Vector3 walk;
    #endregion

    #region UNY FUNCS
    private void Start () {
        veh = GetComponent<Vehicle>();
    }

    private void FixedUpdate () {

        if (crntPath != null) {

            walkCoef += Time.deltaTime / 5;

            Vector3 pos, look;
            PathNode next;
            crntPath.GetWalkPoseByCoef(walkCoef, false, out pos, out look, out next);

            walk = pos;

            veh.SteerLookAt(pos);
            veh.ApplyDrive(1);
        }
    }

    private void OnDrawGizmos () {
        Gizmos.color = Color.red;

        Gizmos.DrawCube(walk, new Vector3(0.3f, 0.3f, 0.3f));
    }
    #endregion

    #region FUNCS
    public void StartRide (PathBuilder path) {
        crntPath = path;
        walkCoef = 1 - crntPath.GetCoefAtNode(crntPath.GetClosestNode(transform.position));
    }
    #endregion

    // OLD
    //   #region Fields
    //   public PathBuilder Path;
    //   public float MinRadiusToJumpNextNode = 1f;
    //   public bool RideOnStart = false;


    //   private Vehicle vehicle;
    //   private PathNode targetNode;
    //   private TileMapTarget tileTarget;
    //   #endregion

    //   #region Props
    //   public bool CanRide { get; set; }
    //   #endregion

    //   void Start () {
    //       vehicle = GetComponent<Vehicle>();
    //       tileTarget = GetComponent<TileMapTarget>();

    //       targetNode = Path.GetClosestNode(transform.position);

    //       if (RideOnStart)
    //           CanRide = true;

    //       vehicle.MaxSpeed = 15;
    //}

    //   private void FixedUpdate()
    //   {
    //       if (CanRide)
    //       {
    //           vehicle.MaxSpeed = Vector3.Distance(transform.position, targetNode.transform.position);
    //           if (vehicle.MaxSpeed < 5)
    //               vehicle.MaxSpeed = 5;
    //           vehicle.ApplyDrive(1);
    //           vehicle.SteerLookAt(targetNode.transform.position);

    //           if (Vector3.Distance(transform.position, targetNode.transform.position) <= MinRadiusToJumpNextNode)
    //               targetNode = targetNode.NextNode;
    //       }

    //       if (tileTarget.CurrentTile.Self != null)
    //           if (tileTarget.CurrentTile.Type == TileType.Crossroad)
    //           {
    //               CrossroadController crossroad = tileTarget.CurrentTile.Self.GetComponent<CrossroadController>();
    //               if (crossroad.IsTrafficOnRoad)
    //               {
    //                   CanRide = false;
    //                   vehicle.ToggleOnBrake();
    //               }
    //               else
    //               {
    //                   CanRide = true;
    //                   vehicle.ToggleOffBrake();
    //               }
    //           }
    //   }

    //   public void StartRide()
    //   {
    //       CanRide = true;
    //   }
    //   public void StartRide(PathBuilder path)
    //   {
    //       StartRide();

    //       this.Path = path;
    //       targetNode = path.GetClosestNode(transform.position);
    //   }
    //   public void StopRide()
    //   {
    //       CanRide = false;
    //   }

    //   private void OnDrawGizmos()
    //   {
    //       if (targetNode != null)
    //       {
    //           Gizmos.DrawLine(targetNode.transform.position, targetNode.transform.position + Vector3.up);
    //           Gizmos.DrawLine(transform.position, targetNode.transform.position + Vector3.up);
    //       }
    //   }

    //   private void OnCollisionEnter(Collision collision)
    //   {
    //       CanRide = false;
    //       vehicle.ToggleOnBrake();
    //   }
}
