﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class Vehicle : MonoBehaviour {

    // True vehicle controll
    #region Fields
    // pubs
    [Header("Wheels")]
    public List<WheelCollider> SteeringColliderWheels = new List<WheelCollider>();
    public List<WheelCollider> DriveColliderWheels = new List<WheelCollider>();

    [Header("Doors")]
    public Transform LeftDoor;
    public Transform RightDoor;

    [Header("Stabilization")]
    public float AntiRoll = 3400;

    [Header("Collision")]
    public float CollisionTimeoutSEC = 1;

    [Header("Steering")]
    public float SteeringSpeed = 1;
    public float SteeringZeroingSpeed = 1;
    public float MaxSteerAngle = 45;

    [Header("Drive")]
    public AnimationCurve DriveCurve;
    public float DriveTorque = 40000;
    public float DriveZeroingSpeed = 4;
    public float MaxSpeed = 25;
    public float AirTimeSEC = 2;

    [Header("Break")]
    public bool BrakeFrontWheels = true;
    public float BrakeForce = 10000;

    [Header("Fading materials")]
    public List<MeshRenderer> RendersWhereSearch = new List<MeshRenderer>();
    public List<Material> MatsToFade = new List<Material>();

    [Header("Tire track")]
    public float TrackWidth = 1f / 3;
    public float TrackPointsDistance = 1;
    public int TrackCornerVerts = 4;
    public int TrackMaxVerts = 20;
    public Material TrackMaterial;
    public Gradient TrackColor;

    [Header("Fuel stuff")]
    public float MaxFuel = 1000;
    public float FuelIntakeBySpeedCoeff = 0.01f;
    public float TimeFuelIntake = 0.1f;
    public float HitImpulse = 5000;
    public float HitFuelIntake = 10;
    public float RollOverFuelIntake = 0.1f;
    public bool CanIntakeFuel = true;

    [Header("Sounds")]
    public VehicleSoundPlayer sound;

    [SerializeField]
    [MinMax(0, 90)]
    MinMaxPair DriftMinMax = new MinMaxPair();

    [Header("Drive cam")]
    [MinMax(0f, 179f)]
    public MinMaxPair Fov = new MinMaxPair(60, 70);

    [Header("Passangers")]
    public Transform BoardingPoint;
    public int MaxPassangersCount;
    public PathBuilder WalkPath;

    [Header("Events")]
    public UnityEvent OnHit;

    public UnityEvent OnBoarded;
    public UnityEvent OnUnboarded;

    public UnityEvent OnLandOn4Wheels;


    // privs

    private Rigidbody rigid;
    private List<WheelCollider> allColliderWheels = new List<WheelCollider>();

    private List<Passanger> crntPassangers = new List<Passanger>();
    private List<Passanger> unboardedPassangers = new List<Passanger>();

    private float crntRolloverTime = 0;
    private const float sqrt2 = 1.414213562373095f;

    private Animator anim;

    private Vector3 crntVel;
    private Vector3 prevVel;
    private Vector3 deltaVel;
    private float driveChangeMult = 1f;

    private bool isHit = true;
    private float airTime = 0;

    private bool canDetectCollision = true;
    #endregion

    #region Props
    public Rigidbody Rigid { get { return rigid; } }

    public bool IsOnGround { get; private set; }

    public float CurrentSpeed { get { return rigid != null ? rigid.velocity.magnitude : 0; } }
    public float CurrentSignedSpeed {
        get {
            if ((transform.forward + rigid.velocity.normalized).magnitude < sqrt2)
                return rigid.velocity.magnitude * -1;
            else
                return rigid.velocity.magnitude;

            //if ((transform.forward * Mathf.Sign(CurrentDrive) + rigid.velocity.normalized).magnitude < sqrt2)
            //    return rigid.velocity.magnitude * -1;
            //else
            //    return rigid.velocity.magnitude;
        }
    }
    public Vector3 CurrentVelocity { get { return rigid.velocity; } set { rigid.velocity = value; } }
    public Vector3 DeltaVelocity { get { return deltaVel; } }
    public float CurrentDrive { get; private set; }
    public float DrivedDistance { get; private set; }
    public bool IsBrakeToggled { get; private set; }

    public float CurrentSteer { get; private set; }

    public float CurrentFuel { get; private set; }
    public float CurrentFuelNormalized { get { return CurrentFuel / MaxFuel; } }
    public bool HasFuel { get { return CurrentFuel > 0; } }
    public bool IsCanIntakeFuel { get { return CanIntakeFuel; } set { CanIntakeFuel = value; } }

    public int PassangersCount { get { return crntPassangers.Count; } }
    public bool CanBoardPassangers { get { return crntPassangers.Count < MaxPassangersCount; } }
    public Passanger[] Passangers { get { return crntPassangers.ToArray(); } }
    public Passanger[] UnboardedPassangers { get { return unboardedPassangers.ToArray(); } }
    public bool IsBoardingNow { get; set; }

    public Side OpenedDoorSide { get; private set; }
    #endregion

    // passanger stuff
    /// <summary>
    /// Совершает попытку посадки пассажира
    /// </summary>
    /// <param name="passanger"></param>
    /// <returns>Возвращает результат операции</returns>
    public bool TryBoardPassanger (Passanger passanger) {
        if (crntPassangers.Count < MaxPassangersCount) {
            crntPassangers.Add(passanger);

            OnBoarded.Invoke();

            return true;
        }
        return false;
    }
    /// <summary>
    /// Совершает попытку высадки пассажира
    /// </summary>
    /// <returns>Возвращает высаженного пассажира</returns>
    public Passanger TryUnboardPassanger () {
        if (crntPassangers.Count > 0) {
            Passanger psngr = crntPassangers[crntPassangers.Count - 1];
            crntPassangers.Remove(psngr);
            unboardedPassangers.Add(psngr);

            OnUnboarded.Invoke();

            return psngr;
        }

        return null;
    }
    private void UpdatePassangersPosition () {
        for (int i = 0; i < crntPassangers.Count; i++)
            if (crntPassangers[i].IsBoarded)
                crntPassangers[i].transform.position = transform.position;
    }

    Vector3 dir, vel;
    // Use this for initialization
    void Start () {
        // rigid
        rigid = GetComponent<Rigidbody>();

        // fill all colliders
        allColliderWheels.AddRange(SteeringColliderWheels);
        allColliderWheels.AddRange(DriveColliderWheels);

        // tire tracks
        //wheelsPoints = new List<Transform>[allColliderWheels.Count];
        //tireTracks = new LineRenderer[allColliderWheels.Count];
        //for (int i = 0; i < tireTracks.Length; i++) {
        //    GameObject trackLine = new GameObject("track");
        //    trackLine.transform.SetParent(allColliderWheels[i].transform);
        //    trackLine.transform.localPosition = Vector3.zero;
        //    trackLine.transform.localRotation = Quaternion.identity;
        //    trackLine.transform.localScale = Vector3.one;

        //    tireTracks[i] = trackLine.AddComponent<LineRenderer>();
        //    tireTracks[i].alignment = LineAlignment.TransformZ;
        //    tireTracks[i].widthMultiplier = TrackWidth;
        //    tireTracks[i].numCornerVertices = TrackCornerVerts;
        //    tireTracks[i].sharedMaterial = TrackMaterial;
        //    tireTracks[i].textureMode = LineTextureMode.Tile;
        //    tireTracks[i].colorGradient = TrackColor;

        //    wheelsPoints[i] = new List<Transform>();
        //}

        // config wheel update rate
        for (int i = 0; i < allColliderWheels.Count; i++)
            allColliderWheels[i].ConfigureVehicleSubsteps(MaxSpeed / 2, (int)MaxSpeed / 2, (int)MaxSpeed / 2);

        CurrentFuel = MaxFuel;

        anim = GetComponent<Animator>();

        OpenedDoorSide = Side.None;
    }
    private void FixedUpdate () {

        UpdatePassangersPosition();

        DrivedDistance += Mathf.Round(rigid.velocity.magnitude) / 50;

        prevVel = crntVel;
        crntVel = rigid.velocity;
        deltaVel = crntVel - prevVel;

        UpdateStabilization();

        UpdateGodMode();


        //sound.PitchEngine(CurrentSpeed / MaxSpeed + 1);
    }
    private void LateUpdate () {
        UpdateFuelIntaking();

        UpdateSpeedLimit();

        UpdateGroundCheck();

        sound.PitchEngine(CurrentSpeed / MaxSpeed);

        updateDriftRecognition();
        //if (IsBrakeToggled && IsOnGround)
        //    sound.PlayBrake(CurrentSpeed / MaxSpeed);
        //else
        //    sound.StopBrake();
    }

    // vehicle controlling stuff
    public void ToggleOnBrake () {
        if (IsBrakeToggled == false) {
            IsBrakeToggled = true;

            CurrentDrive = 0;
            for (int i = 0; i < DriveColliderWheels.Count; i++)
                DriveColliderWheels[i].brakeTorque = BrakeForce;

            if (BrakeFrontWheels)
                for (int i = 0; i < SteeringColliderWheels.Count; i++)
                    SteeringColliderWheels[i].brakeTorque = BrakeForce;

        }
    }
    public void ToggleOffBrake () {
        if (IsBrakeToggled) {
            IsBrakeToggled = false;

            for (int i = 0; i < DriveColliderWheels.Count; i++)
                DriveColliderWheels[i].brakeTorque = 0;

            if (BrakeFrontWheels)
                for (int i = 0; i < SteeringColliderWheels.Count; i++)
                    SteeringColliderWheels[i].brakeTorque = 0;
        }
    }

    public void ApplyFuel (float value) {
        CurrentFuel += value;
        CurrentFuel = Mathf.Clamp(CurrentFuel, 0, MaxFuel);
    }
    public void ApplySteering (int direction) {
        for (int i = 0; i < SteeringColliderWheels.Count; i++) {
            CurrentSteer = Mathf.Lerp(CurrentSteer, MaxSteerAngle * direction, Time.deltaTime * SteeringSpeed);
            SteeringColliderWheels[i].steerAngle = CurrentSteer;
        }
    }
    public void Steer (float degree) {
        degree = Mathf.Clamp(degree, -MaxSteerAngle, MaxSteerAngle);
        CurrentSteer = Mathf.Lerp(CurrentSteer, degree, Time.deltaTime * SteeringSpeed);

        for (int i = 0; i < SteeringColliderWheels.Count; i++)
            SteeringColliderWheels[i].steerAngle = degree;
    }
    public float SteerLookAt (Vector3 target) {
        Vector3 dir = (target - transform.position).normalized;
        float angle = Quaternion.LookRotation(dir).eulerAngles.y - transform.rotation.eulerAngles.y;
        //if (angle > MaxSteerAngle)
        //    angle = MaxSteerAngle;
        //else if (angle < -MaxSteerAngle)
        //    angle = -MaxSteerAngle;
        for (int i = 0; i < SteeringColliderWheels.Count; i++)
            SteeringColliderWheels[i].steerAngle = angle;

        return angle;
    }
    public void ApplyDrive (int direction) {
        if (!IsBrakeToggled) {
            direction = (int)Mathf.Sign(direction);

            if (Mathf.Sign(CurrentDrive) != Mathf.Sign(direction))
                CurrentDrive = 0;

            CurrentDrive += Time.deltaTime * direction;
            CurrentDrive = Mathf.Clamp(CurrentDrive, -1, 1);

            SetDrive(CurrentDrive);
        }
    }
    public void SetDrive (float normValue) {
        CurrentDrive = normValue;

        float torque = DriveCurve.Evaluate(Mathf.Abs(normValue)) * DriveTorque;
        torque *= Mathf.Sign(normValue);

        for (int i = 0; i < DriveColliderWheels.Count; i++)
            DriveColliderWheels[i].motorTorque = torque * driveChangeMult;
    }

    public void UpdateDriveZeroing () {
        CurrentDrive = Mathf.Lerp(CurrentDrive, 0, Time.deltaTime * DriveZeroingSpeed);
        for (int i = 0; i < DriveColliderWheels.Count; i++) {
            DriveColliderWheels[i].motorTorque = CurrentDrive;
        }


    }
    public void UpdateSteeringZeroing () {
        for (int i = 0; i < SteeringColliderWheels.Count; i++) {
            CurrentSteer = Mathf.Lerp(CurrentSteer, 0, Time.deltaTime * SteeringZeroingSpeed);
            SteeringColliderWheels[i].steerAngle = CurrentSteer;
        }
    }

    // checkers stuff
    private void updateDriftRecognition () {

        if (Mathf.Round(rigid.velocity.magnitude) > 0) {
            dir = transform.forward * Mathf.Sign(CurrentDrive);
            vel = rigid.velocity;
            float angle = Vector3.Angle(dir, vel);

            if (IsBrakeToggled) {
                sound.PlayBrake(CurrentSpeed / MaxSpeed);
            }
            else {
                if (IsOnGround)
                    sound.PlayBrake((angle - DriftMinMax.Min) / (DriftMinMax.Max - DriftMinMax.Min));
                else
                    sound.StopBrake();
            }
        }
        else
            sound.StopBrake();
    }
    private void UpdateGroundCheck () {

        IsOnGround = false;
        for (int i = 0; i < allColliderWheels.Count; i++) {
            if (allColliderWheels[i].isGrounded) {
                IsOnGround = true;

                if (airTime > AirTimeSEC) {
                    Debug.Log("Grounded " + airTime);
                    isHit = false;
                    OnLandOn4Wheels.Invoke();
                }

                airTime = 0;
                break;
            }
        }

        if (!IsOnGround)
            airTime += Time.deltaTime;
    }
    private void UpdateStabilization () {
        WheelHit hit;
        float travelL = 1;
        float travelR = 1;

        WheelCollider WheelR = SteeringColliderWheels[0];
        WheelCollider WheelL = SteeringColliderWheels[1];

        bool groundedL = WheelL.GetGroundHit(out hit);
        if (groundedL)
            travelL = (-WheelL.transform.InverseTransformPoint(hit.point).y - WheelL.radius) / WheelL.suspensionDistance;

        bool groundedR = WheelR.GetGroundHit(out hit);
        if (groundedR)
            travelR = (-WheelR.transform.InverseTransformPoint(hit.point).y - WheelR.radius) / WheelR.suspensionDistance;

        float antiRollForce = (travelL - travelR) * AntiRoll;

        if (groundedL)
            rigid.AddForceAtPosition(WheelL.transform.up * -antiRollForce,
                   WheelL.transform.position);
        if (groundedR)
            rigid.AddForceAtPosition(WheelR.transform.up * antiRollForce,
                   WheelR.transform.position);
    }

    private void UpdateSpeedLimit () {
        if (rigid.velocity.magnitude > MaxSpeed)
            rigid.velocity = rigid.velocity.normalized * MaxSpeed;
    }

    private void UpdateFuelIntaking () {

        if (CanIntakeFuel) {
            CurrentFuel -= TimeFuelIntake;
            CurrentFuel -= CurrentSpeed * FuelIntakeBySpeedCoeff;
            if (CurrentFuel < 0)
                CurrentFuel = 0;
        }
    }

    // doors stuff
    private void OnDoorBeginOpen () {
        sound.PlayDoorOpen();
    }
    private void OnDoorOpened (Side side) {
        if (side == Side.Left && OpenedDoorSide == Side.Right)
            OpenedDoorSide = Side.LeftRight;
        else if (side == Side.Right && OpenedDoorSide == Side.Left)
            OpenedDoorSide = Side.LeftRight;
        else
            OpenedDoorSide = side;
    }
    private void OnDoorClosed (Side side) {
        if (side == Side.Left && OpenedDoorSide == Side.LeftRight)
            OpenedDoorSide = Side.Right;
        else if (side == Side.Right && OpenedDoorSide == Side.LeftRight)
            OpenedDoorSide = Side.Left;
        else
            OpenedDoorSide = Side.None;

        sound.PlayDoorClose();
    }
    public void SetDoorState (Side side, bool opened) {
        if (Mathf.Round(CurrentSpeed) == 0) {
            if (side == Side.Left)
                anim.SetBool("ldoor_open", opened);
            else if (side == Side.Right)
                anim.SetBool("rdoor_open", opened);
        }
    }
    public bool GetDoorState (Side side) {
        if (side == Side.Left)
            return anim.GetBool("ldoor_open");
        if (side == Side.Right)
            return anim.GetBool("rdoor_open");

        return false;
    }
    public void CloseDoor (Side side) {
        SetDoorState(side, false);
    }
    public void CloseAllDoors () {
        CloseDoor(Side.Left);
        CloseDoor(Side.Right);
    }
    public void OpenDoor (Side side) {
        SetDoorState(side, true);
    }
    public void SwitchDoor (Side side) {
        SetDoorState(side, !GetDoorState(side));
    }

    // cheat stuff
    bool isGodMode = false;
    public void EnableGodMode () {
        if (!isGodMode) {
            isGodMode = true;
            setGodMode(true);

            Debug.Log("god mode enabled");
        }
    }
    public void DisableGodMode () {
        if (isGodMode) {
            isGodMode = false;
            setGodMode(false);

            Debug.Log("god mode disabled");
        }
    }
    public void SwitchGodMode () {
        if (isGodMode)
            DisableGodMode();
        else
            EnableGodMode();
    }
    private void setGodMode (bool val) {
        float mult = 0;
        if (val) mult = 10;
        else mult = 1 / 10;

        rigid.mass *= mult;
        DriveTorque *= mult;

        AntiRoll *= mult;

        for (int i = 0; i < allColliderWheels.Count; i++) {
            JointSpring spring = allColliderWheels[i].suspensionSpring;
            spring.spring *= mult;
            allColliderWheels[i].suspensionSpring = spring;
        }
    }
    public void UpdateGodMode () {
        if (isGodMode) {
            transform.localRotation = Quaternion.Euler(
                0,
                transform.rotation.eulerAngles.y,
                transform.rotation.eulerAngles.z);
        }
    }

    // bonus stuff
    bool speedBoostEnabled = false;
    public void EnableSpeedBoost (float timeSEC) {
        StartCoroutine(countSpeedBoost(timeSEC));
        StartCoroutine(countDeloreanPoseBySpeed());
    }
    private IEnumerator countSpeedBoost (float timeSEC) {
        speedBoostEnabled = true;

        float speedChange = 1.5f;
        MaxSpeed *= speedChange;
        driveChangeMult *= speedChange;
        FuelIntakeBySpeedCoeff /= speedChange;

        StartCoroutine(countDeloreanWheelPosing(45, false, false));

        float time = 0;
        while (time < timeSEC) {
            time += Time.deltaTime;

            Camera.main.fieldOfView = Fov.Min + (Fov.Max - Fov.Min) * (CurrentSpeed / MaxSpeed);

            yield return 0;
        }

        Camera.main.fieldOfView = Fov.Min;

        speedBoostEnabled = false;
        StartCoroutine(countDeloreanWheelPosing(0, false, true));

        MaxSpeed /= speedChange;
        driveChangeMult /= speedChange;
        FuelIntakeBySpeedCoeff *= speedChange;
    }
    private IEnumerator countDeloreanPoseBySpeed () {
        MeshWheelPoser mwp = GetComponent<MeshWheelPoser>();

        while (speedBoostEnabled) {
            for (int i = 0; i < mwp.Pairs.Count; i++) {
                if (mwp.Pairs[i].Side == Side.Left) {
                    mwp.Pairs[i].RotOffset.y = -60 * (CurrentSpeed / MaxSpeed);
                }

                else if (mwp.Pairs[i].Side == Side.Right) {
                    mwp.Pairs[i].RotOffset.y = 60 * (CurrentSpeed / MaxSpeed);
                }
            }

            yield return 0;
        }

        if (speedBoostEnabled == false)
            while (Mathf.Round(mwp.Pairs[0].RotOffset.y) != 0) {
                for (int i = 0; i < mwp.Pairs.Count; i++) {
                    mwp.Pairs[i].RotOffset.y = Mathf.Lerp(mwp.Pairs[i].RotOffset.y, 0, Time.deltaTime * 10);
                }

                yield return 0;
            }
    }
    private IEnumerator countDeloreanWheelPosing (float targetAngle, bool attachAtBegin, bool attachAtEnd) {
        MeshWheelPoser mwp = GetComponent<MeshWheelPoser>();

        for (int i = 0; i < mwp.Pairs.Count; i++) {
            mwp.Pairs[i].Attach = attachAtBegin;

            if (attachAtBegin == false)
                mwp.Pairs[i].DeloreanPS.Play(true);
        }

        while (Mathf.Round(Mathf.Abs(mwp.Pairs[0].RotOffset.z)) != Mathf.Round(Mathf.Abs(targetAngle))) {
            for (int i = 0; i < mwp.Pairs.Count; i++) {
                if (mwp.Pairs[i].Side == Side.Left) {
                    mwp.Pairs[i].RotOffset.z = Mathf.Lerp(mwp.Pairs[i].RotOffset.z, targetAngle, Time.deltaTime * 10);
                }

                else if (mwp.Pairs[i].Side == Side.Right) {
                    mwp.Pairs[i].RotOffset.z = Mathf.Lerp(mwp.Pairs[i].RotOffset.z, -targetAngle, Time.deltaTime * 10);
                }
            }

            yield return 0;
        }

        for (int i = 0; i < mwp.Pairs.Count; i++) {
            //mwp.Pairs[i].Attach = attachAtEnd;

            if (attachAtEnd)
                mwp.Pairs[i].DeloreanPS.Stop(true);
        }
    }

    public void EnableInvincibility (float timeSEC) {
        StartCoroutine(countInvincibility(timeSEC));
    }
    private IEnumerator countInvincibility (float timeSEC) {

        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerVehicle"), LayerMask.NameToLayer("BotVehicle"), true);
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerVehicle"), LayerMask.NameToLayer("Vehicle"), true);
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerVehicle"), LayerMask.NameToLayer("BrokenPiece"), true);
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerVehicle"), LayerMask.NameToLayer("BotRoad"), false);
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerVehicle"), LayerMask.NameToLayer("Prop"), true);
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerVehicle"), LayerMask.NameToLayer("Camera"), true);
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerVehicle"), LayerMask.NameToLayer("Passanger"), true);
        VehicleCam.all[0].IgnoreLayers =
            (1 << LayerMask.NameToLayer("BotVehicle")) |
            (1 << LayerMask.NameToLayer("Vehicle")) |
            (1 << LayerMask.NameToLayer("BrokePiece")) |
            (1 << LayerMask.NameToLayer("Prop"));

        // mat alpha set
        List<Material> found = new List<Material>();
        for (int i = 0; i < RendersWhereSearch.Count; i++) {
            for (int j = 0; j < RendersWhereSearch[i].materials.Length; j++) {
                for (int k = 0; k < MatsToFade.Count; k++) {

                    if (RendersWhereSearch[i].materials[j].name == MatsToFade[k].name + " (Instance)") {
                        found.Add(RendersWhereSearch[i].materials[j]);
                    }
                }
            }
        }

        StartCoroutine(countMatAlpha(1, 0, 1f / 2, found.ToArray()));

        canDetectCollision = false;

        float time = 0;
        while (time < timeSEC) {
            time += Time.deltaTime;

            yield return 0;
        }

        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerVehicle"), LayerMask.NameToLayer("BotVehicle"), false);
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerVehicle"), LayerMask.NameToLayer("Vehicle"), false);
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerVehicle"), LayerMask.NameToLayer("BrokenPiece"), false);
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerVehicle"), LayerMask.NameToLayer("BotRoad"), true);
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerVehicle"), LayerMask.NameToLayer("Prop"), false);
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerVehicle"), LayerMask.NameToLayer("Camera"), false);
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerVehicle"), LayerMask.NameToLayer("Passanger"), false);
        VehicleCam.all[0].IgnoreLayers = 0;

        StartCoroutine(countMatAlpha(0, 1, 1f / 2, found.ToArray()));

        canDetectCollision = true;
    }
    private IEnumerator countMatAlpha (float from, float to, float timeSEC, params Material[] mats) {

        float a = from;
        while (Mathf.Round(a * 100) != Mathf.Round(to * 100)) {
            a = Mathf.Lerp(a, to, Time.deltaTime * (1 / timeSEC));

            for (int i = 0; i < mats.Length; i++)
                mats[i].SetFloat("_Alpha", a);
            yield return 0;
        }
    }

    private void OnCollisionEnter (Collision collision) {
        sound.PlayBump(CurrentSpeed / MaxSpeed);

        if (collision.gameObject.GetComponentsInChildren<Passanger>().Length != 0)
            isHit = false;

        if (isHit && canDetectCollision) {
            if (collision.impulse.magnitude >= HitImpulse) {
                OnHit.Invoke();
                CurrentFuel -= HitFuelIntake;

                StartCoroutine(countdownCollisionTimeout());
            }
        }
        isHit = true;
        airTime = 0;
    }
    private IEnumerator countdownCollisionTimeout () {
        canDetectCollision = false;
        float time = 0;
        while (time < CollisionTimeoutSEC) {
            time += Time.deltaTime;
            yield return 0;
        }

        canDetectCollision = true;
    }

    private void OnCollisionStay (Collision collision) {
        IsOnGround = true;
        airTime = 0;
    }

    private void OnDrawGizmos () {
        if (BoardingPoint != null)
            Gizmos.DrawSphere(BoardingPoint.transform.position, 0.2f);

        Gizmos.color = Color.white;
        Gizmos.DrawLine(transform.position, transform.position + dir.normalized * 3);
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + vel.normalized * 3);

        //if (wheelsPoints != null)
        //    for (int i = 0; i < wheelsPoints.Length; i++)
        //        for (int j = 0; j < wheelsPoints[i].Count; j++)
        //            Gizmos.DrawSphere(wheelsPoints[i][j], 0.3f);
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(Vehicle))]
public class PlayerCarControllerInspector : Editor {
    Vehicle tgt;

    public override void OnInspectorGUI () {
        DrawDefaultInspector();
        tgt = target as Vehicle;

        PrintVehicleData();
    }

    private void PrintVehicleData () {
        GUILayout.BeginVertical(EditorStyles.helpBox);

        EditorGUILayout.LabelField("Vehicle data", EditorStyles.boldLabel);

        EditorGUILayout.LabelField("Drive: " + tgt.CurrentDrive);
        EditorGUILayout.LabelField("Steer: " + tgt.CurrentSteer);
        EditorGUILayout.LabelField("Veloctiy: " + tgt.CurrentSpeed);

        GUILayout.EndVertical();
    }
}
#endif