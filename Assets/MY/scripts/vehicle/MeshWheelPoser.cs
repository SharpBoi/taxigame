﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshWheelPoser : MonoBehaviour {

    [Header("Wheels")]
    public List<WheelMeshPair> Pairs = new List<WheelMeshPair>();

    private void FixedUpdate()
    {
        for (int i = 0; i < Pairs.Count; i++) {
            Pairs[i].UpdatePose();
        }
    }
}

[System.Serializable]
public class WheelMeshPair {
    public Transform Mesh;
    public WheelCollider Collider;
    public ParticleSystem DeloreanPS;
    public bool Attach = true;
    public Side Side = Side.None;

    public Vector3 RotOffset;

    public void UpdatePose () {

        Mesh.localRotation = Quaternion.Euler(RotOffset);

        if (Attach) {
            Vector3 pos, rot;
            Quaternion quat;

            Collider.GetWorldPose(out pos, out quat);
            rot = quat.eulerAngles;

            Mesh.position = Vector3.Lerp(Mesh.position, pos, Time.deltaTime * 10);
            Mesh.rotation = Quaternion.Euler(rot.x, rot.y, rot.z);
        }
    }
}