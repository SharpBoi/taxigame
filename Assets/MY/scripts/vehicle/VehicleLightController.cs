﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Vehicle))]
public class VehicleLightController : MonoBehaviour {

    #region Fields
    [Header("Light")]
    public Color BackLightDefaultColor;
    public Color BackLightBrakeColor;
    public MeshRenderer BackLightMesh;
    public Material BackLightMaterial;


    private Vehicle vehicle;
    #endregion

    #region Props
    public bool IsLighting { get; private set; }
    #endregion

    void Start () {

        vehicle = GetComponent<Vehicle>();

        // brakelight mat stuff
        for (int i = 0; i < BackLightMesh.sharedMaterials.Length; i++)
            if (BackLightMesh.sharedMaterials[i].name == BackLightMaterial.name)
            {
                BackLightMaterial = BackLightMesh.materials[i];
                break;
            }
        
        setColor(BackLightMaterial, BackLightDefaultColor);
    }

    private void FixedUpdate()
    {
        if (vehicle.IsBrakeToggled || vehicle.CurrentDrive < 0)
            TurnOnBackLight();
        else
            TurnOffBackLight();
    }

    public void TurnOnBackLight()
    {
        if (IsLighting == false)
        {
            IsLighting = true;
            
            setColor(BackLightMaterial, BackLightBrakeColor);
        }
    }
    public void TurnOffBackLight()
    {
        if (IsLighting == true)
        {
            IsLighting = false;
            
            setColor(BackLightMaterial, BackLightDefaultColor);
        }
    }

    private void setColor (Material mat, Color col) {

        float a = mat.color.a;
        Color c = col;
        c.a = a;
        mat.color = c;
    }
}
