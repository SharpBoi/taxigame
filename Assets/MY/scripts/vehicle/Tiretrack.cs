﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class Tiretrack : MonoBehaviour {

    #region FIELDS


    private List<Transform> pnts = new List<Transform>();
    private LineRenderer line;

    private TiretrackManager ttm;

    private GameObject parent;
    private Vector3 prevPos;
    private Vector3 prevRot;
    private Vector3 prevScl;
    #endregion

    #region PROPS
    public List<Transform> Points { get { return pnts; } }
    public Transform LastPoint { get { return pnts.Count > 0 ? pnts[pnts.Count - 1] : null; } }
    public GameObject Parent { get { return parent; } }
    public float Length { get; private set; }
    #endregion

    void Start () {
        init();
    }
    private void init () {
        if (line == null) {
            line = GetComponent<LineRenderer>();
            line.alignment = LineAlignment.TransformZ;
        }
    }

    private void FixedUpdate () {
        // check if parent transform changed, then update line pos
        if (parent != null) {

            if (prevPos != parent.transform.position ||
                prevRot != parent.transform.rotation.eulerAngles ||
                prevScl != parent.transform.lossyScale) {

                UpdateLinePos();
            }

            prevPos = parent.transform.position;
            prevRot = parent.transform.rotation.eulerAngles;
            prevScl = parent.transform.lossyScale;
        }
    }

    public void AddPoint (Vector3 pnt, Vector3 surfaceNorm, float alphaScale, GameObject _parent, TiretrackManager _ttm) {
        init();

        if (parent == null) {
            parent = _parent;

            transform.SetParent(parent.transform);
            transform.position = pnt;
        }
        else if (parent != _parent)
            throw new System.Exception("One Tiretrack only for one GameObject !!!");

        if (ttm == null)
            ttm = _ttm;
        else if (ttm != _ttm)
            throw new System.Exception("Tiretrack already has parent TiretrackManager");

        GameObject anchor = new GameObject();
        anchor.transform.SetParent(transform);
        anchor.transform.position = pnt;
        anchor.name = "anchor (" + pnts.Count + ")";

        line.positionCount++;
        pnts.Add(anchor.transform);

        line.transform.rotation = Quaternion.LookRotation(surfaceNorm);

        UpdateLinePos();
        calcLen();
    }
    public void RemovePoint (int index) {
        line.positionCount--;
        Destroy(pnts[index].gameObject);
        pnts.RemoveAt(index);

        UpdateLinePos();
        calcLen();

        Debug.Log("Remove point !!");
    }
    public void ClearPoints () {
        init();

        line.positionCount = 0;
        pnts.Clear();
    }

    public void UpdateLinePos () {
        for (int i = 0; i < pnts.Count; i++) {
            line.SetPosition(i, pnts[i].position);
        }
    }
    private void calcLen () {
        Length = 0;
        for (int i = 1; i < pnts.Count; i++) {
            Length += Vector3.Distance(pnts[i - 1].position, pnts[i].position);
        }
    }

    private void OnDrawGizmos () {
        for (int i = 0; i < pnts.Count; i++)
            Gizmos.DrawSphere(pnts[i].position, 0.2f);
    }
}
