﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiretrackManager : MonoBehaviour {

    #region FIELDS
    [Header("Tiretrack config")]
    public float VertsDist = 1f;
    public float GapDist = 4f;
    public float HeightOffset = 0.01f;
    public float TrackLength = 5f;
    public Tiretrack TiretrackPrefab;

    [Header("Physics config")]
    public TiretrackManagerPhysics PhysType = TiretrackManagerPhysics.Collider;


    private List<Tiretrack> tracks = new List<Tiretrack>();
    private WheelCollider wheel;
    private WheelHit hit;
    #endregion

    #region PROPS
    public Tiretrack LastTrack { get { return tracks.Count > 0 ? tracks[tracks.Count - 1] : null; } }
    #endregion

    void Start () {
        TiretrackPrefab.ClearPoints();

        if (PhysType == TiretrackManagerPhysics.Wheel)
            wheel = GetComponent<WheelCollider>();
    }

    private void FixedUpdate () {

        if (PhysType == TiretrackManagerPhysics.Wheel) {
            if (wheel.GetGroundHit(out hit)) {
                updateTrackPosing(hit.point + hit.normal * HeightOffset, hit.normal, hit.collider.gameObject);
            }
        }
    }

    private void OnCollisionStay (Collision collision) {
        if (PhysType == TiretrackManagerPhysics.Collider) {
            // calc next point
            Vector3 pnt = Vector3.zero;
            Vector3 norms = Vector3.zero;
            for (int i = 0; i < collision.contacts.Length; i++) {
                pnt += collision.contacts[i].point;
                norms += collision.contacts[i].normal;
            }
            pnt /= collision.contacts.Length;
            norms /= collision.contacts.Length;
            norms.Normalize();
            norms *= HeightOffset;
            pnt += norms;


            updateTrackPosing(pnt, norms, collision.gameObject);
        }
    }

    private void updateTrackPosing (Vector3 pnt, Vector3 surfaceNorm, GameObject other) {

        float alpha = 1;

        // create initial track and new point to him
        if (LastTrack == null) {
            Tiretrack inst = Instantiate(TiretrackPrefab.gameObject).GetComponent<Tiretrack>();
            tracks.Add(inst);
            inst.AddPoint(pnt, surfaceNorm, alpha, other, this);

            clampTrackLen();
        }

        if (LastTrack != null) {

            if (LastTrack.Parent != other) {
                Tiretrack inst = Instantiate(TiretrackPrefab.gameObject).GetComponent<Tiretrack>();
                tracks.Add(inst);
                inst.AddPoint(pnt, surfaceNorm, alpha, other, this);

                // велдим два трэка при переходе между разными родительскими объектами, если расстояние меж точек < GapDist
                Tiretrack prevTrack = tracks[tracks.Count - 2];
                if (Vector3.Distance(prevTrack.LastPoint.position, LastTrack.LastPoint.position) < GapDist) {
                    prevTrack.LastPoint.position = LastTrack.LastPoint.position;
                    prevTrack.UpdateLinePos();
                }

                clampTrackLen();
                Debug.Log("New tiretrack cuz another parent");
            }

            float dist = Vector3.Distance(LastTrack.LastPoint.position, pnt);

            if (dist > GapDist) {
                Tiretrack inst = Instantiate(TiretrackPrefab).GetComponent<Tiretrack>();
                tracks.Add(inst);

                Debug.Log("New tiretrack cuz gap");
            }

            if (dist > VertsDist) {
                LastTrack.AddPoint(pnt, surfaceNorm, alpha, other, this);

                clampTrackLen();
            }
        }
    }
    private void clampTrackLen () {
        while (tracksLength() > TrackLength) {
            Tiretrack firstTrack = tracks[0];

            if (firstTrack.Points.Count > 0)
                firstTrack.RemovePoint(0);
            else {
                Destroy(tracks[0].gameObject);
                tracks.RemoveAt(0);
            }
        }
    }
    private float tracksLength () {
        float len = 0;
        for (int i = 0; i < tracks.Count; i++) {
            len += tracks[i].Length;
        }
        return len;
    }
}
