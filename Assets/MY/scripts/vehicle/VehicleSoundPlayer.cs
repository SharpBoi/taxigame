﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class VehicleSoundPlayer : MonoBehaviour {

    public static List<VehicleSoundPlayer> all = new List<VehicleSoundPlayer>();

    #region Fields
    public AudioSource EngineStart;
    public float StartIdleAfterSEC = 0.450f;
    public AudioSource EngineIdle;

    //public AnimationCurve DriveSoundCurve;
    //public AudioSource EngineDrive;

    public AudioSource DoorOpen;
    public AudioSource DoorClose;

    public AudioSource Brake;

    public List<AudioSource> CrashSounds = new List<AudioSource>();


    private AudioSource lastBumpToPlay;
    private float startScratchVolume;
    #endregion

    // Use this for initialization
    void Start () {
        StartCoroutine(startToIdle());

        all.Add(this);
    }
    private void OnDestroy () {
        all.Remove(this);
    }
    private IEnumerator startToIdle () {

        EngineStart.Play();

        float crntStartTime = 0;
        while (crntStartTime <= StartIdleAfterSEC) {
            crntStartTime += Time.deltaTime;

            yield return 0;
        }

        EngineIdle.gameObject.SetActive(true);
        EngineIdle.Play();

        yield return 0;
    }
    private void Update () {
        //if (EngineStart2Idle.time >= EngineStart2Idle.duration - 0.1)
        //    if (EngineIdle.isPlaying == false)
    }

    public void PitchEngine (float speedNorm) {
        EngineIdle.pitch = Mathf.Lerp(EngineIdle.pitch, speedNorm + 1, Time.deltaTime * 10);
    }

    public void PlayBrake (float volume) {
        if (!Brake.isPlaying)
            Brake.Play();
        Brake.volume = Mathf.Lerp(Brake.volume, volume, Time.deltaTime * 10);

    }
    public void StopBrake () {
        Brake.Stop();
    }

    public void PlayDoorOpen () {
        DoorOpen.Play();

        Debug.Log("SOUND: door open");
    }
    public void PlayDoorClose () {
        DoorClose.Play();

        Debug.Log("SOUND: door close");
    }

    public void PlayBump (float volumeMult) {
        //if (playingBump == null) {
        //    StartCoroutine(playBump(volumeMult));
        //}

        StartCoroutine("playBump", 1f);
    }
    private IEnumerator playBump (float volumeMult) {

        AudioSource selectedBump = lastBumpToPlay;
        while (selectedBump == lastBumpToPlay)
            selectedBump = CrashSounds[Random.Range(0, CrashSounds.Count)];

        lastBumpToPlay = selectedBump;

        GameObject bumpGO = Instantiate(lastBumpToPlay.gameObject);
        bumpGO.transform.SetParent(lastBumpToPlay.transform.parent);
        AudioSource bump = bumpGO.GetComponent<AudioSource>();
        bump.Play();

        while (bump.isPlaying)
            yield return 0;

        Destroy(bumpGO);

        yield return 0;

        //playingBump = CrashSounds[Random.Range(0, CrashSounds.Count)];

        //playingBump.volume = 1 * volumeMult;
        //playingBump.Play();
        //while (playingBump.time < playingBump.clip.length && playingBump.isPlaying) {
        //    yield return 0;
        //}

        //playingBump.Stop();
        //playingBump = null;

        //yield return 0;
    }

    public void DisableAllAudio () {
        EngineStart.enabled =
            EngineIdle.enabled =
            DoorOpen.enabled =
            DoorClose.enabled =
            Brake.enabled = false;
    }
}
