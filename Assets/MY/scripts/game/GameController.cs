﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    #region Fields
    public static GameController Instance { get; private set; }


    // pubs
    [Header("World")]
    public float VehicleDeathHeight = -10;
    public Vehicle Car;
    public MapGenerator Map;
    public Ufo ufo;

    [Header("Difficult")]
    public float DifficultStep = 0.1f;
    public float Difficult = 0;

    [Header("Game")]
    public GameLayout GameLayout;
    public float NiceDriveReward = 100;
    public float BadDriveReward = -100;
    public float KillPsngrReward = -500;
    public float CleverDriveReward = 100;
    public float BoardReward = 100;
    public float UnboardReward = 100;
    public float MaxReplaceCooldownSEC = 30;

    [Header("Gameover")]
    public int GameoverTime = 10;
    public GameoverLayout GameoverLayout;


    private float crntScore = 0;

    private float crntLooseTime = 0;
    private bool isGameOver = false;

    private TileMapTarget carTile;
    private bool isGotReward = false;

    private string[] cheats = new string[] {
        "iddqd",
        "fuel",
        "score",
        "speed",
        "inv"
    };
    private string cheat = "";
    private float crntCheatTime = 0;
    private float maxCheatTimeSec = 4;

    private bool isCountingOnWrongDirection;

    private float crntReplaceCooldownSEC;

    private bool targetTileFound = false;
    #endregion

    #region Props
    public bool IsGameStarted { get; private set; }
    public bool IsCheatActivated { get; private set; }
    public float ReplaceCooldown { get { return crntReplaceCooldownSEC; } }
    public float ReplaceCooldownNorm { get { return crntReplaceCooldownSEC / MaxReplaceCooldownSEC; } }
    #endregion

    #region Props
    #endregion

    // Use this for initialization
    void Start () {
        Debug.Log("OS: " + SystemInfo.operatingSystem);


        crntLooseTime = GameoverTime;

        if (Instance == null)
            Instance = this;
        else
            throw new Exception("There can be only one game !");

        carTile = Car.GetComponent<TileMapTarget>();

        AudioListener.volume = 1;

        increaseDifficult(0);
    }

    private void FixedUpdate () {
        if (IsGameStarted) {
            UpdateFuelBar();
            UpdateLooseCounter();
            UpdateLooseTimeCheck();
            UpdateDeathHeight();

            UpdateObstacleDriveReward();

            UpdateUfoCam();

            if (ufo.Ray.RigidsInRay.Contains(Car.Rigid)) {
                GameLayout.HidePsngrHint();
            }
        }
    }
    private void Update () {
        if (IsGameStarted) {
            UpdateCheatRecognition();
        }
    }
    private void LateUpdate () {
        if (IsGameStarted) {
            UpdateMoveDirectionWarning();
        }
    }

    // MAP STUFF
    // call by passanger board/unboard event
    private void updateMapBoardingState () {
        Debug.Log("game.onUnboard.car.psngrs.count " + Car.PassangersCount);

        if (Car.PassangersCount == 0) {
            Map.CanGenUnboard = false;

            for (int i = 0; i < Map.Tiles.Length; i++)
                if (Map.Tiles[i].Type == TileType.Unboarding) {
                    Map.Tiles[i].BoardController.HideCircleMark();
                    Map.Tiles[i].BoardController.HideArrowMark();
                }
        }
        else {
            Map.CanGenUnboard = true;

            for (int i = 0; i < Map.Tiles.Length; i++)
                if (Map.Tiles[i].Type == TileType.Unboarding)
                    Map.Tiles[i].BoardController.ShowCircleMark();
        }


        if (Car.PassangersCount == Car.MaxPassangersCount) {
            Map.CanGenBoard = false;

            for (int i = 0; i < Map.Tiles.Length; i++)
                if (Map.Tiles[i].Type == TileType.Boarding) {
                    Map.Tiles[i].BoardController.HideCircleMark();
                    Map.Tiles[i].BoardController.SetInteractable(false);
                }
        }
        else {
            Map.CanGenBoard = true;

            for (int i = 0; i < Map.Tiles.Length; i++)
                if (Map.Tiles[i].Type == TileType.Boarding) {
                    Map.Tiles[i].BoardController.ShowCircleMark();
                    Map.Tiles[i].BoardController.SetInteractable(true);
                }
        }
    }

    public void GenerateTargetUnboardTile () {
        Debug.Log("tile gened");


        Tile lastTile = Map.Tiles[Map.Tiles.Length - 1];
        if (lastTile.Type == TileType.Unboarding) {

            lastTile.BoardController.MakeCircleRed();

            if (targetTileFound == false) {
                float rnd = UnityEngine.Random.Range(0f, 1f);
                if (rnd <= 0.65f) {
                    lastTile.BoardController.SetAsTargetUnboard();
                    targetTileFound = true;
                }
            }
        }
    }

    private void UpdateFuelBar () {
        GameLayout.SetGasBar(Car.CurrentFuelNormalized);
    }
    private void UpdateLooseCounter () {
        if (Car.HasFuel == false && Car.IsBoardingNow == false) {
            GameLayout.SetLooseCounterVis(true);

            crntLooseTime -= Time.deltaTime;
            if (crntLooseTime < 0) crntLooseTime = 0;

            if (Mathf.Round(Car.CurrentSpeed) == 0 && crntLooseTime > 1)
                crntLooseTime = 1;

            GameLayout.UpdateLooseCounter(crntLooseTime);
        }
        else {
            crntLooseTime = GameoverTime;
            GameLayout.SetLooseCounterVis(false);
        }
    }
    private void UpdateLooseTimeCheck () {
        if (crntLooseTime == 0 && isGameOver == false || Car == null) {
            isGameOver = true;

            ShowGameOver();

            float score = 0;
            for (int i = 0; i < Car.UnboardedPassangers.Length; i++)
                score += Car.UnboardedPassangers[i].CurrentPaymentNormalized;
            GameoverLayout.SetScore(score);
        }
    }
    private void UpdateDeathHeight () {
        if (Car.transform.position.y < -10)
            ReplacePlayerOnTile(0, false);
    }

    private void UpdateMoveDirectionWarning () {

        if (carTile.CurrentTile.Self != null && Car.IsOnGround && Car.IsBoardingNow == false)
            if (carTile.CurrentTile.Self.Type != TileType.TurnLeft &&
                carTile.CurrentTile.Self.Type != TileType.TurnRight) {
                float angle = Vector3.SignedAngle(carTile.CurrentTile.Self.transform.forward, Car.transform.forward, Vector3.up);

                float speedRounding = 2.5f;

                if (Mathf.Round(Car.CurrentSpeed * speedRounding) != 0)
                    if (angle < 0 && Mathf.Round(Car.CurrentSignedSpeed * speedRounding) > 0 ||
                        angle > 0 && Mathf.Round(Car.CurrentSignedSpeed * speedRounding) < 0) {
                        GameLayout.StartWarnShowCounting();
                    }
                    else
                        GameLayout.HideDirectionWarning();
            }

        if (Car.IsBoardingNow)
            GameLayout.HideDirectionWarning();
    }
    //private IEnumerator startCountToReplacePlayer()
    //{
    //    isCountingOnWrongDirection = true;
    //    float time = 0;
    //    while (true)
    //    {
    //        time += Time.deltaTime;

    //        if (time >= 1)
    //            break;

    //        yield return 0;
    //    }

    //    GameLayout.HideDirectionWarning();
    //    isCountingOnWrongDirection = false;
    //    ReplacePlayerOnTile(1);
    //}
    private void UpdateObstacleDriveReward () {

        if (carTile.CurrentTile.Self != null)
            if (carTile.CurrentTile.Self.PrevTile != null)
                if (carTile.CurrentTile.Self.PrevTile.Type == TileType.Crossroad) {

                    CrossroadController crossroad = carTile.CurrentTile.Self.PrevTile.GetComponent<CrossroadController>();

                    if (isGotReward == false && crossroad.IsTrafficOnRoad && carTile.PrevTile.GotHitHere == false) {

                        isGotReward = true;

                        ApplyScore(CleverDriveReward);
                        GameLayout.ShowCleverDrive();

                        Debug.Log("CROSSROAD REWARD !!!");
                    }
                }
                else
                    isGotReward = false;
    }
    private void UpdateCheatRecognition () {
        string key = Input.inputString;

        if (key == "") {
            crntCheatTime += Time.deltaTime;
            if (crntCheatTime >= maxCheatTimeSec) {
                crntCheatTime = 0;
                cheat = "";
            }
        }
        else {
            crntCheatTime = 0;
            cheat += key.ToLower();

            for (int i = 0; i < cheats.Length; i++) {
                if (cheat.IndexOf(cheats[i]) != -1) {
                    cheat = cheats[i];

                    if (cheat == "iddqd") {
                        if (IsCheatActivated) {
                            IsCheatActivated = false;
                            Car.DisableGodMode();

                            Debug.Log("CHEAT: god mod activ");
                        }
                        else {
                            IsCheatActivated = true;
                            Car.EnableGodMode();

                            Debug.Log("CHEAT: god mod deactiv");
                        }
                    }

                    else if (cheat == "fuel") {
                        Car.ApplyFuel(Car.MaxFuel);
                        Debug.Log("CHEAT: fuel");
                    }

                    else if (cheat == "score") {
                        ApplyScore(1000);
                        Debug.Log("CHEAT: score");
                    }

                    else if (cheat == "speed") {
                        Car.EnableSpeedBoost(30);
                        Debug.Log("CHEAT: speed !");
                    }

                    else if (cheat == "inv") {
                        Car.EnableInvincibility(30);
                        Debug.Log("CHEAT: invincibility !");
                    }

                    cheat = "";
                }
            }
        }
    }
    private void UpdateUfoCam () {
        if (ufo.Ray.RigidsInRay.Contains(Car.Rigid)) {
            VehicleCam camFlw = Camera.main.GetComponent<VehicleCam>();

            camFlw.FollowSpeed = 1;
            camFlw.Target = Car.transform;
            camFlw.OffsetPos = 50;
            camFlw.OffsetHeight = 2;
            camFlw.AngleX = 0;
        }
    }

    public void ReplacePlayerOnTileWithCooldown (int indexOffset) {
        ReplacePlayerOnTile(indexOffset, true);
    }
    public void ReplacePlayerOnTile (int indexOffset, bool useCooldown) {

        if (Car.IsBoardingNow == false &&
            crntReplaceCooldownSEC == 0 &&
            ufo.Ray.RigidsInRay.Contains(Car.Rigid) == false ||
            useCooldown == false) {

            Tile closestTile = Util.Min(Map.Tiles, (tile) => {
                return Vector3.Distance(Car.transform.position, tile.transform.position);
            });

            if (closestTile != null) {

                if (ufo.CurrentTile != null)
                    if (closestTile.Index <= ufo.CurrentTile.Index)
                        for (int i = 0; i < Map.Tiles.Length; i++) {
                            if (Map.Tiles[i].Index > ufo.CurrentTile.Index) {
                                closestTile = Map.Tiles[i];
                                break;
                            }
                        }

                if (closestTile.ReturnPoint != null)
                    Car.transform.position = closestTile.ReturnPoint.position;
                else
                    Car.transform.position = closestTile.transform.position;

                Car.transform.rotation = Quaternion.Euler(0, closestTile.transform.rotation.eulerAngles.y + 90, 0);
                Car.CurrentVelocity = Vector3.zero;
                Car.Rigid.angularVelocity = Vector3.zero;

                if (useCooldown)
                    StartCoroutine(cooldownReplace());
            }
        }
    }
    private IEnumerator cooldownReplace () {

        crntReplaceCooldownSEC = MaxReplaceCooldownSEC;

        while (crntReplaceCooldownSEC > 0) {
            crntReplaceCooldownSEC -= Time.deltaTime;
            Debug.Log(crntReplaceCooldownSEC);
            GameLayout.SetCooldownRect(ReplaceCooldownNorm);
            yield return 0;
        }

        crntReplaceCooldownSEC = 0;
    }

    public void ShowGameOver () {
        Time.timeScale = 0;

        GameoverLayout.Show(crntScore, 1);
        IsGameStarted = false;

        for (int i = 0; i < VehicleSoundPlayer.all.Count; i++) {
            VehicleSoundPlayer.all[i].DisableAllAudio();
        }

        StartCoroutine(decreaseWorldAudio());
    }
    private IEnumerator decreaseWorldAudio () {

        float coef = 1;
        while (coef > 0) {

            AudioListener.volume = coef;
            coef -= Time.unscaledDeltaTime;

            yield return 0;
        }
        coef = 0;
    }

    public void StartGame () {
        IsGameStarted = true;
        Map.GenerateVehicles = true;
    }
    public void RestartScene () {
        Time.timeScale = 1;
        crntLooseTime = GameoverTime;

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ShowUIHitHint () {
        GameLayout.ShowDangerousDrive();
        ApplyScore(BadDriveReward);
        GameLayout.SetScore(crntScore);
    }
    public void ShowHitPsngr () {
        ApplyScore(KillPsngrReward);
        GameLayout.ShowKillPsngr();
    }

    public void OnStartBoarding () {
        Passanger psngr = null;
        for (int i = 0; i < Passanger.all.Count; i++) {
            if (Passanger.all[i].IsBoarding) {
                psngr = Passanger.all[i];
                break;
            }
        }

        GameLayout.ShowBoardingReplica(psngr);
    }
    public void OnBoarded (Passanger psngr) {
        updateMapBoardingState();

        Map.TargetGenerateTo.ClearHistory();

        ApplyScore(BoardReward);
        GameLayout.ShowBoardPsngr();
    }
    public void OnStartUnboarding () {
        Passanger psngr = null;
        for (int i = 0; i < Passanger.all.Count; i++) {
            if (Passanger.all[i].IsUnboarding) {
                psngr = Passanger.all[i];
                break;
            }
        }

        if (carTile.CurrentTile.Type == TileType.Unboarding)
            if (carTile.CurrentTile.Self.BoardController.IsTargetUnboard) {
                GameLayout.ShowGoodUnboardReplica(psngr);
            }
            else {
                GameLayout.ShowBadUnboardReplica(psngr);
            }
    }
    public void OnUnboarded (Passanger psngr) {
        updateMapBoardingState();
        increaseDifficult(DifficultStep);
        targetTileFound = false;

        if (carTile.CurrentTile.Self.BoardController.IsTargetUnboard) {
            Debug.Log("Unboarded on right tile !!!");

            if (!IsCheatActivated) {
                ApplyScore(psngr.CurrentPayment);
                Car.ApplyFuel(psngr.CurrentPayment);

                ApplyScore(UnboardReward);
                GameLayout.ShowUnboardPsngr();
            }
        }
        else {
            Debug.Log("Unboarded on WRONG tile !!!");

            psngr.SetPayment(psngr.CurrentPayment / 2);

            if (!IsCheatActivated) {
                ApplyScore(psngr.CurrentPayment);
                Car.ApplyFuel(psngr.CurrentPayment);
            }
        }



        //updateMapBoardingState();
        //increaseDifficult();

        //for (int i = 0; i < Map.Tiles.Length; i++) {
        //    if (Map.Tiles[i].Type == TileType.Unboarding)
        //        Map.Tiles[i].BoardController.HideArrowMark();
        //}

        //if (Map.TargetGenerateTo.DrivedUnboardTiles.Length - 1 == psngr.TileIndexWhereUnboard) {
        //    ShowPayment();

        //    if (!IsCheatActivated) {
        //        ApplyScore(psngr.CurrentPayment);
        //        Car.ApplyFuel(psngr.CurrentPayment);
        //    }

        //    Debug.Log("Thats RIGHT stop");
        //}
        //else {
        //    Debug.Log("Thats WRONG stop");

        //    psngr.SetPayment(psngr.CurrentPayment / 2);

        //    if (!IsCheatActivated) {
        //        ApplyScore(psngr.CurrentPayment);
        //        Car.ApplyFuel(psngr.CurrentPayment);
        //    }
        //}

        //GameLayout.ShowUnboardHint();
    }

    private void increaseDifficult (float diffStep) {

        Difficult += diffStep;
        //difficult = Mathf.Clamp(difficult, 0, 1);

        // change broken road gen prob
        Map.BrokenRoadGenProbab += diffStep;
        Map.BrokenRoadGenProbab = Mathf.Clamp(Map.BrokenRoadGenProbab, 0, 1);

        // change vehicles gen probab
        Map.VehiclesCountPerGen = (int)(2 * Difficult) + 1;
        Map.VehicleGenProbability += diffStep;
        Map.VehicleGenProbability = Mathf.Clamp(Map.VehicleGenProbability, 0, 1);

        // change probs of tiles gen
        for (int i = 0; i < Map.TilesTypesProbs.Count; i++) {

            Map.TilesTypesProbs[i].SetProb(
                probByDifficult(Difficult, Map.TilesTypesProbs[i].StartProbability, Map.TilesTypesProbs[i].DifficultChangeMultiplier));
        }
    }
    private float probByDifficult (float difficult, float startGenProb, float diffChangeMult) {
        return startGenProb + difficult * diffChangeMult;
    }

    public void ApplyScore (float value) {
        crntScore += value;
        if (crntScore < 0) crntScore = 0;
        GameLayout.SetScore(crntScore);
    }

    public void ApplyBonus (Bonus bonus) {
        if (bonus.Type == BonusType.Fuel) {
            Debug.Log("BONUS: fuel");

            Car.ApplyFuel(2000);
        }

        else if (bonus.Type == BonusType.Invincibility) {
            Debug.Log("BONUS: invincibility");

            Car.EnableInvincibility(30);
        }

        else if (bonus.Type == BonusType.SpeedBoost) {
            Debug.Log("BONUS: speed boost");

            Car.EnableSpeedBoost(30);
        }
    }
}
