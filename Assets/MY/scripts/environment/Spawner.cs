﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Spawner : MonoBehaviour {

    #region Fields
    public float SpawnPerSec = 1;
    [Range(0, 1)]
    public float RandomizeSpawnRate = 0;
    public bool CanSpawnIfZoneBusy = false;
    
    public GameObject[] Exemplars;
    public bool RandomSpawnObjects = true;
    public float InitialSpeed = 1;

    public bool IsSpawnNow;
    public bool SpawnOnce;

    public UnityEvent OnSpawn;


    private bool isZoneEmpty = true;
    private float crntSpawnTime = 0;
    #endregion

    #region Props
    public bool Spawnable { get; private set; }
    public GameObject LastSpawnedObject { get; private set; }
    #endregion

    private void Start()
    {
        Spawnable = true;
    }

    private void FixedUpdate()
    {
        if (IsSpawnNow && Spawnable)
        {
            bool canSpawn = Random.Range(0f, 1f) <= 1 - RandomizeSpawnRate;

            crntSpawnTime += Time.deltaTime * SpawnPerSec;
            if (crntSpawnTime >= 1 && (isZoneEmpty || CanSpawnIfZoneBusy) && canSpawn)
            {
                crntSpawnTime = 0;

                spawn();

                if (SpawnOnce)
                    Spawnable = false;
            }
        }
    }

    private void spawn()
    {
        GameObject inst = null;
        if (RandomSpawnObjects)
            inst = Instantiate(Exemplars[Random.Range(0, Exemplars.Length)]);
        else
            inst = Instantiate(Exemplars[0]);
        inst.transform.SetParent(transform.parent);

        inst.transform.rotation = transform.rotation;
        inst.transform.position = transform.position;

        Rigidbody instRigid = inst.GetComponent<Rigidbody>();
        if (instRigid != null)
            instRigid.velocity = transform.forward * InitialSpeed;

        LastSpawnedObject = inst;

        OnSpawn.Invoke();
    }

    public void PauseSpawn()
    {
        IsSpawnNow = false;
    }
    public void StopSpawn()
    {
        IsSpawnNow = false;
        crntSpawnTime = 0;
    }
    public void PlaySpawn()
    {
        IsSpawnNow = true;
    }

    private void OnTriggerStay(Collider other)
    {
        isZoneEmpty = false;
    }
    private void OnTriggerExit(Collider other)
    {
        isZoneEmpty = true;
    }
}
