﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteInEditMode]
public class PathBuilder : MonoBehaviour {

    #region Fields
    public static List<PathBuilder> AllPaths = new List<PathBuilder>();
    public static PathBuilder GetClosestPath (Vector3 pos) {
        //PathNode[] nodes = new PathNode[AllPaths.Count];
        //for (int i = 0; i < AllPaths.Count; i++) {
        //    nodes[i] = AllPaths[i].GetClosestNode(pos);
        //}

        PathNode[] nodes = Util.Cast(AllPaths, (path) => {
            return path.GetClosestNode(pos);
        });

        PathNode closest = Util.Min(nodes, (node) => {
            return Vector3.Distance(pos, node.transform.position);
        });

        return closest.Path;
    }


    public float GizmoRadius = 0.3f;
    public bool EnableBezier = false;
    public float BezierInterpolation = 1f;


    private PathNode[] nodes = new PathNode[0];
    private List<Vector3> bezierPoints = new List<Vector3>();
    #endregion

    #region Props
    public PathNode[] Nodes { get { return nodes; } }
    public float PathLength {
        get {
            float len = 0;
            for (int i = 1; i < nodes.Length; i++)
                len += Vector3.Distance(nodes[i].transform.position, nodes[i - 1].transform.position);

            return len;
        }
    }
    #endregion

    private void Awake () {
        GetNodes();
    }
    private void Start () {
        GetNodes();

        if (AllPaths.Contains(this) == false)
            AllPaths.Add(this);
    }
    private void Update () {
        if (transform.childCount != nodes.Length)
            GetNodes();

        if (EnableBezier)
            BuildBezier();
        if (!EnableBezier && bezierPoints.Count != 0)
            bezierPoints.Clear();
    }

    private void OnDrawGizmos () {
        if (nodes.Length >= 2) {
            if (!EnableBezier)
                for (int i = 1; i < nodes.Length; i++) {
                    Gizmos.DrawLine(nodes[i - 1].transform.position, nodes[i].transform.position);
                }
            else
                for (int i = 1; i < bezierPoints.Count; i++) {
                    Gizmos.DrawLine(bezierPoints[i], bezierPoints[i - 1]);
                }
        }
    }

    private void OnDestroy () {
        AllPaths.Remove(this);
    }

    public void AddNodeAt (int at) {
        GameObject nodeObj = new GameObject();
        nodeObj.AddComponent(typeof(PathNode));
        PathNode node = nodeObj.GetComponent<PathNode>();

        node.Path = this;
        node.name = "node (" + at + ")";
        nodeObj.transform.SetParent(this.transform);
        nodeObj.transform.SetSiblingIndex(at);

        //ArrayUtility.Insert<PathNode>(ref nodes, at, node);
        List<PathNode> nodesList = new List<PathNode>();
        nodesList.AddRange(nodes);
        nodesList.Insert(at, node);
        nodes = nodesList.ToArray();

        for (int i = 0; i < nodes.Length; i++) {
            nodes[i].Index = i;
            nodes[i].name = "node (" + i + ")";
        }

        PathNode prevNode = null;
        PathNode nextNode = null;

        if (at != 0)
            prevNode = nodes[(at - 1) % nodes.Length];
        else
            prevNode = nodes[nodes.Length - 1];
        nextNode = nodes[(at + 1) % nodes.Length];
        nodeObj.transform.position = (prevNode.transform.position + nextNode.transform.position) / 2;

        prevNode.NextNode = node;
        node.PrevNode = prevNode;

        nextNode.PrevNode = node;
        node.NextNode = nextNode;
    }
    public void GetNodes () {
        nodes = GetComponentsInChildren<PathNode>();
        for (int i = 0; i < nodes.Length; i++) {
            nodes[i].Path = this;
            if (i > 0) {
                nodes[i].PrevNode = nodes[i - 1];
                nodes[i - 1].NextNode = nodes[i];
            }
            nodes[i].Index = i;
            nodes[i].name = "node (" + i + ")";
        }
        nodes[0].PrevNode = nodes[nodes.Length - 1];
        nodes[nodes.Length - 1].NextNode = nodes[0];
    }
    public PathNode GetClosestNode (Vector3 point, out int index) {
        index = -1;

        float min = float.MaxValue;
        int minI = -1;
        for (int i = 0; i < nodes.Length; i++) {
            float dist = Vector3.Distance(point, nodes[i].transform.position);
            if (min >= dist) {
                min = dist;
                minI = i;
            }
        }

        index = minI;

        if (minI != -1)
            return nodes[minI];

        return null;
    }
    public PathNode GetClosestNode (Vector3 point) {
        int i = 0;
        return GetClosestNode(point, out i);
    }
    public int GetShortestDirection (PathNode from, PathNode to, List<PathNode> chain = null) {

        if (from.Index < to.Index)
            return 1;
        else
            return -1;
    }
    public void GetWalkPoseByCoef (float coef, bool loopCoef, out Vector3 pos, out Vector3 lookDir, out PathNode nextNode) {

        if (loopCoef) {
            if (coef > 1)
                coef = coef - (int)coef;
            else if (coef < 0)
                coef = 1 - Mathf.Abs(Mathf.Abs((int)coef) + coef);
        }
        else {
            coef = Mathf.Clamp(coef, 0, 1);
        }

        PathNode a = Nodes[0];
        PathNode b = Nodes[1];

        float normSegment = 0;
        float prevNormSegment = 0;

        for (int i = 1, iPrev = 0; i < Nodes.Length + 1; i++) {
            iPrev = i - 1;
            if (i == Nodes.Length) {
                i = 0;
                iPrev = Nodes.Length - 1;
            }

            a = Nodes[iPrev];
            b = Nodes[i];

            prevNormSegment = normSegment;
            normSegment += Vector3.Distance(a.transform.position, b.transform.position) / PathLength;

            if (normSegment >= coef)
                break;
        }

        Vector3 dir = b.transform.position - a.transform.position;
        dir.Normalize();

        pos = a.transform.position + dir * (coef - prevNormSegment) * PathLength;
        lookDir = dir;
        nextNode = b;
    }



    /// <summary>
    /// Инвертирует двусвязное дерево нодов этого пути
    /// </summary>
    public void Reverse () {
        Array.Reverse(nodes);
        for (int i = 0; i < nodes.Length; i++)
            Util.Swap(ref nodes[i].PrevNode, ref nodes[i].NextNode);
    }

    public float GetCoefAtNode (PathNode node) {

        float coef = 0;

        for (int i = 1; i < nodes.Length; i++) {
            coef += Vector3.Distance(nodes[i - 1].transform.position, nodes[i].transform.position);
            if (nodes[i] == node)
                break;
        }

        return coef / PathLength;
    }
    public float GetCoefAtNode (int nodeIndex) {
        for (int i = 0; i < nodes.Length; i++)
            if (nodes[i].Index == nodeIndex)
                return GetCoefAtNode(nodes[i]);

        return -1;
    }

    public void BuildBezier () {

        bezierPoints.Clear();
        if (BezierInterpolation < 1) BezierInterpolation = 1;

        int n = nodes.Length - 1;

        for (float t = 0; t <= 1; t += 1 / BezierInterpolation) {

            Vector3 offset = nodes[nodes.Length - 1].transform.position;

            // calc first part of func
            Vector3 first = bezierFirst(n, t) * (nodes[0].transform.position - offset);
            // calc last part of func
            Vector3 last = bezierLast(n, t) * (nodes[nodes.Length - 1].transform.position - offset);
            Vector3 mid = Vector3.zero;

            // precalc coefs of middle part of func
            float evenCoef = bezierEvenMid(n, t);
            float notEventCoef = bezierNotEvenMid(n, t);
            float midCoef = 0;

            // calc mid part of function
            for (int i = 1; i < nodes.Length - 1; i++) {
                if (i % 2 == 1) midCoef = notEventCoef;
                else midCoef = evenCoef;

                mid += midCoef * (nodes[i].transform.position - offset);
            }

            // add all calculated points
            bezierPoints.Add(first + mid + last + offset);
        }
        // add last node 
        bezierPoints.Add(nodes[nodes.Length - 1].transform.position);
    }

    private float bezierFirst (int n, float t) {
        return Mathf.Pow(1 - t, n);
    }
    private float bezierLast (int n, float t) {
        return Mathf.Pow(t, n);
    }
    private float bezierEvenMid (int n, float t) {
        return n * Mathf.Pow(t, n - 1) * (1 - t);
    }
    private float bezierNotEvenMid (int n, float t) {
        return n * t * Mathf.Pow(1 - t, n - 1);
    }
}

#if UNITY_EDITOR
[CanEditMultipleObjects]
[CustomEditor(typeof(PathBuilder))]
public class PathBuilderInspector : Editor {
    private int at;

    public override void OnInspectorGUI () {
        DrawDefaultInspector();
        PathBuilder tgt = target as PathBuilder;

        if (GUILayout.Button("Fill nodes"))
            tgt.GetNodes();

        EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
        {
            at = EditorGUILayout.IntField("At", at);
            if (GUILayout.Button("Add")) {
                tgt.AddNodeAt(at);
            }
        }
        EditorGUILayout.EndHorizontal();
    }
}
#endif