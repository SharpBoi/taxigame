﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Trigger : MonoBehaviour {

    #region Fields

    public UnityEvent OnTrigger;

    #endregion

    private void OnTriggerEnter(Collider other)
    {
        OnTrigger.Invoke();
    }
}
