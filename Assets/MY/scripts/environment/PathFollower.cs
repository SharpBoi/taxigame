﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFollower : MonoBehaviour {

    #region Fields
    public PathBuilder Path;

    public float WalkSpeed = 1;
    public float MinRadiusToNodeToWalkToNext;

    public bool CanWalk = true;
    public int WalkDirection = 1;


    public float TESTCoefWalk = 0;
    public bool TESTCanTestWalk = false;


    private int prevNodeIndex = 0;
    private int crntNodeIndex = 0;
    private int nextNodeIndex = 1;
    #endregion

    #region Props
    public int CurrentNodeIndex {
        get { return crntNodeIndex; }
        set {
            crntNodeIndex = value;
            nextNodeIndex = value + 1;
            if (value == 0)
                prevNodeIndex = Path.Nodes.Length - 1;
            else
                prevNodeIndex = value - 1;
        }
    }
    public PathNode CurrentNode { get { return Path.Nodes[crntNodeIndex]; } }
    public PathNode PrevNode { get { return Path.Nodes[prevNodeIndex]; } }
    public PathNode NextNode { get { return Path.Nodes[nextNodeIndex]; } }
    #endregion

    #region Funcs
    private void Start () {

        prevNodeIndex = Path.Nodes.Length - 1;
        crntNodeIndex = 0;
        nextNodeIndex = crntNodeIndex + 1;
    }

    private void FixedUpdate () {
        if (TESTCanTestWalk) {
            Vector3 pos, look;
            PathNode node;
            Path.GetWalkPoseByCoef(TESTCoefWalk, true, out pos, out look, out node);
            transform.position = Vector3.Lerp(transform.position, pos, Time.deltaTime * 10);
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(look), Time.deltaTime * 10);
        }
    }

    private void calcNodeIndices (int newNodeIndex) {

        crntNodeIndex = Util.Ring(newNodeIndex, Path.Nodes.Length);

        prevNodeIndex = Util.Ring(crntNodeIndex - 1, Path.Nodes.Length);

        nextNodeIndex = Util.Ring(crntNodeIndex + 1, Path.Nodes.Length);
    }

    public void WalkByCoef (float coef) {

        Vector3 pos;
        Vector3 dir;
        PathNode nextNode;
        Path.GetWalkPoseByCoef(coef, true, out pos, out dir, out nextNode);

        transform.position = pos;
        transform.rotation = Quaternion.LookRotation(dir);
    }
    public void WalkByCoef (float coef, out PathNode nextNode) {

        Vector3 pos;
        Vector3 dir;
        Path.GetWalkPoseByCoef(coef, true, out pos, out dir, out nextNode);

        transform.position = pos;
        transform.rotation = Quaternion.LookRotation(dir);
    }

    public void StartWalk () {
        CanWalk = true;
    }
    public void StopWalk () {
        CanWalk = false;
    }
    #endregion
}
