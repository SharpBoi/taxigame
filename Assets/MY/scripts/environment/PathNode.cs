﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

#if UNITY_EDITOR
[CanEditMultipleObjects]
#endif
public class PathNode : MonoBehaviour {

    #region Fields
    public PathBuilder Path;

    public bool IsStop = false;

    public int Index;

    //[HideInInspector]
    public PathNode PrevNode;
    //[HideInInspector]
    public PathNode NextNode;
    
    #endregion

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position, Path.GizmoRadius);
    }
}

#if UNITY_EDITOR
[CanEditMultipleObjects]
[CustomEditor(typeof(PathNode))]
public class PathNodeInspector : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        PathNode tgt = target as PathNode;

        EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
        {
            if (GUILayout.Button("Add node before"))
                tgt.Path.AddNodeAt(tgt.Index);

            if (GUILayout.Button("Add node after"))
                tgt.Path.AddNodeAt(tgt.Index + 1);
        }
        EditorGUILayout.EndHorizontal();
    }
}
#endif