﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class Teleport : MonoBehaviour {

    #region Fields
    [HideInInspector]
    public int TargetTagIndex = -1;
    [HideInInspector]
    public string TargetTag;
    public Transform TpTo;

    public bool OverrideRotation;
    public bool KeepVelocity;
    #endregion

    private void Start()
    { 

    }

    private void OnTriggerEnter(Collider other)
    {
        Transform target = other.transform;

        if (TargetTag == target.tag)
        {
            target.position = TpTo.position;
            if (OverrideRotation)
                target.rotation = TpTo.rotation;

            Rigidbody targetRigid = target.GetComponent<Rigidbody>();
            if (targetRigid != null)
            {
                if (KeepVelocity)
                    targetRigid.velocity = TpTo.forward * targetRigid.velocity.magnitude;
                else
                    targetRigid.velocity = Vector3.zero;
            }
        }
    }

    private void OnDrawGizmos()
    {
        if (TpTo != null)
        {
            Gizmos.DrawLine(transform.position, TpTo.position);
            Gizmos.DrawLine(TpTo.position, TpTo.forward * 2 + TpTo.position);
            Gizmos.DrawSphere(TpTo.transform.position, 0.3f);
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(Teleport))]
[CanEditMultipleObjects]
public class TeleportInspector : Editor
{
    public override void OnInspectorGUI()
    {
        Teleport tgt = target as Teleport;

        DrawDefaultInspector();
        tgt.TargetTagIndex = EditorGUILayout.Popup("Target tag index", tgt.TargetTagIndex, UnityEditorInternal.InternalEditorUtility.tags);
        tgt.TargetTag = UnityEditorInternal.InternalEditorUtility.tags[tgt.TargetTagIndex];
    }
}
#endif