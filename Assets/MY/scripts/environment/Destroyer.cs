﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class Destroyer : MonoBehaviour {

    #region FIELDS
    public bool DestroyWithScript = false;
    [HideInInspector]
    public MonoBehaviour SpecComponent;

    public bool DestroyWithTag = false;
    [HideInInspector]
    public string SpecTag;

    public bool DestroyEverything;

    public List<Collider> IgnoreColliders = new List<Collider>();
    
    public UnityEvent BeforeDestroy;

    #endregion

    private void OnTriggerEnter(Collider other)
    {
        if (IgnoreColliders.Contains(other) == false)
        {
            if (DestroyWithScript)
                if (other.GetComponent(SpecComponent.GetType()) != null)
                {
                    BeforeDestroy.Invoke();
                    Destroy(other.gameObject);
                    return;
                }

            if (DestroyWithTag)
                if (other.tag == SpecTag)
                {
                    BeforeDestroy.Invoke();
                    Destroy(other.gameObject);
                    return;
                }

            if (DestroyEverything)
            {
                BeforeDestroy.Invoke();
                Destroy(other.gameObject);
            }
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(Destroyer))]
[CanEditMultipleObjects]
public class DestroyerInspector : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Destroyer tgt = target as Destroyer;

        if (tgt.DestroyWithScript)
        {
            tgt.SpecComponent = EditorGUILayout.ObjectField(tgt.SpecComponent, typeof(MonoBehaviour), true) as MonoBehaviour;
        }

        if (tgt.DestroyWithTag)
        {
            tgt.SpecTag = EditorGUILayout.TextField(tgt.SpecTag);
        }
    }
}
#endif