﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossroadController : MonoBehaviour {

    #region Fields
    public List<Spawner> Spawners = new List<Spawner>();
    public float MaxSwitchTime = 10;

    public TrafficLightController[] TrafficLights;


    private float crntSwitchTime = 0;
    #endregion

    #region PROPS
    public bool IsTrafficOnRoad { get; private set; }
    #endregion

    // Use this for initialization
    void Start () {
        for (int i = 0; i < Spawners.Count; i++)
            Spawners[i].PlaySpawn();

        IsTrafficOnRoad = true;
        StartCoroutine("countToGreen");
	}

    private void FixedUpdate()
    {
        //crntSwitchTime += Time.deltaTime;
        //if (crntSwitchTime >= MaxSwitchTime && (trafficOnRoad == false || roadOpened))
        //{
        //    crntSwitchTime = 0;

        //    roadOpened = !roadOpened;

        //    //Path.SetAllWalkersEnableWalk(!roadOpened);

        //    // switch vehicle spawners
        //    for (int i = 0; i < Spawners.Count; i++)
        //        if (roadOpened)
        //            Spawners[i].StopSpawn();
        //        else
        //            Spawners[i].PlaySpawn();

        //    // switch color on traffic lights
        //    for (int i = 0; i < TrafficLights.Length; i++)
        //        if (roadOpened)
        //            TrafficLights[i].ShowGreen();
        //        else
        //            TrafficLights[i].ShowRed();
        //}
    }

    private IEnumerator countToGreen() {

        crntSwitchTime = 0;
        while (true) {
            crntSwitchTime += Time.deltaTime;
            if (crntSwitchTime >= MaxSwitchTime)
                break;

            yield return 0;
        }

        IsTrafficOnRoad = false;

        // stop spawner
        for (int i = 0; i < Spawners.Count; i++)
            Spawners[i].StopSpawn();

        // show green light
        for (int i = 0; i < TrafficLights.Length; i++)
            TrafficLights[i].ShowGreen();

        // start count to red
        StartCoroutine("countToRed");
    }
    private IEnumerator countToRed() {

        crntSwitchTime = 0;
        while (true) {
            crntSwitchTime += Time.deltaTime;
            if (crntSwitchTime >= MaxSwitchTime)
                break;

            yield return 0;
        }

        IsTrafficOnRoad = true;

        // play spawner
        for (int i = 0; i < Spawners.Count; i++)
            Spawners[i].PlaySpawn();

        // show red light
        for (int i = 0; i < TrafficLights.Length; i++)
            TrafficLights[i].ShowRed();

        // start count to green
        StartCoroutine("countToGreen");
    }

    //private void OnTriggerStay(Collider other)
    //{
    //    PathFollower walker = other.GetComponent<PathFollower>();
    //    if (walker != null)
    //        trafficOnRoad = true;
    //}
    //private void OnTriggerExit(Collider other)
    //{
    //    PathFollower walker = other.GetComponent<PathFollower>();
    //    if (walker != null)
    //        trafficOnRoad = false;
    //}
}
