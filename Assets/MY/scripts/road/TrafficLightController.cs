﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficLightController : MonoBehaviour {

    #region Fields
    public GameObject Red;
    public GameObject Yellow;
    public GameObject Green;
    #endregion

    #region Props
    public bool IsRedColor { get; private set; }
    public bool IsYellowColor { get; private set; }
    public bool IsGreenColor { get; private set; }
    #endregion

    // Use this for initialization
    void Start () {
        ShowRed();
	}

    public void ShowRed()
    {
        if (IsRedColor == false)
        {
            Red.SetActive(IsRedColor = true);
            Yellow.SetActive(IsYellowColor = false);
            Green.SetActive(IsGreenColor = false);
        }
    }
    public void ShowYellow()
    {
        if (IsYellowColor == false)
        {
            Red.SetActive(IsRedColor = false);
            Yellow.SetActive(IsYellowColor = true);
            Green.SetActive(IsGreenColor = false);
        }
    }
    public void ShowGreen()
    {
        if (IsGreenColor == false)
        {
            Red.SetActive(IsRedColor = false);
            Yellow.SetActive(IsYellowColor = false);
            Green.SetActive(IsGreenColor = true);
        }
    }
}
