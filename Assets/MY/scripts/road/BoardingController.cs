﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BoardingController : MonoBehaviour {

    #region Fields
    // pubs
    [Header("Config")]
    public Tile ParentTile;
    public ZoneType Type;
    public List<Bonus> BonusPrefabs = new List<Bonus>();
    public Transform ThrowBonusAt;

    [Header("Psngr")]
    public Spawner PassangerSpawner;
    public Vector3 PassangerPosLocal;
    public float BoardRadius = 1f;

    [Header("Visual")]
    public GameObject CircleMark;
    public GameObject ArrowMark;
    public Color Green = Color.green;
    public Color Red = Color.red;
    public float HideSpeed = 1;
    public string ShaderColorNameToChangeAlpha = "_TopColor";

    [Header("Events")]
    public UnityEvent OnCarEnterZone;
    public UnityEvent OnCarExitZone;


    private bool isGreen = false;
    private bool isRed = false;
    #endregion

    #region Props
    public Vector3 PassangerPosGlobal { get { return transform.TransformVector(PassangerPosLocal) + transform.position; } }

    public bool IsTargetUnboard { get; private set; }

    public Passanger Passanger { get; private set; }

    public bool Interactable { get; private set; }
    #endregion

    private void Start () {
        Interactable = true;

        //if (Type == ZoneType.Unboarding)
        //    HideArrowMark();
    }

    public void HideCircleMark () {
        if (Interactable)
            StartCoroutine("countHideCircleMark");
        //CircleMark.gameObject.SetActive(false);
    }
    private IEnumerator countHideCircleMark () {
        float crntHideTime = 0;

        Material circleMat = CircleMark.GetComponent<MeshRenderer>().material;
        string colorName = "_TopColor";
        Color color = circleMat.GetColor(colorName);

        while (true) {
            crntHideTime += Time.deltaTime * HideSpeed;
            if (crntHideTime >= 1)
                break;

            color.a -= Time.deltaTime * HideSpeed;

            circleMat.SetColor(colorName, color);

            yield return 0;
        }

        CircleMark.gameObject.SetActive(false);
    }
    public void ShowCircleMark () {
        if (Interactable)
            CircleMark.gameObject.SetActive(true);
    }

    public void MakeCircleGreen () {
        if (isGreen == false) {
            CircleMark.GetComponent<MeshRenderer>().material.SetColor("_TopColor", Green);
            isGreen = true;
            isRed = false;
        }
    }
    public void MakeCircleRed () {
        if (isRed == false) {
            CircleMark.GetComponent<MeshRenderer>().material.SetColor("_TopColor", Red);
            isRed = true;
            isGreen = false;
        }
    }

    public void ShowArrowMark () {
        ArrowMark.SetActive(true);
    }
    public void HideArrowMark () {
        ArrowMark.SetActive(false);
    }

    public void SetInteractable (bool value) {
        Interactable = value;
    }
    public void InteractOn () {
        SetInteractable(true);
    }
    public void InteractOff () {
        SetInteractable(false);
    }

    public void SetAsTargetUnboard () {

        if (Type == ZoneType.Unboarding) {

            IsTargetUnboard = true;

            MakeCircleGreen();
            ShowArrowMark();
        }
        else
            Debug.Log("MY: cannot set not Unboard type tile to target unboard");
    }
    
    public void PlacePassanger () {
        //psngr.transform.position = PassangerPosGlobal;

        Passanger = PassangerSpawner.LastSpawnedObject.GetComponent<Passanger>();
        Passanger.transform.position = PassangerPosGlobal;
        Passanger.BoardZone = this;
    }

    private void OnTriggerEnter (Collider other) {
        if (Interactable) {
            Vehicle car = other.GetComponent<Vehicle>();
            if (car != null) {
                OnCarEnterZone.Invoke();
            }
        }
    }
    private void OnTriggerStay (Collider other) {
        if (Interactable) {
            Vehicle car = other.GetComponent<Vehicle>();

            if (car != null) {

                if (car.IsBoardingNow == false && Vector3.Distance(transform.position, car.transform.position) <= BoardRadius) {
                    car.ToggleOnBrake();

                    if (Mathf.Round(car.CurrentSpeed) == 0) {

                        if (Type == ZoneType.Boarding)
                            StartCoroutine(countBoarding(car));

                        else if (Type == ZoneType.Unboarding)
                            StartCoroutine(countUnboarding(car));
                    }
                }
            }
        }
    }
    private IEnumerator countBoarding (Vehicle car) {
        car.IsBoardingNow = true;
        car.OpenDoor(Side.Right);
        if (car.TryBoardPassanger(Passanger)) {
            Passanger.StartBoarding(car, ParentTile);
        }

        while (Passanger.IsBoarding) {
            yield return 0;
        }

        car.ToggleOffBrake();
        car.CloseAllDoors();
        car.IsBoardingNow = false;
        Passanger = null;
        HideCircleMark();
        Interactable = false;
    }
    private IEnumerator countUnboarding (Vehicle car) {
        car.IsBoardingNow = true;
        car.OpenDoor(Side.Right);
        Passanger = car.TryUnboardPassanger();
        if (Passanger != null) {
            Passanger.StartUnboarding(this, ParentTile);
        }

        while (Passanger.IsUnboarding) {
            yield return 0;
        }

        car.IsBoardingNow = false;
        car.CloseAllDoors();
        car.ToggleOffBrake();
        HideCircleMark();
        Interactable = false;

        // inst bonus
        if (Passanger.HitsWhileInCar == 0) {
            Debug.Log("instantiate bonus: " + BonusPrefabs);
            Bonus bonus = Instantiate(BonusPrefabs[Random.Range(0, BonusPrefabs.Count)].gameObject).GetComponent<Bonus>();

            bonus.gameObject.SetActive(true);
            bonus.transform.position = Passanger.transform.position + new Vector3(0, 0.5f, 0);
            bonus.ThrowTo(ThrowBonusAt);
        }
    }

    private void OnTriggerExit (Collider other) {
        if (Interactable) {
            Vehicle car = other.GetComponent<Vehicle>();
            if (car != null) {
                car.CloseAllDoors();
                OnCarExitZone.Invoke();
            }
        }
    }

    private void OnDrawGizmos () {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position, 0.4f);

        Gizmos.color = Color.white;
        Gizmos.DrawSphere(PassangerPosGlobal, 0.2f);

        Gizmos.DrawLine(transform.position, transform.position + transform.forward * BoardRadius);
    }
}