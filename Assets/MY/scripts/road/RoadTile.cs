﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadTile : MonoBehaviour {

    #region Filds
    public bool TakeSelfAsDefault = true;
    public Transform DefaultRoad;
    public List<Transform> ReplaceRoads = new List<Transform>();

    [Range(0, 1)]
    public float ReplaceProbability = 1;

    public bool RandomYRotation = true;
    public float RandomYRotStep = 180;

    public bool ReplaceOnStart;


    private GameObject container = null;
    private bool isInited = false;
    #endregion

    public void Start()
    {
        init();

        if (ReplaceOnStart)
            Replace();
    }

    private void init()
    {
        if (!isInited)
        {
            if (TakeSelfAsDefault)
                DefaultRoad = transform;

            // если скрипт висит на дефолтной дороге, то создаем контейнер, 
            // перекидываем скрипт на контейнер и перемещаем дефолтную дорогу в контейнер
            if (this.transform == DefaultRoad.transform)
            {
                // создаем и позиционируем контейнер
                container = new GameObject();
                container.transform.SetParent(DefaultRoad.parent);
                container.transform.position = DefaultRoad.position;
                container.transform.rotation = DefaultRoad.rotation;

                // копируем в контейнер этот компонент
                RoadTile copyRoadTile = Util.CopyComponent(this, container);
                copyRoadTile.TakeSelfAsDefault = false;
                copyRoadTile.ReplaceOnStart = false;
                // удаляем этот компонент с дефолтной дороге
                Destroy(this);

                // перемещаем дефолтную дорогу в контейнер
                DefaultRoad.SetParent(container.transform);
            }
            else
                container = gameObject;

            isInited = true;
        }
    }

    public void Replace()
    {
        init();

        if (Random.Range(0f, 1f) <= ReplaceProbability)
        {
            GameObject replacePrefab = ReplaceRoads[Random.Range(0, ReplaceRoads.Count)].gameObject;

            replacePrefab.GetComponent<LightmappedPrefab>().GetRenderers();
            GameObject replaceInst = LightmappedPrefab.InstantiateWithLightmaps(replacePrefab.GetComponent<LightmappedPrefab>(), null).gameObject;
            //GameObject replaceInst = Instantiate(replacePrefab);
            replaceInst.transform.SetParent(container.transform);
            replaceInst.transform.localPosition = Vector3.zero;
            replaceInst.transform.localRotation = Quaternion.identity;

            if (RandomYRotation)
            {
                int rotRndThershold = (int)Mathf.Round(360 / RandomYRotStep);
                float rndRot = Random.Range(1, rotRndThershold) * RandomYRotStep;

                replaceInst.transform.localRotation = Quaternion.Euler(
                    replaceInst.transform.localRotation.x,
                    rndRot,
                    replaceInst.transform.localRotation.z);
            }

            // не робит. хз поч
            //Destroy(DefaultRoad.gameObject);
            DefaultRoad.gameObject.SetActive(false);
        }
    }
}
