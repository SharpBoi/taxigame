﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TileType { Forward = 0, Crossroad = 1, Boarding = 2, Unboarding = 3, Obstacle = 4, TurnLeft = 90, TurnRight = -90 }

public enum ZoneType { Boarding, Unboarding }

public enum Side { Left, Right, LeftRight, None }

public enum PhysicsEvent { Collision, Radius }

public enum TiretrackManagerPhysics { Collider, Wheel }

public enum BonusType { Invincibility, Fuel, SpeedBoost }