﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class LeaderboardNet : MonoBehaviour {

    #region Fields
    private static LeaderboardNet inst;


    public string DefaultServerUrl = "http://localhost:3000";
    public string ServerUrl = "http://";
    public bool UseDefaultUrl = true;
    #endregion

    #region Props
    public static LeaderboardNet Instance { get { return inst; } }

    public UserScore LastUser { get; private set; }
    #endregion

    private void Start()
    {
        if (ServerUrl[ServerUrl.Length - 1] == '/')
            ServerUrl = ServerUrl.Substring(0, ServerUrl.Length - 2);

        if (inst == null)
            inst = this;
        else
            throw new Exception("Allowed only one instance of Leaderboard Net");
    }

    public void GetLeaderboard()
    {
        StartCoroutine("getLeaderboard");
    }
    private IEnumerator getLeaderboard()
    {
        WWW w = null;
        if (UseDefaultUrl)
            w = new WWW(DefaultServerUrl + "/api/leaderboard/get");
        else
            w = new WWW(ServerUrl + "/api/leaderboard/get");

        while (!w.isDone)
            yield return 0;

        if (w.error == null)
        {
            string jsnStr = Encoding.UTF8.GetString(w.bytes);
            Debug.Log(jsnStr);
            Leaderboard leaderboard = JsonUtility.FromJson<Leaderboard>(jsnStr);

            if (OnGetLeaderboardDone != null)
                OnGetLeaderboardDone(leaderboard);

            Debug.Log("GET leaderboard successful");
        }
        else
            Debug.Log("GET leaderboard err: " + w.error);
        w.Dispose();

        yield return 0;
    }
    public event Action<Leaderboard> OnGetLeaderboardDone;

    public void UpdateLeaderboard(UserScore userScore)
    {
        LastUser = userScore;
        StartCoroutine("updateLeaderboard", userScore);
    }
    private IEnumerator updateLeaderboard(object value)
    {
        UserScore user = (UserScore)value;
        string jsn = JsonUtility.ToJson(user);


        WWW w = null;
        if (UseDefaultUrl)
            w = new WWW(DefaultServerUrl + "/api/leaderboard/update", Encoding.UTF8.GetBytes(jsn));
        else
            w = new WWW(ServerUrl + "/api/leaderboard/update", Encoding.UTF8.GetBytes(jsn));

        while (!w.isDone)
            yield return 0;
        
        Debug.Log("Leaderboard update done " + jsn);
        w.Dispose();

        yield return 0;
    }
}
