﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellMap
{
    #region Fields
    private List<Vector2> cells = new List<Vector2>();

    public Vector2 Size { get; private set; }
    #endregion

    #region Props
    public Vector2[] Cells { get { return cells.ToArray(); } }
    #endregion

    #region Funcs
    public void AddCell(Vector2 pos)
    {
        cells.Add(pos);

        updateSize();
    }
    public void AddCell(float x, float y)
    {
        AddCell(new Vector2(x, y));
    }

    public int CellIndexAt(float x, float y)
    {
        for (int i = 0; i < cells.Count; i++)
            if (cells[i].x == x && cells[i].y == y)
                return i;

        return -1;
    }
    public int CellIndexAt(Vector2 pos)
    {
        return CellIndexAt(pos.x, pos.y);
    }
    
    /// <summary>
    /// Возворащает индекс тайла, лежащий в указанном направлении из указанной точки
    /// </summary>
    /// <param name="from"></param>
    /// <param name="dir"></param>
    /// <param name="distance"></param>
    /// <param name="maxCheckLength"></param>
    /// <returns></returns>
    public int CellIndexAtDirection(Vector2 from, Vector2 dir, out int distance, int maxCheckLength = 100)
    {
        if (dir.x != 0 && dir.y != 0)
            throw new ArgumentException("direction cannot represent non-straight angle");

        distance = -1;

        for (int i = 1; i <= maxCheckLength; i++)
        {
            int index = CellIndexAt(from + dir * i);
            if (index != -1)
            {
                distance = i;
                return index;
            }
        }

        return -1;
    }
    public int CellIndexAtDirection(Vector2 from, Vector2 dir, int maxCheckLength = 100)
    {
        int dist = 0;
        return CellIndexAtDirection(from, dir, out dist, maxCheckLength);
    }

    public void RemoveCell(int x, int y)
    {
        for (int i = 0; i < cells.Count; i++)
            if (cells[i].x == x && cells[i].y == y)
                cells.RemoveAt(i);

        updateSize();
    }
    public void RemoveCell(Vector2Int pos)
    {
        RemoveCell(pos.x, pos.y);
    }
    public void RemoveCell(int index)
    {
        cells.RemoveAt(index);
        updateSize();
    }

    private void updateSize()
    {
        Vector2 min = new Vector2(int.MaxValue, int.MaxValue);
        Vector2 max = new Vector2();

        for (int i = 0; i < cells.Count; i++)
        {
            Vector2 cell = cells[i];

            if (min.x > cell.x) min.x = cell.x;
            if (min.y > cell.y) min.y = cell.y;

            if (max.x < cell.x) max.x = cell.x;
            if (max.y < cell.y) max.y = cell.y;
        }

        Size = max - min;
    }
    #endregion
}
