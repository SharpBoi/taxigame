﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor.SceneManagement;
using UnityEditor;
#endif

public class MapGenerator : MonoBehaviour {

    #region Fields
    // pubs
    public Transform SourceTilesContainer;

    public TileMapTarget TargetGenerateTo;

    [Header("Map gen config")]
    public int ForwardCountAfterTurn = 2;
    public int PregenerateCount = 20;
    public int OnwardTilesCount = 10;
    public int BackwardTilesCount = 5;

    [Header("Vehicles gen config")]
    public bool GenerateVehicles = false;
    public List<VehiclePathRider> VehiclesPrefabs = new List<VehiclePathRider>();
    [Range(0, 1)]
    public float VehicleGenProbability;
    public int VehiclesCountPerGen = 1;

    [Header("Tile gen config")]
    [Range(0, 1)]
    public float BrokenRoadGenProbab = 0;

    [HideInInspector]
    public List<TileTypeProbability> TilesTypesProbs = new List<TileTypeProbability>();

    [Header("Events")]
    public UnityEvent OnTileGenerated;


    // privs
    private Tile[] allSrcTiles;
    private Dictionary<TileType, List<Tile>> tilesByTypes = new Dictionary<TileType, List<Tile>>();
    private Dictionary<TileType, List<Tile>> spawnableTilesByTypes = new Dictionary<TileType, List<Tile>>();

    private List<TileData> allTilesHistory = new List<TileData>();
    private List<Tile> crntTiles = new List<Tile>();
    private Transform crntTilesContainer;

    private CellMap map = new CellMap();
    private int crntForwardCount = 0;
    #endregion

    #region Props
    public Tile[] Tiles { get { return crntTiles.ToArray(); } }
    public TileData[] AllTilesHistory { get { return allTilesHistory.ToArray(); } }

    public bool CanGenUnboard { get; set; }
    public bool CanGenBoard { get; set; }
    #endregion

    // Use this for initialization
    void Start () {
        allSrcTiles = SourceTilesContainer.GetComponentsInChildren<Tile>(true);
        SourceTilesContainer.gameObject.SetActive(false);

        for (int i = 0; i < allSrcTiles.Length; i++) {
            // fill only spawnable tiles
            if (allSrcTiles[i].Spawnable) {
                if (spawnableTilesByTypes.ContainsKey(allSrcTiles[i].Type) == false) {
                    List<Tile> tiles = new List<Tile>();
                    tiles.Add(allSrcTiles[i]);
                    spawnableTilesByTypes.Add(allSrcTiles[i].Type, tiles);
                }
                else {
                    spawnableTilesByTypes[allSrcTiles[i].Type].Add(allSrcTiles[i]);
                }
            }

            // fill all tiles by types
            if (tilesByTypes.ContainsKey(allSrcTiles[i].Type) == false) {
                List<Tile> tiles = new List<Tile>();
                tiles.Add(allSrcTiles[i]);
                tilesByTypes.Add(allSrcTiles[i].Type, tiles);
            }
            else {
                tilesByTypes[allSrcTiles[i].Type].Add(allSrcTiles[i]);
            }
        }

        crntTilesContainer = new GameObject().transform;
        crntTilesContainer.name = "current_tile_container";

        CanGenBoard = true;

        // pre generation
        for (int i = 0; i < PregenerateCount; i++)
            while (TryGenerateNextTile() == false) ;
    }

    private void FixedUpdate () {
        Tile crntTile = getTileThatContainsTarget();
        if (crntTile != null)
            TargetGenerateTo.SetCurrentTile(crntTile.Data);

        if (crntTile != null) {
            // generate onward
            Tile lastTile = crntTiles[crntTiles.Count - 1];
            if (lastTile.Index - crntTile.Index < OnwardTilesCount)
                for (int i = 0; i < OnwardTilesCount - (lastTile.Index - crntTile.Index); i++)
                    TryGenerateNextTile();

            // remove backward
            Tile firstTile = crntTiles[0];
            if (crntTile.Index - firstTile.Index > BackwardTilesCount)
                for (int i = 0; i < crntTile.Index - firstTile.Index - BackwardTilesCount; i++)
                    RemoveTile(0);

            //// удаляем все тайлы, до первого предыдущего поворота
            //Tile turnTile = null;
            //for (int i = crntTiles.Count - 1; i >= 0; i--)
            //    if (crntTiles[i].Index < crntTile.Index &&
            //        (crntTiles[i].Type == TileType.TurnLeft || crntTiles[i].Type == TileType.TurnRight)) {
            //        turnTile = crntTiles[i];
            //        break;
            //    }

            //if (turnTile != null)
            //    if (crntTile.Index > turnTile.Index)
            //        for (int i = 0; i < crntTiles.Count; i++) {
            //            if (crntTiles[0].Index != turnTile.Index)
            //                RemoveTile(0);
            //            else
            //                break;
            //        }
        }
    }

    public bool TryGenerateNextTile () {
        #region Work variant
        Tile nextTilePrefab = null;

        if (map.Cells.Length == 0 && crntTiles.Count == 0) // первая точка(перывй тайл)
        {
            //nextTilePrefab = GetRandomExampleTileByItsType(TileType.TurnLeft, false);
            nextTilePrefab = allSrcTiles[Random.Range(0, allSrcTiles.Length)];

            if (nextTilePrefab.IsStartTile == false)
                return false;

            nextTilePrefab.transform.rotation = Quaternion.identity;
            nextTilePrefab.GetComponent<LightmappedPrefab>().GetRenderers();
            Tile tileInst = LightmappedPrefab.InstantiateWithLightmaps(nextTilePrefab.GetComponent<LightmappedPrefab>(), null).GetComponent<Tile>();
            tileInst.gameObject.SetActive(true);
            tileInst.ReplaceRoads(BrokenRoadGenProbab);

            tileInst.transform.SetParent(crntTilesContainer);
            tileInst.transform.rotation = Quaternion.Euler(0, 0, 0);
            tileInst.transform.position = Vector3.zero;

            map.AddCell(Vector2Int.zero);
            crntTiles.Add(tileInst);
            nextTilePrefab = null;

            return true;
        }
        else {
            // поля
            Vector2 crntPoint = new Vector2Int();
            Vector2 nextPoint = new Vector2Int();
            Tile crntTile = null;

            // вычисляем тип следующего тайла с вероятностью
            TileType genType = genTileType();

            // Вызывает ошибку генерации - карта не генерится
            if (genType == TileType.Forward)
                crntForwardCount++;
            else {
                if (crntForwardCount < ForwardCountAfterTurn)
                    return false;
                crntForwardCount = 0;
            }

            // получаем текущую точку тайла на 2Д карте и текущий тайл
            crntPoint = map.Cells[map.Cells.Length - 1];
            crntTile = crntTiles[crntTiles.Count - 1];

            // генерим следующую точку, и получаем префаб следующего тайла
            nextPoint = crntPoint + crntTile.TurnDir;
            nextTilePrefab = GetRandomExampleTileByItsType(genType, true);
            nextTilePrefab.transform.rotation = Quaternion.identity;
            if (nextTilePrefab == null)
                return false;

            if (nextTilePrefab.IsStartTile)
                return false;

            // считаем направление следующего тайла
            Vector2Int nextTileDir = new Vector2Int(
                (int)Mathf.Cos((nextTilePrefab.transform.rotation.eulerAngles.y - nextTilePrefab.TurnAngle) * Mathf.Deg2Rad),
                (int)Mathf.Sin((nextTilePrefab.transform.rotation.eulerAngles.y - nextTilePrefab.TurnAngle) * Mathf.Deg2Rad));

            // чекаем, можно ли поставить некст тайл указывающий в некст направление, в некст точку
            if (map.CellIndexAtDirection(nextPoint, nextTileDir, 100) != -1)
                return false;

            // some flag filters

            if (CanGenUnboard == false && genType == TileType.Unboarding)
                return false;
            if (CanGenBoard == false && genType == TileType.Boarding)
                return false;

            // Вызывает ошибку генерации - карта не генерится, я хуй знает
            //if (canGenForwardAfterTurn)
            //{
            //    if (genType == TileType.Forward)
            //        crntForwardCount++;
            //    else
            //        return false;

            //    if (crntForwardCount >= ForwardCountAfterTurn)
            //    {
            //        canGenForwardAfterTurn = false;
            //        crntForwardCount = 0;
            //    }
            //}


            // инстансим некст тайл от ранее найденного шаблонного тайла
            //Tile nextTilInst = Instantiate(nextTileExample);
            nextTilePrefab.GetComponent<LightmappedPrefab>().GetRenderers();
            Tile nextTileInst = LightmappedPrefab.InstantiateWithLightmaps<LightmappedPrefab>(nextTilePrefab.GetComponent<LightmappedPrefab>(), null).GetComponent<Tile>();
            nextTileInst.gameObject.SetActive(true);
            nextTileInst.transform.SetParent(crntTilesContainer);
            nextTileInst.Index = (crntTile.Index + 1);
            nextTileInst.Map = this;
            nextTileInst.PrevTile = crntTiles[crntTiles.Count - 1];
            nextTileInst.ReplaceRoads(BrokenRoadGenProbab);
            nextTileInst.GetPath();

            nextTileInst.transform.rotation = Quaternion.Euler(0, crntTile.transform.eulerAngles.y - crntTile.TurnAngle, 0);
            nextTileInst.transform.position = new Vector3(nextPoint.x, 0, nextPoint.y) * (nextTilePrefab.Size / 2 + crntTile.Size / 2);

            crntTiles.Add(nextTileInst);
            map.AddCell(nextPoint);
            OnTileGenerated.Invoke();

            // плейсим машинки на тайлы
            if (GenerateVehicles &&
                crntTiles[crntTiles.Count - 1].Path != null && Random.Range(0f, 1f) <= VehicleGenProbability) {

                Tile lastTile = crntTiles[crntTiles.Count - 1];
                PathBuilder tilePath = lastTile.Path;

                Vector3 pos, look;
                PathNode node;

                float offset = 1f / VehiclesCountPerGen;
                for (int i = 0; i < VehiclesCountPerGen; i++) {

                    GameObject vehGOPrefab = VehiclesPrefabs[Random.Range(0, VehiclesPrefabs.Count)].gameObject;

                    GameObject vehGOInst = Instantiate(vehGOPrefab);
                    VehiclePathRider vehInst = vehGOInst.GetComponent<VehiclePathRider>();

                    if (i == 0)
                        tilePath.GetWalkPoseByCoef(0.1f, false, out pos, out look, out node);
                    else
                        tilePath.GetWalkPoseByCoef(offset * i, false, out pos, out look, out node);


                    vehGOInst.transform.position = pos;
                    vehGOInst.transform.rotation = Quaternion.LookRotation(look);
                    vehGOInst.transform.SetParent(lastTile.transform);

                    vehInst.StartRide(tilePath);
                }
            }


            // сохраняем краткую историю всех инстансированных тайлов
            bool contains = false;
            for (int i = 0; i < allTilesHistory.Count; i++)
                if (allTilesHistory[i].Index == nextTileInst.Index) {
                    contains = true;
                    break;
                }
            if (contains == false)
                allTilesHistory.Add(new TileData() { Index = nextTileInst.Index, Type = nextTileInst.Type, Self = nextTileInst });

            if (nextTileInst.Type == TileType.TurnLeft || nextTileInst.Type == TileType.TurnRight) {
                crntForwardCount = 0;
            }
            return true;
        }
        #endregion
    }

    private TileType genTileType () {

        // sort
        TileTypeProbability[] newTtps = Util.StairSort(TilesTypesProbs, (ttp) => { return ttp.Probability; });
        TilesTypesProbs.Clear();
        TilesTypesProbs.AddRange(newTtps);

        float totalMax = 0;
        float[] probs = new float[TilesTypesProbs.Count];
        for (int i = 0; i < TilesTypesProbs.Count; i++) {
            totalMax += TilesTypesProbs[i].Probability;
            probs[i] = TilesTypesProbs[i].Probability;
        }

        float rnd = Random.Range(0f, 1f);

        // normalize
        for (int i = 0; i < probs.Length; i++) {
            probs[i] /= totalMax;
        }

        // offset
        for (int i = 1; i < probs.Length; i++) {
            probs[i] += probs[i - 1];
        }

        for (int i = 1; i < probs.Length; i++) {

            if (probs[i - 1] < rnd && rnd < probs[i])
                return TilesTypesProbs[i].Type;

        }

        //float prevProb = 0;
        //for (int i = 0; i < TilesTypesProbs.Count; i++) {
        //    if (rnd <= TilesTypesProbs[i].Probability / totalMax + prevProb)
        //        return TilesTypesProbs[i].Type;

        //    prevProb += TilesTypesProbs[i].Probability / totalMax + prevProb;
        //}

        return TileType.Forward;
    }
    private float calcCrntRoadDegree () {
        float ret = 0;
        for (int i = 0; i < crntTiles.Count; i++)
            ret += crntTiles[i].TurnAngle;

        return ret;
    }
    private Tile getTileThatContainsTarget () {
        for (int i = 0; i < crntTiles.Count; i++)
            if (crntTiles[i].ContainsPoint(TargetGenerateTo.transform.position))
                return crntTiles[i];

        return null;
    }

    private Tile GetRandomExampleTileByItsType (TileType type, bool checkSpawnable) {
        if (tilesByTypes.ContainsKey(type) == false)
            throw new System.ArgumentException("there is no tiles with passed type");

        if (checkSpawnable) {
            if (spawnableTilesByTypes.ContainsKey(type)) {
                List<Tile> tiles = spawnableTilesByTypes[type];
                return tiles[Random.Range(0, tiles.Count)];
            }
            return null;
        }
        else {
            List<Tile> tiles = tilesByTypes[type];
            return tiles[Random.Range(0, tiles.Count)];
        }
    }
    private void RemoveTile (int index) {
        Destroy(crntTiles[index].gameObject);
        crntTiles.RemoveAt(index);
        map.RemoveCell(index);
    }

    public void EnableVehiclesGen () {
        GenerateVehicles = true;
    }
    public void DisableVehiclesGen () {
        GenerateVehicles = false;
    }

    // test
    private void OnDrawGizmos () {
        Gizmos.color = Color.red;
        for (int i = 0; i < map.Cells.Length; i++) {
            Gizmos.DrawSphere(new Vector3(
                map.Cells[i].x,
                10,
                map.Cells[i].y),
                0.5f);
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(MapGenerator))]
public class MapGeneratorInspector : Editor {
    public override void OnInspectorGUI () {
        DrawDefaultInspector();
        List<TileTypeProbability> probs = (target as MapGenerator).TilesTypesProbs;

        GUILayout.BeginVertical("HelpBox");
        {
            GUILayout.Label("Tiles types and generations probabilities");

            if (GUILayout.Button("+"))
                probs.Add(new TileTypeProbability() { Probability = 0.1f, Type = TileType.Forward });
            if (GUILayout.Button("-") && probs.Count > 0)
                probs.RemoveAt(probs.Count - 1);

            for (int i = 0; i < probs.Count; i++) {
                GUILayout.BeginVertical(EditorStyles.helpBox);
                {
                    TileTypeProbability prob = probs[i];
                    prob.DifficultChangeMultiplier = EditorGUILayout.FloatField("Difficult change multiplier", prob.DifficultChangeMultiplier);
                    prob.Type = (TileType)EditorGUILayout.EnumPopup("Tile type", prob.Type);
                    prob.StartProbability = EditorGUILayout.FloatField("Start Probability", prob.StartProbability);
                    prob.Probability = EditorGUILayout.FloatField("Probability", prob.Probability);
                    if (GUILayout.Button("x")) {
                        probs.RemoveAt(i);
                        break;
                    }
                    probs[i] = prob;
                }
                GUILayout.EndVertical();
            }
        }
        GUILayout.EndVertical();

        (target as MapGenerator).TilesTypesProbs = probs;

        if (GUI.changed && Application.isPlaying == false) {
            serializedObject.ApplyModifiedProperties();
            EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
        }
    }
}
#endif

[System.Serializable]
public class TileTypeProbability {

    [SerializeField]
    public float DifficultChangeMultiplier = 1;

    [SerializeField]
    public TileType Type;

    [SerializeField]
    public float StartProbability;

    [SerializeField]
    public float Probability;

    public void SetProb (float value) {
        Probability = value;
    }
    public void ApplyProb (float value) {
        Probability += value;
    }

    public override string ToString () {
        return Type + "; " + Probability;
    }
}