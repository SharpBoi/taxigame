﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

//[System.Serializable]
//public struct TileData
//{
//    [Header("Size")]
//    public float Height;
//    public float Size;

//    [Header("Tile config")]
//    public TileType Type;
//    public bool IsStartTile;

//    public bool Spawnable;

//    public int Index;
//}

public class Tile : MonoBehaviour {

    #region Fields
    [Header("Size")]
    public float Height;
    public float Size;

    [Header("Tile config")]
    public TileType Type = TileType.Forward;

    public bool IsStartTile = false;
    public bool Spawnable = true;

    public bool RequireNextTileType;
    public TileType NextTileType;

    public bool ReplaceRoadsOnStart = false;
    [Range(0, 1)]
    public float ChangeRoadProbability;

    [Header("Tile content")]
    public Transform ReturnPoint;
    public Transform StartPoint;

    public BoardingController BoardController;

    public PathBuilder Path;
    public bool AutoSearchPath = true;

    [Header("World")]
    public MapGenerator Map;
    public Tile PrevTile;

    // tile size
    private float
        right, left,
        top, bottom,
        forw, backw;
    private float halfSize = 0;

    private List<Vehicle> crntVehicles = new List<Vehicle>();
    private RoadTile[] roadTiles;

    private TileData data = new TileData();
    #endregion

    #region Props
    public bool IsWasContainsCar { get; private set; }

    public int TurnAngle {
        get {
            if (Type == TileType.TurnLeft)
                return 90;
            else if (Type == TileType.TurnRight)
                return -90;

            return 0;
        }
    }
    public Vector2 TurnDir {
        get {
            return new Vector2(
                Mathf.Cos((transform.rotation.eulerAngles.y - TurnAngle) * Mathf.Deg2Rad),
                -Mathf.Sin((transform.rotation.eulerAngles.y - TurnAngle) * Mathf.Deg2Rad));
        }
    }

    public TileData Data { get { return data; } }
    public int Index { get { return data.Index; } set { data.Index = value; } }
    #endregion

    private void Start () {
        BoxCollider box = gameObject.AddComponent<BoxCollider>() as BoxCollider;
        box.center = new Vector3(0, 0, 0);
        box.size = new Vector3(Size, Height * 2, Size);
        box.isTrigger = true;

        if (AutoSearchPath) {
            GetPath();
        }

        data = new TileData() {
            GotHitHere = false,
            Index = Index,
            Type = Type,
            ReturnPoint = ReturnPoint.transform.position,
            Self = this
        };

        if (ReplaceRoadsOnStart) {
            roadTiles = GetComponentsInChildren<RoadTile>();
            for (int i = 0; i < roadTiles.Length; i++) {
                roadTiles[i].ReplaceProbability = ChangeRoadProbability;
                roadTiles[i].Replace();
            }
        }
    }

    public bool ContainsPoint (Vector3 point) {
        halfSize = Size / 2;

        right = transform.position.x + halfSize;
        left = transform.position.x - halfSize;

        top = transform.position.y + Height / 2;
        bottom = transform.position.y - Height / 2;

        forw = transform.position.z + halfSize;
        backw = transform.position.z - halfSize;

        if (left < point.x && point.x < right &&
            bottom < point.y && point.y < top &&
            backw < point.z && point.z < forw)
            return true;

        return false;
    }

    public void ReplaceRoads (float replaceProbab) {
        roadTiles = GetComponentsInChildren<RoadTile>();
        if (roadTiles != null)
            for (int i = 0; i < roadTiles.Length; i++) {
                roadTiles[i].ReplaceProbability = replaceProbab;
                roadTiles[i].Replace();
            }
    }

    public void GetPath () {
        PathBuilder[] paths = GetComponentsInChildren<PathBuilder>();
        if (paths.Length >= 1)
            Path = paths[0];
    }

    private void OnTriggerEnter (Collider other) {
        Vehicle car = other.GetComponent<Vehicle>();
        if (car != null) {
            if (crntVehicles.Contains(car) == false)
                crntVehicles.Add(car);

            VehiclePathRider rider = car.GetComponent<VehiclePathRider>();
            if (rider != null) {
                if (Path != null)
                    rider.StartRide(Path);
            }
        }
    }
    private void OnTriggerExit (Collider other) {
        Vehicle car = other.GetComponent<Vehicle>();
        if (car != null) {
            crntVehicles.Remove(car);
        }
    }

    private void OnDrawGizmos () {

        #region Draw bound box
        Gizmos.DrawWireCube(transform.position + new Vector3(0, Height / 2, 0), new Vector3(Size, Height, Size));
        #endregion

        #region Draw turn lines
        Vector3 from = transform.TransformVector(new Vector3(-Size / 2, Height, 0)) + transform.position;
        Vector3 to = Vector3.zero;

        if (Type != TileType.TurnLeft && Type != TileType.TurnRight) {
            to = transform.TransformVector(new Vector3(Size / 2, Height, 0)) + transform.position;
        }
        if (Type == TileType.TurnLeft) {
            to = transform.TransformVector(new Vector3(0, Height, Size / 2)) + transform.position;
        }
        if (Type == TileType.TurnRight) {
            to = transform.TransformVector(new Vector3(0, Height, -Size / 2)) + transform.position;
        }

        Gizmos.color = Color.red;
        Gizmos.DrawLine(from, to);
        Gizmos.DrawSphere(to, 0.3f);

        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(from, 0.3f);
        #endregion

        #region Draw return point
        if (ReturnPoint != null)
            Gizmos.DrawSphere(ReturnPoint.position, 0.4f);
        #endregion

        #region Draw start point
        if (StartPoint != null) {
            Gizmos.DrawSphere(StartPoint.transform.position, 0.2f);
            Gizmos.DrawLine(StartPoint.transform.position, StartPoint.transform.position + StartPoint.transform.forward);
        }

        #endregion
    }
}

[System.Serializable]
public class TileData : ICloneable {
    public int Index;
    public TileType Type;
    public bool GotHitHere;

    public Vector3 ReturnPoint;

    public Tile Self;

    public object Clone () {
        return new TileData() {
            GotHitHere = GotHitHere,
            Index = Index,
            ReturnPoint = ReturnPoint,
            Type = Type
        };
    }
    public void SetGotHit (bool val) {
        GotHitHere = val;
    }

    public override string ToString () {
        return Index + "; " + Type;
    }
}

#if UNITY_EDITOR
//[CustomEditor(typeof(Tile))]
//[CanEditMultipleObjects]
//public class TileInspector : Editor
//{
//    public override void OnInspectorGUI()
//    {
//        DrawDefaultInspector();

//        Tile tgt = target as Tile;

//        if (tgt.RequireNextTileType)
//        {
//            tgt.NextTileType = (TileType)EditorGUILayout.EnumPopup("Next tile type", tgt.NextTileType);
//        }

//        if (tgt.Type == TileType.Boarding || tgt.Type == TileType.Unboarding)
//        {
//            tgt.BoardController = EditorGUILayout.ObjectField("Boarding Controller", tgt.BoardController, typeof(BoardingController), true) as BoardingController;
//        }
//    }
//}
#endif