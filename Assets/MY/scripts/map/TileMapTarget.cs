﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileMapTarget : MonoBehaviour {

    #region Fields
    private List<TileData> drivedTiles = new List<TileData>();
    private List<TileData> drivedBoardTiles = new List<TileData>();
    private List<TileData> drivedUnboardTiles = new List<TileData>();
    private List<TileData> tilesWhereHit = new List<TileData>();

    private float prevDist;
    private float dist;
    private float dDist;
    private TileData crntTile = new TileData();
    private TileData prevTile = new TileData();
    #endregion
    
    #region Props
    public TileData CurrentTile { get { return crntTile; } }
    public TileData PrevTile { get { return prevTile; } }
    public TileData[] DrivedTiles { get { return drivedTiles.ToArray(); } }
    public TileData[] DrivedBoardTiles { get { return drivedBoardTiles.ToArray(); } }
    public TileData[] DrivedUnboardTiles { get { return drivedUnboardTiles.ToArray(); } }
    public TileData[] TilesWhereHit { get { return tilesWhereHit.ToArray(); } }

    public float OnTileDeltaMoving { get { return Mathf.Round(dDist * 100); } }
    /// <summary>
    /// Возвращает 1, если объект движется вперед по тайлу, иначе -1. Движение считается относительно точки возврата на тайле
    /// </summary>
    public sbyte OnTileMoveDirection { get { return (sbyte)Mathf.Sign(OnTileDeltaMoving); } }
    #endregion

    private void FixedUpdate()
    {
        prevDist = dist;
        dist = Vector3.Distance(transform.position, CurrentTile.ReturnPoint);
        dDist = dist - prevDist;
    }

    public bool SetCurrentTile(TileData data)
    {
        if (crntTile.Index != data.Index)
        {
            prevTile = crntTile;
            crntTile = data;
        }

        if (drivedTiles.Count == 0)
            drivedTiles.Add(data);
        else
        {
            if (drivedTiles[drivedTiles.Count - 1].Index != data.Index)
            {
                drivedTiles.Add(data);

                if (data.Type == TileType.Boarding)
                    drivedBoardTiles.Add(data);

                if (data.Type == TileType.Unboarding)
                    drivedUnboardTiles.Add(data);

                return true;
            }
        }
        return false;
    }

    public void ClearHistory()
    {
        drivedTiles.Clear();
        drivedBoardTiles.Clear();
        drivedUnboardTiles.Clear();
        tilesWhereHit.Clear();
    }

    public void RegisterHitOnCurrentTile()
    {
        bool contains = false;
        for (int i = 0; i < drivedTiles.Count; i++)
            if (drivedTiles[i].Index == crntTile.Index)
            {
                drivedTiles[i].SetGotHit(true);

                for (int j = 0; j < tilesWhereHit.Count; j++)
                    if (tilesWhereHit[j].Index == drivedTiles[i].Index)
                    {
                        tilesWhereHit[j].SetGotHit(true);
                        contains = true;
                        break;
                    }

                if (!contains)
                    tilesWhereHit.Add(drivedTiles[i]);

                break;
            }
    }
    public void ClearTilesWhereHit()
    {
        tilesWhereHit.Clear();
    }
}