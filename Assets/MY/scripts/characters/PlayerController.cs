﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerController : MonoBehaviour {

    #region Fields
    public bool UserControlEnabled = true;

    public GameController Game;
    public MapGenerator Map;
    public Vehicle Vehicle;

    public SteeringWheel SteerWheel;
    public MyButton BtnFwd;
    public MyButton BtnBwd;

    public UnityEvent BeforeDestroy;


    private bool canDrive = true;
    #endregion

    void Start () {
    }

    private void FixedUpdate () {
        if (Vehicle.IsBoardingNow)
            canDrive = false;
        else
            canDrive = true;

        if (UserControlEnabled) {
            UpdateMobileInput();

            if (BtnBwd.IsHolding == false && BtnFwd.IsHolding == false && SteerWheel.IsHolding == false)
                UpdateKeyboardInput();
        }
    }
    private void OnDestroy () {
        BeforeDestroy.Invoke();
    }

    private void UpdateMobileInput () {

        // drive
        if (Vehicle.HasFuel && canDrive && Vehicle.IsOnGround) {
            if (BtnFwd.IsHolding) {
                if (Mathf.Round(Vehicle.CurrentSignedSpeed) < 0)
                    Vehicle.ToggleOnBrake();
                else
                    Vehicle.ToggleOffBrake();

                Vehicle.ApplyDrive(1);
            }

            else if (BtnBwd.IsHolding) {

                if (Mathf.Round(Vehicle.CurrentSignedSpeed) > 0)
                    Vehicle.ToggleOnBrake();
                else
                    Vehicle.ToggleOffBrake();

                Vehicle.ApplyDrive(-1);
            }

            else {
                Vehicle.UpdateDriveZeroing();
            }
        }

        // steer
        Vehicle.Steer(-SteerWheel.Degree);

        // air stabil
        if (Vehicle.IsOnGround == false) {
            if (BtnFwd.IsHolding)
                Vehicle.Rigid.AddForceAtPosition(-transform.up * 0.05f, transform.position + transform.forward, ForceMode.VelocityChange);

            if (BtnBwd.IsHolding)
                Vehicle.Rigid.AddForceAtPosition(transform.up * 0.05f, transform.position + transform.forward, ForceMode.VelocityChange);

            if (SteerWheel.IsHolding) {

                Vehicle.transform.Rotate(Vector3.up, -(SteerWheel.Degree / SteerWheel.DegreeLimits));

                //Vehicle.Rigid.AddForceAtPosition(transform.right * (SteerWheel.Degree / SteerWheel.DegreeLimits) * 0.05f,
                //    transform.position + Vehicle.Rigid.centerOfMass + transform.forward, ForceMode.VelocityChange);

                //Vehicle.Rigid.AddForceAtPosition(-transform.right * (SteerWheel.Degree / SteerWheel.DegreeLimits) * 0.05f,
                //    transform.position + Vehicle.Rigid.centerOfMass + transform.forward, ForceMode.VelocityChange);
            }
        }
    }

    private void UpdateKeyboardInput () {
        // keyboard control
        // back on closest tile
        if (Input.GetKeyDown(KeyCode.R))
            Game.ReplacePlayerOnTile(0, true);

        // drive
        bool isDriving = false;
        if (Vehicle.HasFuel && canDrive && Vehicle.IsOnGround) {
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) {
                if (Mathf.Round(Vehicle.CurrentSignedSpeed) < 0)
                    Vehicle.ToggleOnBrake();
                else {
                    if (Vehicle.IsBrakeToggled)
                        Vehicle.ToggleOffBrake();
                    Vehicle.ApplyDrive(1);
                }

                isDriving = true;
            }
            if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) {
                if (Mathf.Round(Vehicle.CurrentSignedSpeed) > 0)
                    Vehicle.ToggleOnBrake();
                else {
                    if (Vehicle.IsBrakeToggled)
                        Vehicle.ToggleOffBrake();
                    Vehicle.ApplyDrive(-1);
                }
                isDriving = true;
            }
        }
        if (!isDriving)
            Vehicle.UpdateDriveZeroing();

        if (Input.GetKey(KeyCode.Space))
            Vehicle.ToggleOnBrake();
        if (Input.GetKeyUp(KeyCode.Space))
            Vehicle.ToggleOffBrake();

        if (Vehicle.IsBrakeToggled)
            Vehicle.UpdateDriveZeroing();

        // steering
        bool isSteering = false;
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) {
            Vehicle.ApplySteering(1);
            isSteering = true;
        }
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) {
            Vehicle.ApplySteering(-1);
            isSteering = true;
        }
        if (!isSteering)
            Vehicle.UpdateSteeringZeroing();

        // fly wasd stabil
        if (Vehicle.IsOnGround == false) {
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
                Vehicle.Rigid.AddForceAtPosition(-transform.up * 0.05f, transform.position + transform.forward, ForceMode.VelocityChange);

            if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
                Vehicle.Rigid.AddForceAtPosition(transform.up * 0.05f, transform.position + transform.forward, ForceMode.VelocityChange);

            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
                Vehicle.transform.Rotate(Vector3.up, 1);
            //Vehicle.Rigid.AddForceAtPosition(transform.right * 0.05f, transform.position + Vehicle.Rigid.centerOfMass + transform.forward, ForceMode.VelocityChange);
            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
                Vehicle.transform.Rotate(Vector3.up, -1);
            //    Vehicle.Rigid.AddForceAtPosition(-transform.right * 0.05f, transform.position + Vehicle.Rigid.centerOfMass + transform.forward, ForceMode.VelocityChange);
        }
    }
}
