﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class PassangerEvent : UnityEvent<Passanger> { }

#if UNITY_EDITOR
[CanEditMultipleObjects]
#endif
public class Passanger : MonoBehaviour {

    public static List<Passanger> all = new List<Passanger>();

    #region Fields
    [Header("Passanger")]
    public ParticleSystem Blood;
    public AudioSource Scream;
    public AudioSource SayHi;
    public Vector3 ReplicaPntLocal;

    [Header("Boarding stuff")]
    public float BoardingTimeSec = 2;
    public float Payment = 400f;
    public float PaymentDecreasement = 10;
    public BoardingController BoardZone;

    [Header("Events")]
    public UnityEvent OnStartBoarding;
    public UnityEvent OnBoarded;
    public UnityEvent OnStartUnboarding;
    public UnityEvent OnUnboarded;
    public UnityEvent OnHit;


    private Vehicle boardCar;
    private BoardingController unboardZone;

    private Animator anim;
    private Rigidbody rigid;
    private new Collider collider;
    private bool screamed = false;
    #endregion

    #region Props
    public Vector3 ReplicaPntGlobal { get { return transform.TransformVector(ReplicaPntLocal) + transform.position; } }
    public Vector3 BeginPos { get; private set; }
    public Vector3 BeginScale { get; private set; }
    public bool IsBoarded { get; private set; }
    public bool IsBoarding { get; private set; }
    public bool IsUnboarding { get; private set; }

    public Tile UnboardTile { get; private set; }
    public Tile BoardTile { get; private set; }
    public int BoardTileIndex { get; private set; }

    public float CurrentPayment { get; private set; }
    public float CurrentPaymentNormalized { get { return CurrentPayment / Payment; } }
    public int HitsWhileInCar { get; private set; }
    #endregion

    // Use this for initialization
    void Start () {

        all.Add(this);

        BeginPos = transform.position;
        BeginScale = transform.localScale;

        CurrentPayment = Payment;

        collider = GetComponent<Collider>();
        anim = GetComponent<Animator>();
        rigid = GetComponent<Rigidbody>();

        rigid.Sleep();
    }
    private void OnDestroy () {
        all.Remove(this);
    }
    private void Update () {
        if (!screamed)
            rigid.Sleep();
    }
    private void OnCollisionEnter (Collision collision) {
        if (!screamed) {
            screamed = true;

            anim = GetComponent<Animator>();

            Scream.Play();
            anim.enabled = false;

            BoardZone.HideCircleMark();
            BoardZone.InteractOff();

            Blood.Play();

            OnHit.Invoke();
        }
    }

    public void StartBoarding (Vehicle car, Tile tile) {
        BoardTile = tile;
        BoardTileIndex = tile.Index;
        boardCar = car;
        boardCar.OnHit.AddListener(DecreasePayment);

        collider.enabled = false;

        StartCoroutine("startBoarding");
    }
    public void StartUnboarding (BoardingController zone, Tile tile) {
        UnboardTile = tile;
        unboardZone = zone;

        if (!gameObject.activeSelf)
            gameObject.SetActive(true);
        StartCoroutine("startUnboarding");
    }

    private IEnumerator startBoarding () {
        IsBoarding = true;
        IsBoarded = false;
        anim.SetBool("walk", true);
        SayHi.Play();
        OnStartBoarding.Invoke();

        boardCar.OnHit.AddListener(countHits);

        PathNode closestToPsngrNode = boardCar.WalkPath.GetClosestNode(transform.position);
        PathNode closestToBoardNode = boardCar.WalkPath.GetClosestNode(boardCar.BoardingPoint.transform.position);


        int walkDir = boardCar.WalkPath.GetShortestDirection(closestToPsngrNode, closestToBoardNode);

        Vector3 defaultPsngrNodePos = closestToPsngrNode.transform.localPosition;
        closestToPsngrNode.transform.position = transform.position;

        float startCoef = boardCar.WalkPath.GetCoefAtNode(closestToPsngrNode);
        float crntCoef = startCoef;
        float targetCoef = boardCar.WalkPath.GetCoefAtNode(closestToBoardNode);

        while (true) {
            //startCoef += Time.deltaTime * (BoardingSpeed / 10) * walkDir;
            crntCoef += Time.deltaTime * walkDir * (1 / BoardingTimeSec) * Mathf.Abs(targetCoef - startCoef);

            PathNode nextNode;
            Vector3 pos, lookDir;
            boardCar.WalkPath.GetWalkPoseByCoef(crntCoef, true, out pos, out lookDir, out nextNode);
            transform.position = pos;
            lookDir.y = 0;
            transform.transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(lookDir * walkDir), Time.deltaTime * 10);

            transform.localScale = Vector3.one * Mathf.Clamp(Vector3.Distance(closestToBoardNode.transform.position, transform.position), 0, 1);

            if (walkDir == 1) {
                if (crntCoef >= targetCoef) break;
            }
            else if (walkDir == -1) {
                if (crntCoef <= targetCoef) break;
            }

            //if (nextNode.PrevNode == closestToBoardNode)
            //    break;
            //if (Vector3.Distance(transform.position, nextNode.PrevNode.transform.position) < 0.2f)

            yield return 0;
        }

        closestToPsngrNode.transform.localPosition = defaultPsngrNodePos;

        IsBoarding = false;
        IsBoarded = true;

        transform.SetParent(null);
        anim.SetBool("walk", false);

        OnBoarded.Invoke();

        gameObject.SetActive(false);

        Debug.Log("passanger boarded");
    }
    private IEnumerator startUnboarding () {
        IsUnboarding = true;
        anim.SetBool("walk", true);
        OnStartUnboarding.Invoke();

        boardCar.OnHit.RemoveListener(countHits);

        PathNode closestToPsngrNode = boardCar.WalkPath.GetClosestNode(transform.position);
        PathNode closestToStopNode = boardCar.WalkPath.GetClosestNode(unboardZone.PassangerPosGlobal);


        int walkDir = boardCar.WalkPath.GetShortestDirection(closestToPsngrNode, closestToStopNode);

        Vector3 defaultClosestToStopNodePos = closestToStopNode.transform.localPosition;
        closestToStopNode.transform.position = unboardZone.PassangerPosGlobal;

        float startCoef = boardCar.WalkPath.GetCoefAtNode(closestToPsngrNode);
        float crntCoef = startCoef;
        float targetCoef = boardCar.WalkPath.GetCoefAtNode(closestToStopNode);

        while (true) {
            closestToStopNode.transform.position = unboardZone.PassangerPosGlobal;
            crntCoef += Time.deltaTime * walkDir * (1 / BoardingTimeSec) * Mathf.Abs(targetCoef - startCoef);

            PathNode nextNode;
            Vector3 pos, look;
            boardCar.WalkPath.GetWalkPoseByCoef(crntCoef, true, out pos, out look, out nextNode);
            transform.position = pos;
            look.y = 0;
            transform.rotation = Quaternion.LookRotation(look * walkDir, Vector3.up);

            transform.localScale = Vector3.one * Mathf.Clamp(Vector3.Distance(closestToPsngrNode.transform.position, transform.position), 0, 1);

            if (Vector3.Distance(transform.position, closestToStopNode.transform.position) < 0.2f)
                break;

            //if (walkDir == 1) {
            //    if (crntCoef >= targetCoef) break;
            //}
            //else if (walkDir == -1) {
            //    if (crntCoef <= targetCoef) break;
            //}

            yield return 0;
        }

        closestToStopNode.transform.localPosition = defaultClosestToStopNodePos;

        IsBoarded = false;
        IsUnboarding = false;
        transform.SetParent(unboardZone.transform);
        anim.SetBool("walk", false);

        boardCar.OnHit.RemoveListener(DecreasePayment);

        OnUnboarded.Invoke();

        Debug.Log("psngr unboarded");
    }

    private void countHits () {
        HitsWhileInCar++;
    }

    public void DecreasePayment () {
        CurrentPayment -= PaymentDecreasement;
        if (CurrentPayment < 0)
            CurrentPayment = 0;
    }

    public void SetPayment (float value) {
        CurrentPayment = value;
    }

    private void OnDrawGizmos () {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(ReplicaPntGlobal, 0.2f);
    }
}
