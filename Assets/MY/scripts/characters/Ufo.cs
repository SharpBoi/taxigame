﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ufo : MonoBehaviour {

    #region Fields
    [Header("Ufo config")]
    public UfoRay Ray;
    public float AttractForce = 600;
    public Vector3 AttractPointOffset = Vector3.up;

    [Header("Ufo movement")]
    public TileMapTarget Target;
    public MapGenerator Map;
    public float Height = 50;

    public float FollowSpeed = 1;
    public int FollowTileOffset = 1;


    private Vector3 dir;
    private float followSpeed;
    private Vector3 nextPos;
    private int lastTileInd = 0;
    #endregion

    #region PROPS
    public Tile CurrentTile { get; private set; }
    #endregion

    private void FixedUpdate() {
        updateAttraction();
        updateFollow();
    }

    private void updateAttraction() {
        for (int i = 0; i < Ray.RigidsInRay.Count; i++) {
            if (Ray.RigidsInRay[i] == null) {
                Ray.RigidsInRay.RemoveAt(i);
                continue;
            }

            dir = (transform.position + AttractPointOffset - Ray.RigidsInRay[i].transform.position).normalized;

            Ray.RigidsInRay[i].velocity = Vector3.zero;
            Ray.RigidsInRay[i].angularDrag = 0.5f;
            Ray.RigidsInRay[i].AddForce(dir * AttractForce, ForceMode.Acceleration);
        }
    }
    private void updateFollow() {

        Tile tile = null;
        if (Target.CurrentTile.Index - FollowTileOffset >= 0) {
            for (int i = 0; i < Map.Tiles.Length; i++) {
                if (Map.Tiles[i].Index == Target.CurrentTile.Index) {
                    if (i - FollowTileOffset >= 0)
                        tile = Map.Tiles[i - FollowTileOffset];
                    else
                        tile = Map.Tiles[0];
                    break;
                }
            }

            CurrentTile = tile;

            if (tile != null && Target.CurrentTile.Index > lastTileInd) {
                lastTileInd = Target.CurrentTile.Index;
                nextPos = tile.transform.position;
            }
            Vector3 pos = transform.position;
            pos = Vector3.Lerp(pos, nextPos, Time.deltaTime * FollowSpeed);
            pos.y = Height;
            transform.position = pos;
        }

        //if (Target.CurrentTile.Index - FollowTileOffset >= 0) {

        //    TileData tile = null;
        //    for (int i = 0; i < Target.DrivedTiles.Length; i++) {
        //        if(Target.DrivedTiles[i].Index == Target.CurrentTile.Index - FollowTileOffset) {
        //            tile = Target.DrivedTiles[i];
        //            break;
        //        }
        //    }


        //    if (Target.CurrentTile.Index > lastTileInd) {
        //        lastTileInd = Target.CurrentTile.Index;
        //        if (tile.Self != null)
        //            nextPos = Target.DrivedTiles[Target.CurrentTile.Index - FollowTileOffset].Self.transform.position;

        //    }
        //    Vector3 pos = transform.position;
        //    pos = Vector3.Lerp(pos, nextPos, Time.deltaTime * FollowSpeed);
        //    pos.y = Height;
        //    transform.position = pos;
        //}
    }

    private void OnDrawGizmos() {
    }
}
