﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UfoRay : MonoBehaviour {

    #region Fields
    [HideInInspector]
    public List<Rigidbody> RigidsInRay = new List<Rigidbody>();


    private Vector3 dir;
    #endregion

    #region Props
    #endregion

    private void OnTriggerEnter(Collider other)
    {
        Rigidbody rigid = other.GetComponent<Rigidbody>();
        if (rigid != null)
            if (RigidsInRay.Contains(rigid) == false)
                RigidsInRay.Add(rigid);
    }
    private void OnTriggerExit(Collider other)
    {
        RigidsInRay.Remove(other.GetComponent<Rigidbody>());
    }
}
