﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playlist : MonoBehaviour {

    #region FIELDS
    public List<AudioClip> BetweenClips = new List<AudioClip>();
    public List<AudioClip> Clips = new List<AudioClip>();
    public bool LoopAll = true;
    public bool PlayOnStart = true;
    public bool PlayRandomOnStart = true;


    private new AudioSource audio;
    private int clipInd = 0;
    private AudioClip prevClip = null;
    #endregion

    void Start () {
        audio = GetComponent<AudioSource>();

        if (PlayOnStart) {
            if (PlayRandomOnStart)
                Play(Random.Range(0, Clips.Count), true);

            else
                Play(0, true);
        }
    }

    private void FixedUpdate () {
        if (LoopAll && audio.isPlaying == false) {
            clipInd++;
            if (clipInd == Clips.Count) clipInd = 0;
            Play(clipInd, true);
        }
    }

    #region FUNCS
    public void Play (int index, bool playStationChange) {

        AudioClip clip = null;

        if (playStationChange) {
            if (Clips.Contains(prevClip) || prevClip == null) {
                prevClip = clip = BetweenClips[Random.Range(0, BetweenClips.Count)];
            }
            else {
                prevClip = clip = Clips[index];
            }
        }

        if (index < Clips.Count) {
            clipInd = index;

            //audio.clip = Clips[index];
            audio.clip = clip;
            audio.Play();
        }
        else
            Debug.LogError("Play list index out of bounds");
    }

    public void Pause () {
        audio.Pause();
    }
    public void Stop () {
        audio.Stop();
    }
    #endregion
}
