﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventSoundPlayer : MonoBehaviour {

    #region Fields
    public List<AudioClip> Clips = new List<AudioClip>();
    public PhysicsEvent physicsEvent = PhysicsEvent.Collision;
    public float Radius = 1f;
    public bool TriggerOnce = true;
    public GameObject SpecialTarget;



    #endregion

    #region  Props
    public bool IsTriggered { get; private set; }
    #endregion

    // Use this for initialization
    void Start () {
	}

    private void play()
    {
        if (TriggerOnce)
            IsTriggered = true;

        //if (!IsTriggered && !audio.isPlaying)
        //{
        //    audio.clip = Clips[Random.Range(0, Clips.Count)];
        //    audio.Play();
        //}
    }

    private void FixedUpdate()
    {
        if (physicsEvent == PhysicsEvent.Radius && IsTriggered == false)
            if (SpecialTarget != null)
            {
                if (Vector3.Distance(SpecialTarget.transform.position, transform.position) <= Radius)
                    play();
            }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (physicsEvent == PhysicsEvent.Collision && IsTriggered == false)
        {
            if (SpecialTarget == null)
                play();
            else if (SpecialTarget == collision.collider.gameObject)
                play();
        }
    }
}
