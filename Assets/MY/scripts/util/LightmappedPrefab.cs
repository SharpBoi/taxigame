﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightmappedPrefab : MonoBehaviour
{
    public MeshRenderer[] Renderers;

    private void Start()
    {
        GetRenderers();
    }

    [ContextMenu("Reset Renderers list")]
    public void GetRenderers()
    {
        Renderers = GetComponentsInChildren<MeshRenderer>();
    }

    public static T InstantiateWithLightmaps<T>(T _prefab, Transform _target) where T : LightmappedPrefab
    {
        T newInstance = Instantiate<T>(_prefab, _target);

        for (int n = 0; n < newInstance.Renderers.Length; n++)
        {
            newInstance.Renderers[n].lightmapIndex = _prefab.Renderers[n].lightmapIndex;
            newInstance.Renderers[n].lightmapScaleOffset = _prefab.Renderers[n].lightmapScaleOffset;

            ReflectionProbe[] prefabProbs = _prefab.GetComponentsInChildren<ReflectionProbe>();
            ReflectionProbe[] instProbs = newInstance.GetComponentsInChildren<ReflectionProbe>();

            for (int j = 0; j < prefabProbs.Length; j++)
                instProbs[j].bakedTexture = prefabProbs[j].bakedTexture;
        }

        return newInstance;
    }
}
