﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class VehicleCam : MonoBehaviour {

    public static List<VehicleCam> all = new List<VehicleCam>();


    // NEW BUT SHIT
    #region Fields
    public float FollowSpeed = 1;

    public Transform Target;

    public float OffsetPos;
    public float OffsetHeight;
    public float OffsetAngle;

    public float AngleX;

    public List<Collider> IgnoreColliders = new List<Collider>();
    public LayerMask IgnoreLayers;

    public Transform BonusPoint;


    private RaycastHit[] hits;
    private RaycastHit hit;
    private bool gotHit = false;

    private Vector3 prevTargetFwd;
    private Vector3 targetFwd;
    private float angle = 0;

    private int mask = 0;
    #endregion

    private void Start () {
        all.Add(this);

        mask = Util.GetResultMask(
            LayerMask.NameToLayer("Default"),
            LayerMask.NameToLayer("BotVehicle"));
    }
    private void OnDestroy () {
        all.Remove(this);
    }

    private void Update () {
        if (!Application.isPlaying) {
            poseSelf();
        }
    }

    private void FixedUpdate () {
        poseSelf();
    }

    private Vector3 absVec (Vector3 vec) {
        vec.x = Mathf.Abs(vec.x);
        vec.y = Mathf.Abs(vec.y);
        vec.z = Mathf.Abs(vec.z);

        return vec;
    }

    private void poseSelf () {
        prevTargetFwd = targetFwd;
        targetFwd = Target.forward;
        targetFwd.y = 0;

        float da = Vector3.SignedAngle(prevTargetFwd, targetFwd, Vector3.up);

        if (Mathf.Abs(da) < 180)
            angle += da;

        Vector3 pos = Target.position +
            new Vector3(
                Mathf.Cos((OffsetAngle - angle) * Mathf.Deg2Rad),
                0,
                Mathf.Sin((OffsetAngle - angle) * Mathf.Deg2Rad)) * OffsetPos +
            new Vector3(0, OffsetHeight, 0);

        hits = Physics.RaycastAll(Target.position, (pos - Target.position).normalized, Vector3.Distance(pos, Target.position), ~IgnoreLayers.value);

        float dist = 0;
        float minHitDist = 0;
        gotHit = false;
        for (int i = 0; i < hits.Length; i++)
            if (IgnoreColliders.Contains(hits[i].collider) == false) {
                dist = Vector3.Distance(hits[i].point, pos);
                if (minHitDist < dist) {
                    minHitDist = dist;
                    hit = hits[i];
                    gotHit = true;
                }
            }

        if (gotHit)
            transform.position = hit.point;
        else
            transform.position = Vector3.Lerp(transform.position, pos, Time.deltaTime * FollowSpeed);

        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(AngleX, angle + 90, 0), Time.deltaTime * FollowSpeed);
    }
}
