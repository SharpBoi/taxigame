﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPSTextUI : MonoBehaviour {
    
    private Text text;
    private float count = 0;

    private void Start()
    {
        text = GetComponent<Text>();

        StartCoroutine("StartFPSCounter");
    }

    IEnumerator StartFPSCounter()
    {
        while (true)
        {
            if (Time.timeScale == 1)
            {
                //yield return new WaitForSeconds(0.1f);
                count = (1 / Time.deltaTime);
                text.text = "FPS :" + (Mathf.Round(count));
            }
            yield return new WaitForSeconds(0.1f);
        }
    }
}