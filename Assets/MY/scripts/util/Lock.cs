﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class Lock : MonoBehaviour {

    public Vector3 Pos;
    public Vector3 Rot;
    public Vector3 Scale;

    public bool LockPos;
    public bool LockRot;
    public bool LockScale;

	void Start () {
        Pos = Vector3.zero;
        Rot = Vector3.zero;
        Scale = Vector3.one;
	}
	
	void Update () {
        if (LockPos)
            this.transform.position = Pos;

        if (LockRot)
            this.transform.rotation = Quaternion.Euler(Rot);

        if (LockScale)
            this.transform.localScale = Scale;
	}
}

#if UNITY_EDITOR
[CustomEditor(typeof(Lock))]
public class LockInspector : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Lock tgt = target as Lock;

        if (GUILayout.Button("Get pos"))
            tgt.Pos = tgt.transform.position;

        if (GUILayout.Button("Get rot"))
            tgt.Rot = tgt.transform.rotation.eulerAngles;

        if (GUILayout.Button("Get scale"))
            tgt.Scale = tgt.transform.localScale;
    }
}
#endif
