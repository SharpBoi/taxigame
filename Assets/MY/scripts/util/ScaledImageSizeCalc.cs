﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class ScaledImageSizeCalc : MonoBehaviour {

    #region Fields
    public Vector2 ScaledSize;
    public string TextSize;
    public string TextSizeRound;
    public float Ratio;


    private Image img;
    private SpriteRenderer sprite;
    #endregion

    private void Update()
    {
        if(img == null)
            img = GetComponent<Image>();
        if (sprite == null)
            sprite = GetComponent<SpriteRenderer>();

        if (img != null)
        {
            Vector2 min = img.rectTransform.anchorMin;
            Vector2 max = img.rectTransform.anchorMax;

            img.rectTransform.anchorMin = img.rectTransform.anchorMax = Vector2.one / 2;

            ScaledSize.x = img.mainTexture.width / (img.mainTexture.width / img.rectTransform.sizeDelta.x);
            ScaledSize.y = img.mainTexture.height / (img.mainTexture.height / img.rectTransform.sizeDelta.y);

            //ScaledSize.x = img.mainTexture.width / img.rectTransform.sizeDelta.x;
            //ScaledSize.y = img.mainTexture.height / img.rectTransform.sizeDelta.y;

            img.rectTransform.anchorMin = min;
            img.rectTransform.anchorMax = max;
        }
        else if (sprite != null)
        {
            ScaledSize.x = sprite.sprite.texture.width * sprite.transform.localScale.x;
            ScaledSize.y = sprite.sprite.texture.height * sprite.transform.localScale.y;

            //ScaledSize.x = sprite.texture.width / sprite.pixelsPerUnit;
            //ScaledSize.y = sprite.texture.height / sprite.pixelsPerUnit;
        }

        TextSize = ScaledSize.x + "x" + ScaledSize.y;
        TextSizeRound = Mathf.Round(ScaledSize.x) + "x" + Mathf.Round(ScaledSize.y);
        Ratio = ScaledSize.x / ScaledSize.y;
    }
}
