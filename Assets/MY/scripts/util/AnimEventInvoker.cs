﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimEventInvoker : MonoBehaviour {

    public UnityEvent Event;

    public void InvokeAnimEvent()
    {
        Event.Invoke();
    }
}
