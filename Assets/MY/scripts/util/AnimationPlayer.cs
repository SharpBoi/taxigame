﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationPlayer : MonoBehaviour {

    #region Fields
    public Animation anim;
    public float timescale = 1;
    public string clipName;
    public bool reverse = false;


    private float crntTime = 0;
    #endregion

    #region Props
    public bool IsPlaying { get; private set; }
    #endregion

    public void Play () {
        PlayAnimation(anim, clipName, timescale, reverse);
    }
    public void PlayAnimation(Animation anim, string clipName, float timescale, bool reverse)
    {
        this.anim = anim;
        this.timescale = timescale;
        this.clipName = clipName;
        this.reverse = reverse;

        if (!IsPlaying)
        {
            if (crntTime < 0) crntTime = 0;
            else if (crntTime > 1) crntTime = 1;
            
            StartCoroutine("playAnim");
        }
    }
    private IEnumerator playAnim()
    {
        IsPlaying = true;

        anim.Play();
        do
        {
            crntTime += Time.unscaledDeltaTime * timescale * (reverse ? -1 : 1);
            crntTime = Mathf.Clamp(crntTime, 0f, 1f);
            
            anim[clipName].normalizedTime = crntTime;

            yield return 0;
        }
        while (crntTime < 1 && crntTime > 0);

        IsPlaying = false;
    }
}
