﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialController : MonoBehaviour {

    #region FIELDS
    public List<MatField> Mats = new List<MatField>();
    #endregion

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

[System.Serializable]
public class MatField {

    public Material Mat;
    public MeshRenderer Renderer;

    public string FieldName;
    public float From;
    public float To;
}