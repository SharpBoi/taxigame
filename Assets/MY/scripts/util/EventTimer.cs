﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventTimer : MonoBehaviour {

    #region Fields
    public float TimeoutSec;

    public bool StartCountOnStart = false;

    public UnityEvent OnTimeout;

    private float crntTime = 0;
    #endregion

    #region Props
    public bool IsCountingNow { get; private set; }
    public float CurrentTime { get { return crntTime; } }
    #endregion

    private void Start()
    {
        if (StartCountOnStart)
            StartCount();
    }

    private void FixedUpdate()
    {
        if (IsCountingNow)
        {
            crntTime += Time.deltaTime;

            if(crntTime >= TimeoutSec)
            {
                OnTimeout.Invoke();
                StopCount();
            }
        }
    }

    public void StartCount()
    {
        IsCountingNow = true;
    }
    public void PauseCount()
    {
        IsCountingNow = false;
    }
    public void StopCount()
    {
        IsCountingNow = false;
        crntTime = 0;
    }
}
