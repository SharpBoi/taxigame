﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.IO;
using System.Text.RegularExpressions;

public class PostBuildProcessors {

    [PostProcessBuild(0)]
    public static void OnBuildComplete(BuildTarget target, string buildPath) {
        Debug.Log("My build complete");
    }

    [PostProcessBuild(1)]
    public static void OnPostProcessBuild(BuildTarget target, string targetPath) {
        var path = Path.Combine(targetPath, "Build/UnityLoader.js");
        var text = File.ReadAllText(path);
        text = Regex.Replace(text, @"compatibilityCheck:function\(e,t,r\)\{.+,Blobs:\{\},loadCode",
            "compatibilityCheck:function(e,t,r){t()},Blobs:{},loadCode");
        File.WriteAllText(path, text);
    }
}
#endif