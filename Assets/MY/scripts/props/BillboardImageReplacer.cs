﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BillboardImageReplacer : MonoBehaviour {

    #region Fields
    public bool ReplaceAllBillboardsOnStart = true;
    public bool HideAllBillboardsOnStart = false;

    public List<Sprite> HorBillboards = new List<Sprite>();
    public List<Sprite> VertBillboards = new List<Sprite>();
    public List<Sprite> StopBillboards = new List<Sprite>();

    #endregion

    void Start () {
        if (ReplaceAllBillboardsOnStart)
            ReplaceAllBillboards();

        if (HideAllBillboardsOnStart)
            HideAllBillboards();
	}

    public void ReplaceAllBillboards()
    {
        for (int i = 0; i < Billboard.Billboards.Count; i++)
        {
            if (Billboard.Billboards[i].IsHorBillboard)
                Billboard.Billboards[i].ReplaceImage(HorBillboards[Random.Range(0, HorBillboards.Count)]);

            else if (Billboard.Billboards[i].IsVertBillboard)
                Billboard.Billboards[i].ReplaceImage(VertBillboards[Random.Range(0, VertBillboards.Count)]);

            else if (Billboard.Billboards[i].IsBillboardOnStop)
                Billboard.Billboards[i].ReplaceImage(StopBillboards[Random.Range(0, StopBillboards.Count)]);
        }
    }

    public void HideAllBillboards()
    {
        for (int i = 0; i < Billboard.Billboards.Count; i++)
        {
            Billboard.Billboards[i].Active = false;
        }
    }
}
