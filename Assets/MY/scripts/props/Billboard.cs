﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour {

    #region Fields
    public static List<Billboard> Billboards = new List<Billboard>();


    public bool IsHorBillboard;
    public bool IsVertBillboard;
    public bool IsBillboardOnStop;

    public bool EnableToReplace;
    public bool ReplaceOnStart = true;
    public bool Active = true;


    private SpriteRenderer sprite;
    private bool active = true;

    #endregion
    
    void Start () {
        if (Billboards.Contains(this) == false)
            Billboards.Add(this);

        sprite = GetComponent<SpriteRenderer>();
	}

    public void ReplaceImage(Sprite tex)
    {
        sprite.sprite = tex;
    }

    private void LateUpdate()
    {
        if (Active)
            Enable();
        else
            Disable();
    }

    public void Enable()
    {
        if(!active)
        {
            active = true;
            gameObject.SetActive(true);
        }
    }
    public void Disable()
    {
        if (active)
        {
            active = false;
            gameObject.SetActive(false);
        }
    }
}
