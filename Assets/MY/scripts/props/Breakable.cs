﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

[RequireComponent(typeof(Rigidbody))]
public class Breakable : MonoBehaviour {

    #region Fields
    public bool TakeSelfAsSolidObject = true;
    public Transform SolidObject;
    public Transform BrokenObject;
    
    public float CriticalImpulse = 5000;
    public float CriticalRadius = 10;
    public GameObject SpecialTarget;

    [Header("What to do")]
    public bool AwakeRigid = false;
    public bool DestroySelf = true;

    [Header("Event")]
    public PhysicsEvent BreakMode = PhysicsEvent.Collision;


    private Rigidbody rigid;
    #endregion

    #region Props
    public bool IsBroken { get; private set; }
    #endregion

    // Use this for initialization
    void Start () {
        rigid = GetComponent<Rigidbody>();
        rigid.Sleep();

        if (TakeSelfAsSolidObject)
            SolidObject = transform;

        if (BrokenObject != null)
            BrokenObject.gameObject.SetActive(false);
	}

    private void FixedUpdate()
    {
        if(BreakMode == PhysicsEvent.Radius && IsBroken == false)
        {
            if (Vector3.Distance(SpecialTarget.transform.position, transform.position) < CriticalRadius)
                Break();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (BreakMode == PhysicsEvent.Collision && IsBroken == false)
        {
            if (SpecialTarget == null)
            {
                if (collision.impulse.magnitude >= CriticalImpulse && IsBroken == false)
                    Break();
            }
            else if (collision.gameObject == SpecialTarget && 
                collision.impulse.magnitude >= CriticalImpulse && IsBroken == false)
                    Break();
        }
    }

    public void Break()
    {
        if (IsBroken == false)
        {
            if (DestroySelf)
            {
                if (BrokenObject != null)
                {
                    BrokenObject.SetParent(SolidObject.parent);
                    BrokenObject.gameObject.SetActive(true);
                }
                if (SolidObject != null)
                    SolidObject.gameObject.SetActive(false);

            }
            else if (AwakeRigid)
            {
                rigid.WakeUp();
            }

            IsBroken = true;
        }
    }

    private void OnDrawGizmos()
    {
        if (BreakMode == PhysicsEvent.Radius)
            Gizmos.DrawWireSphere(transform.position, CriticalRadius);
    }
}