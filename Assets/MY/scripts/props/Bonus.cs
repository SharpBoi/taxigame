﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Bonus : MonoBehaviour {

    public static List<Bonus> all = new List<Bonus>();


    #region FIELDS
    [Header("Config")]
    public bool RandomizeTypeOnStart = true;
    public BonusType Type;
    public float ThrowMult = 1;
    public float ThrowHeight = 0.2f;
    public AudioSource PickupSound;
    [TagSelector]
    public string PickupCollisionTag;
    public List<Collider> DisableColliders;
    public float CanPickupAfterSEC = 1;

    [Header("Visual")]
    public Transform Visual;
    public float RotateSpeed = 1;
    public float SwingFreq = 1;
    public float SwingAmplitude = 0.01f;
    public float Height = 0;
    public ParticleSystem PickupPS;

    [Header("Events")]
    public UnityEvent OnPickup = new UnityEvent();


    private Rigidbody rigid;
    private float angle = 0;
    private bool pickable = false;
    #endregion

    private void OnLevelWasLoaded (int level) {
        all.Add(this);
    }
    private void Start () {
        if (RandomizeTypeOnStart) {
            Type = (BonusType)(UnityEngine.Random.Range(0, Enum.GetNames(typeof(BonusType)).Length));
        }

        init();

        StartCoroutine(countCanPickupAfterSEC());
    }
    private void init () {
        if (!rigid)
            rigid = GetComponent<Rigidbody>();
    }

    private void FixedUpdate () {
        Visual.transform.Rotate(Vector3.up, RotateSpeed);

        Vector3 pos = Visual.transform.localPosition;
        angle += Time.deltaTime;
        pos.y = Height + Mathf.Sin(angle * SwingFreq) * SwingAmplitude;
        Visual.transform.localPosition = pos;
    }

    #region FUNCS
    public void ThrowTo (Transform target) {
        init();

        Vector3 dir = (target.position - transform.position).normalized;
        float dist = Vector3.Distance(target.position, transform.position);

        Vector3 vi = dir;
        vi *= ThrowMult;
        vi.y = ThrowHeight;

        rigid.velocity = vi;
    }
    #endregion

    public void OnTriggerEnter (Collider collider) {
        if (collider.gameObject.tag == PickupCollisionTag && pickable) {
            OnPickup.Invoke();

            PickupSound.Play();
            if (Type == BonusType.Fuel)
                PickupPS.Play();

            Destroy(rigid);
            for (int i = 0; i < DisableColliders.Count; i++)
                if (collider.transform.parent != PickupPS.transform)
                    DisableColliders[i].enabled = false;

            Visual.gameObject.SetActive(false);

            StartCoroutine(countPickingUp(collider.transform));
        }
    }
    private IEnumerator countPickingUp (Transform picker) {

        while (PickupPS.isEmitting) {
            yield return 0;
        }

        while (PickupSound.isPlaying == false || PickupPS.particleCount != 0 || PickupSound.isPlaying) {
            //transform.position = picker.position;
            transform.position = VehicleCam.all[0].BonusPoint.transform.position;

            yield return 0;
        }

        Destroy(gameObject);
        Debug.Log("Bonus removed");
    }

    private IEnumerator countCanPickupAfterSEC () {
        float time = 0;
        while (time <= CanPickupAfterSEC) {
            time += Time.deltaTime;

            yield return 0;
        }

        pickable = true;
    }
}

//private float parabola (float h, float len ,float x) {
//    float ox = len / 2;
//    float sy = -h;
//    float oy = Mathf.Pow(len / 2, 2) * Mathf.Abs(sy);

//    return sy * Mathf.Pow(x - ox, 2) + oy;
//}
