﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StartLayout : MonoBehaviour {

    #region FIELDS
    [Header("Events")]
    public UnityEvent IfMobile;
    public UnityEvent IfDesktop;
    #endregion

    #region UNITY FUNCS
    private void Start () {
        if (Util.IsMobile)
            IfMobile.Invoke();

        else if (Util.IsDesktop)
            IfDesktop.Invoke();
    }

    #endregion
}
