﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameoverLayout : MonoBehaviour {

    #region Fields
    public CanvasGroup LayoutCanvGroup;

    public LeaderboardView Leaderboard;
    public MyKeyboard Keyboard;

    public TextMeshProUGUI YourResult;
    public TextMeshProUGUI YourPlace;

    public Button Top100;
    public Button Restart;

    public UnityEvent OnTop100;
    public UnityEvent OnRestart;
    #endregion

    void Start () {
        Top100.onClick.AddListener(OnTop100.Invoke);
        Restart.onClick.AddListener(OnRestart.Invoke);

        if (SystemInfo.operatingSystem.ToLower().IndexOf("windows") == 0 ||
            SystemInfo.operatingSystem.ToLower().IndexOf("mac") == 0) {

            Keyboard.gameObject.SetActive(false);
        }
	}

    public void SetScore(float score)
    {
        YourResult.text = score.ToString();
    }
    public void SetPlace(float place)
    {
        YourPlace.text = place.ToString();
    }

    public void Show(float score, float delaySec = 0) {

        LeaderboardNet.Instance.OnGetLeaderboardDone += (leaderboard) => {
            Leaderboard.SetLeaderboardContent(leaderboard);

            leaderboard.leaderboard = Util.StairSort(leaderboard.leaderboard, new Util.Func<UserScore, float>((us) => { return us.score; }));
            Array.Reverse(leaderboard.leaderboard);

            int place = leaderboard.leaderboard.Length + 1;
            for (int i = 0; i < leaderboard.leaderboard.Length; i++) {
                UserScore us = leaderboard.leaderboard[i];
                if (score >= us.score) {
                    place = i + 1;
                    break;
                }
            }

            SetScore(score);
            SetPlace(place);

            if (place < 100 && score > 0) {
                Leaderboard.AddNewUserScore(score);
                Leaderboard.Show();
                Keyboard.Show();
            }
        };
        LeaderboardNet.Instance.GetLeaderboard();

        gameObject.SetActive(true);

        StartCoroutine("show", delaySec);
    }
    private IEnumerator show(object delaySec) {
        float delay = (float)delaySec;
        float crntDelayTime = 0;

        LayoutCanvGroup.alpha = 0;

        while (true) {
            crntDelayTime += Time.fixedUnscaledDeltaTime * (1 / delay);
            LayoutCanvGroup.alpha = crntDelayTime;

            if (crntDelayTime >= 1)
                break;

            yield return 0;
        }

    }

    public void KeyboardInputToLeaderboardUser () {
        Leaderboard.LastUserScoreView.UserNickInput.text = Keyboard.InputText;
        Leaderboard.LastUserScoreView.SetToDefaultModeAndFillNicknameFromInput();
    }

    public void WWW_UpdateLeaderboard() {
        LeaderboardNet.Instance.UpdateLeaderboard(new UserScore() {
            name = Leaderboard.LastUserScoreView.Nickname,
            score = Leaderboard.LastUserScoreView.Score
        });
    }
}
