﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Labeler : MonoBehaviour {

    #region  FIELDS
    public float DescentTime = 1;
    public float WaitTime = 1;
    public float UpliftTime = 1;


    private List<RectTransform> labels = new List<RectTransform>();
    private RectTransform rectTransform;
    #endregion

    private void Start () {
        if (Application.isPlaying) {
            rectTransform = transform as RectTransform;

            FillLabels();
            PosAllLabelsOnDefault();

            HideAllLabelPrefabs();
        }
    }

    void Update () {
        if (Application.isPlaying == false) {
            FillLabels();
        }
    }

    public void FillLabels () {
        labels.Clear();
        for (int i = 0; i < transform.childCount; i++) {
            RectTransform child = transform.GetChild(i) as RectTransform;

            if (labels.Contains(child) == false)
                labels.Add(child);
        }
    }
    public void PosLabelOnDefault (RectTransform label) {
        Vector3 pos = label.transform.position;
        pos.y = transform.position.y + rectTransform.sizeDelta.y / 2 + label.sizeDelta.y / 2;
        label.transform.position = pos;
    }
    public void PosAllLabelsOnDefault () {
        for (int i = 0; i < labels.Count; i++) {
            PosLabelOnDefault(labels[i]);
        }
    }
    public void HideAllLabelPrefabs () {
        for (int i = 0; i < labels.Count; i++) {
            labels[i].gameObject.SetActive(false);
        }
    }

    public void ShowLabel (string name) {
        RectTransform labelPrefb = Util.GetSuitable(labels, (lbl) => { return lbl.name == name; });

        RectTransform labelInst = Instantiate(labelPrefb.gameObject, transform).transform as RectTransform;
        labelInst.position = labelPrefb.position;
        labelInst.localScale = Vector3.one;
        labelInst.gameObject.SetActive(true);
        labelInst.SetAsLastSibling();
        PosLabelOnDefault(labelInst);

        StartCoroutine(showLabel(labelInst));
    }
    private IEnumerator showLabel (RectTransform label) {
        Debug.Log(label);

        float targetY = rectTransform.position.y - rectTransform.sizeDelta.y / 2 + label.sizeDelta.y / 2;

        while (true) {

            Vector3 pos = label.transform.position;

            pos.y = Mathf.Lerp(pos.y, targetY, Time.deltaTime * DescentTime);

            label.transform.position = pos;

            if (Mathf.Round(pos.y) == Mathf.Round(targetY)) {
                StartCoroutine(waitLabel(label));
                break;
            }

            yield return 0;
        }

        yield return 0;
    }
    private IEnumerator waitLabel (RectTransform label) {
        float time = 0;
        while (time < WaitTime) {
            time += Time.deltaTime;

            yield return 0;
        }

        StartCoroutine(hideLabel(label));
    }
    private IEnumerator hideLabel (RectTransform label) {

        //float targetY = rectTransform.position.y + rectTransform.sizeDelta.y / 2 + label.sizeDelta.y / 2;

        //while (true) {

        //    Vector3 pos = label.transform.position;

        //    pos.y = Mathf.Lerp(pos.y, targetY, Time.deltaTime * DescentTime);

        //    label.transform.position = pos;

        //    if (Mathf.Round(pos.y) == Mathf.Round(targetY)) {
        //        break;
        //    }

        //    yield return 0;
        //}

        CanvasGroup cg = label.gameObject.GetComponent<CanvasGroup>();

        while (cg.alpha > 0) {
            cg.alpha -= Time.deltaTime;

            yield return 0;
        }
        cg.alpha = 0;

        Destroy(label.gameObject);
    }
}
