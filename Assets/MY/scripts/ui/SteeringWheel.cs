﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class SteeringWheel : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

    #region FIELDS
    public float DegreeLimits = 20;
    public float ZeroingSpeed = 10;
    public float Sense = 1;

    public UnityEvent OnDown;
    public UnityEvent OnHold;
    public UnityEvent OnUp;


    private int startTouchId = -1;
    private Vector2 startTouchPos;
    private Vector2 touchPos;
    private float crntDeg = 0;
    private float prevDeg = 0;
    private float deg = 0;
    private bool firstHold = true;
    #endregion

    #region PROPS
    public bool IsHolding { get; private set; }
    public float Degree { get { return deg; } }
    #endregion

    #region UNITY FUNCS

    void Start () {

    }

    private void FixedUpdate () {

        if (IsHolding) {
            OnHold.Invoke();

            bool touchFound = false;
            Touch crntTouch = getTouchById(startTouchId, out touchFound);

            if (touchFound) touchPos = crntTouch.position;
            else touchPos = Input.mousePosition;

            prevDeg = crntDeg;
            crntDeg = Mathf.Atan2(touchPos.y - transform.position.y, touchPos.x - transform.position.x) * Mathf.Rad2Deg;
            float dd = crntDeg - prevDeg;
            if (firstHold) dd = 0;

            if (Mathf.Abs(dd) < 180) deg += dd * Sense;

            // limiting rotation
            if (deg < -DegreeLimits) deg = -DegreeLimits;
            if (deg > DegreeLimits) deg = DegreeLimits;

            transform.rotation = Quaternion.Euler(0, 0, deg);
            firstHold = false;
        }
        else {
            firstHold = true;

            deg = Mathf.Lerp(deg, 0, Time.deltaTime * ZeroingSpeed);
            transform.rotation = Quaternion.Euler(0, 0, deg);
        }
    }

    public void OnPointerDown (PointerEventData eventData) {
        IsHolding = true;
        OnDown.Invoke();

        startTouchPos = eventData.position;

        for (int i = 0; i < Input.touches.Length; i++) {
            if (Input.touches[i].position == startTouchPos) {
                startTouchId = Input.touches[i].fingerId;
                break;
            }
        }
    }
    public void OnPointerUp (PointerEventData eventData) {
        IsHolding = false;
        OnUp.Invoke();
    }

    private void OnDrawGizmos () {
        Vector3 to = Vector3.zero;

        to.x = Mathf.Cos(DegreeLimits * Mathf.Deg2Rad);
        to.y = Mathf.Sin(DegreeLimits * Mathf.Deg2Rad);

        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + to * 100);

        to.x = Mathf.Cos(-DegreeLimits * Mathf.Deg2Rad);
        to.y = Mathf.Sin(-DegreeLimits * Mathf.Deg2Rad);

        Gizmos.DrawLine(transform.position, transform.position + to * 100);
    }
    #endregion

    #region FUNCS
    private Touch getTouchById (int touchId, out bool touchFound) {
        touchFound = false;
        for (int i = 0; i < Input.touches.Length; i++) {
            if (Input.touches[i].fingerId == startTouchId) {
                touchFound = true;
                return Input.touches[i];
            }
        }

        return default(Touch);
    }
    #endregion
}
