﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MyButton : MonoBehaviour/*, IPointerDownHandler, IPointerUpHandler*/ {

    #region FIELDS
    public GraphicRaycaster raycaster;


    public UnityEvent OnDown;
    public UnityEvent OnUp;
    public UnityEvent OnHold;

    
    private MyOS os;
    private PointerEventData ped;
    private List<RaycastResult> hits = new List<RaycastResult>();
    #endregion

    #region PROPS
    public bool IsHolding { get; private set; }
    #endregion

    void Start () {
        os = Util.GetOS();

        ped = new PointerEventData(EventSystem.current);
    }

    private void FixedUpdate () {
        bool found = false;
        if(os == MyOS.Android || os == MyOS.Ios) {

            for (int i = 0; i < Input.touches.Length; i++) {
                ped.position = Input.touches[i].position;

                hits.Clear();
                raycaster.Raycast(ped, hits);

                for (int j = 0; j < hits.Count; j++) {
                    if(hits[j].gameObject == this.gameObject) {
                        found = true;
                        break;
                    }
                }

                if (found) break;
            }
        }

        else if(os == MyOS.Mac || os == MyOS.Win) {
            if (Input.GetMouseButton(0)) {
                ped.position = Input.mousePosition;

                hits.Clear();
                raycaster.Raycast(ped, hits);

                for (int i = 0; i < hits.Count; i++) {
                    if (hits[i].gameObject == this.gameObject) {
                        found = true;
                        break;
                    }
                }
            }
        }

        if (found) {

            if (!IsHolding) {
                IsHolding = true;
                OnDown.Invoke();
            }
            else {
                OnHold.Invoke();
            }
        }
        else {
            if (IsHolding) {
                IsHolding = false;
                OnUp.Invoke();
            }
        }
    }

    #region FUNCS
    //public void OnPointerDown (PointerEventData eventData) {
    //    IsHolding = true;

    //    OnDown.Invoke();
    //}

    //public void OnPointerUp (PointerEventData eventData) {
    //    IsHolding = false;

    //    OnUp.Invoke();
    //}

    #endregion
}
