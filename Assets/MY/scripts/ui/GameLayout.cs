﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameLayout : MonoBehaviour {

    #region Fields

    [Header("Player")]
    public Vehicle Player;

    [Header("Labels")]
    public Text ScoreText;
    public GasBarController GasBar;

    [Header("Controls")]
    public MyButton BtnFwd;
    public MyButton BtnBwd;
    public SteeringWheel SteeringWheel;
    public Button BtnRespawn;
    public RectTransform RespawnCooldownRect;

    [Header("Hints")]
    public Labeler Labeler;
    //public Image SafeDrivingHint;
    //public Image DangerousDrivingHint;
    //public Image BoardHint;
    //public Image UnboardHint;
    public Image MoveDirectionWarning;
    public float DirectionWarningShowDelay = 1;
    public float DirectionWarningHideDelay = 0.5f;

    public Text GameOverCounter;

    public Transform PsngrHint;
    public TMP_Text PsngrPayValue;

    [Header("Replics")]
    public RectTransform PsngrBoardingReplica;
    public RectTransform PsngrGoodReplica;
    public RectTransform PsngrBadReplica;


    private float crntDirWarnShowDelay = 0;
    private bool isCounting = false;

    private bool psngrHintVisible = false;

    private Vector2 beginRespCooldownRectSize;
    #endregion

    #region Props
    public bool IsWarningShown { get; private set; }
    #endregion

    #region UNITY FUNCS
    private void Start () {

        if (Util.GetOS() == MyOS.Win || Util.GetOS() == MyOS.Mac) {
            BtnBwd.gameObject.SetActive(false);
            BtnFwd.gameObject.SetActive(false);
            SteeringWheel.gameObject.SetActive(false);
            BtnRespawn.gameObject.SetActive(false);
        }

        beginRespCooldownRectSize = RespawnCooldownRect.sizeDelta;
        RespawnCooldownRect.sizeDelta = new Vector2(RespawnCooldownRect.sizeDelta.x, 0);
    }

    private void FixedUpdate () {

        if (Player.PassangersCount > 0) {

            if (psngrHintVisible == false)
                StartCoroutine(showPsngrHint());

            Vector3 screenPos = Camera.main.WorldToScreenPoint(Player.Passangers[0].ReplicaPntGlobal);

            PsngrPayValue.text = Player.Passangers[0].CurrentPayment.ToString();
            PsngrHint.transform.position = Vector3.Lerp(PsngrHint.transform.position, screenPos, Time.deltaTime * 10);
        }
        else if (psngrHintVisible) {
            StartCoroutine(hidePsngrHint());

        }
    }
    #endregion

    #region FUNCS
    public void SetGasBar (float value) {
        GasBar.FillState = value;
    }

    // Show hints
    public void ShowSafeDrive () {
        Labeler.ShowLabel("nice_drive");
    }
    public void ShowDangerousDrive () {
        Labeler.ShowLabel("bad_drive");
    }
    public void ShowKillPsngr () {
        Labeler.ShowLabel("kill_psngr");
    }
    public void ShowCleverDrive () {
        Labeler.ShowLabel("clever_drive");
    }
    public void ShowBoardPsngr () {
        Labeler.ShowLabel("take_psngr");
    }
    public void ShowUnboardPsngr () {
        Labeler.ShowLabel("unboard_psngr");
    }

    // passanger replica
    public void ShowBoardingReplica (Passanger psngr) {
        PsngrBoardingReplica.gameObject.SetActive(true);

        StartCoroutine(updateReplicaPos(PsngrBoardingReplica, psngr));
    }
    public void ShowGoodUnboardReplica (Passanger psngr) {
        PsngrGoodReplica.gameObject.SetActive(true);

        StartCoroutine(updateReplicaPos(PsngrGoodReplica, psngr));
    }
    public void ShowBadUnboardReplica (Passanger psngr) {
        PsngrBadReplica.gameObject.SetActive(true);

        StartCoroutine(updateReplicaPos(PsngrBadReplica, psngr));
    }
    private IEnumerator updateReplicaPos (RectTransform replica, Passanger psngr) {

        while (psngr.IsBoarding || psngr.IsUnboarding) {

            Vector3 screenpos = Camera.main.WorldToScreenPoint(psngr.ReplicaPntGlobal);
            replica.transform.position = Vector3.Lerp(replica.transform.position, screenpos, Time.deltaTime * 10);

            yield return 0;
        }

        replica.gameObject.SetActive(false);
    }

    public void SetWrongDirectionVisible (bool visible) {
        MoveDirectionWarning.gameObject.SetActive(visible);
    }
    public void StartWarnShowCounting () {
        if (isCounting == false) {
            isCounting = true;
            crntDirWarnShowDelay = 0;

            StartCoroutine("countShowWarn");
        }
    }
    public void HideDirectionWarning () {
        if (IsWarningShown || isCounting) {
            isCounting = false;
            IsWarningShown = false;

            MoveDirectionWarning.gameObject.SetActive(false);
        }
    }
    private IEnumerator countShowWarn () {
        while (isCounting && IsWarningShown == false) {
            crntDirWarnShowDelay += Time.deltaTime;

            if (crntDirWarnShowDelay >= DirectionWarningShowDelay) {
                IsWarningShown = true;
                isCounting = false;

                MoveDirectionWarning.gameObject.SetActive(true);
            }

            yield return 0;
        }
    }

    public void SetLooseCounterVis (bool visible) {
        if (GameOverCounter.gameObject.activeSelf != visible)
            GameOverCounter.gameObject.SetActive(visible);
    }
    public void UpdateLooseCounter (float time) {
        GameOverCounter.text = ((int)time + " : " + Mathf.Round((time - (int)time) * 1000));
    }

    public void SetScore (float score) {
        string scoreText = Mathf.Round(score).ToString();

        while (scoreText.Length < 4)
            scoreText = "0" + scoreText;

        ScoreText.text = scoreText;
    }

    public void SetCooldownRect (float coef) {
        Vector2 sz = RespawnCooldownRect.sizeDelta;
        sz.y = beginRespCooldownRectSize.y * coef;
        RespawnCooldownRect.sizeDelta = sz;
    }

    public void HidePsngrHint () {
        PsngrHint.gameObject.SetActive(false);
    }
    private IEnumerator showPsngrHint () {

        psngrHintVisible = true;
        PsngrHint.gameObject.SetActive(true);
        CanvasGroup cg = PsngrHint.GetComponent<CanvasGroup>();
        cg.alpha = 0;
        while (cg.alpha < 1) {
            cg.alpha += Time.deltaTime;
            yield return 0;
        }

        yield return 0;
    }
    private IEnumerator hidePsngrHint () {

        psngrHintVisible = false;
        PsngrHint.gameObject.SetActive(true);
        CanvasGroup cg = PsngrHint.GetComponent<CanvasGroup>();
        cg.alpha = 1;
        while (cg.alpha > 0) {
            cg.alpha -= Time.deltaTime;
            yield return 0;
        }
        PsngrHint.gameObject.SetActive(false);

        yield return 0;
    }
    #endregion
}
