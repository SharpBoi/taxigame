﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GasBarController : MonoBehaviour {

    public RectTransform Indicator;

    public float FillState = 1;


    private Image indicatorImg;
    private float h, s, v;
    private float initWidth;

	// Use this for initialization
	void Start () {
        initWidth = Indicator.sizeDelta.x;
        indicatorImg = Indicator.GetComponent<Image>();
        Color.RGBToHSV(indicatorImg.color, out h, out s, out v);
	}
	
	// Update is called once per frame
	void Update () {

        if (FillState > 1) FillState = 1;
        if (FillState < 0) FillState = 0;

        indicatorImg.color = Color.HSVToRGB(h * FillState, s + ((1 - s) * (1 - FillState)), v);

        Vector2 sz = Indicator.sizeDelta;
        sz.x = initWidth * FillState;
        Indicator.sizeDelta = sz;
	}
}
