﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UserScoreView : MonoBehaviour {

    #region Fields
    public Image BackImage;
    public TextMeshProUGUI UserPlace;
    public TextMeshProUGUI UserScore;
    public TextMeshProUGUI UserNickname;
    public TMP_InputField UserNickInput;

    public Button SetNick;
    public RectTransform SetNickPoint;

    public Color EditModeColor;
    public Color DefaultColor;

    public LeaderboardView ParentLB;

#if UNITY_EDITOR
    [ReadOnly]
#endif
    public int Index = 0;
    #endregion

    #region Props
    public int Place { get { return int.Parse(UserPlace.text); } }
    public string Nickname { get { return UserNickname.text; } set { UserNickname.text = value; } }
    public float Score { get { return float.Parse(UserScore.text); } set { UserScore.text = value.ToString(); } }

    public bool IsEditMode { get; private set; }
    #endregion

    #region Funcs
    public void Start() {

        UserNickInput.onEndEdit.AddListener(new UnityEngine.Events.UnityAction<string>((smbl) => {

            Debug.Log(smbl);

        }));

    }
    public void Update() {
        SetNick.transform.position = SetNickPoint.position;
    }

    private void OnEnable() {
        //if (IsEditMode)
        //    SetNick.gameObject.SetActive(true);
    }
    private void OnDisable() {
        //SetNick.gameObject.SetActive(false);
    }

    public void SetPlace(int place) { UserPlace.text = place.ToString(); }
    public void SetNickname(string nick) { Nickname = nick; }
    public void SetNicknameFromInput() {
        SetNickname(UserNickInput.text);
    }
    public void SetScore(float score) { Score = score; }
    public void SetIndex(int i) { Index = i; }

    public void SetEditMode(bool value) {
        IsEditMode = value;
        if (value) {
            SetNick.gameObject.SetActive(true);
            //SetNick.transform.SetParent(ParentLB.transform.parent);
            //SetNick.transform.SetSiblingIndex(ParentLB.transform.GetSiblingIndex() + 1);

            BackImage.color = EditModeColor;

            UserNickInput.gameObject.SetActive(true);
            UserNickname.gameObject.SetActive(false);
        }
        else {
            SetNick.gameObject.SetActive(false);

            BackImage.color = DefaultColor;

            UserNickInput.gameObject.SetActive(false);
            UserNickname.gameObject.SetActive(true);
        }
    }

    public void SetColor(Color color) {
        DefaultColor = color;
        BackImage.color = color;
    }

    public void SetToDefaultModeAndFillNicknameFromInput() {
        if (UserNickInput.text != "") {
            SetNicknameFromInput();
            SetEditMode(false);
        }
    }
    #endregion
}

[System.Serializable]
public class UserScore {
    public string name;
    public float score;
}