﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LeaderboardView : MonoBehaviour {

    #region Fields
    public ScrollRect ScrollView;
    public RectTransform Container;

    public UserScoreView ContentExample;
    public Color EvenColor = Color.white;
    public Color DefaultColor = Color.white;

    public int PageStep = 10;
    public TextMeshProUGUI PageText;

    public bool TESTAddUser;


    private List<UserScoreView> userViews = new List<UserScoreView>();
    private Animation anim;
    private bool showed;
    private UserScoreView lastUser;

    private bool inited = false;
    #endregion

    #region Props
    public int ScrollElementIndex { get { return userViews.Count - (int)(ScrollView.verticalNormalizedPosition * userViews.Count); } }
    public UserScoreView ScrollElement { get { return userViews[ScrollElementIndex]; } }

    public int ScrollPage { get { return (ScrollElementIndex / PageStep + 1) * PageStep; } }

    public UserScoreView LastUserScoreView { get { return lastUser; } }
    #endregion

    void Start () {
        init();
	}

    private void FixedUpdate() {
        if (TESTAddUser) {
            TESTAddUser = false;

            AddNewUserScore(123);
        }
    }

    private void init()
    {
        if (!inited)
        {
            inited = true;

            anim = GetComponent<Animation>();
            ContentExample.gameObject.SetActive(false);
        }
    }
    
    // visualization stuff
    /// <summary>
    /// Заполняет лидерборду по заданному массиву игроков
    /// </summary>
    /// <param name="leadbrd"></param>
    public void SetLeaderboardContent(Leaderboard leadbrd)
    {
        List<UserScore> users = new List<UserScore>();
        users.AddRange(Util.StairSort(leadbrd.leaderboard, new Util.Func<UserScore, float>((arg) => { return arg.score; })));
        users.Reverse();

        //clear childs
        userViews.Clear();
        for (int i = 0; i < Container.childCount; i++)
            Destroy(Container.GetChild(i).gameObject);

        // fill with new UserScore views
        for (int i = 0; i < users.Count; i++)
        {
            GameObject instGO = Instantiate(ContentExample.gameObject);
            UserScoreView inst = instGO.GetComponent<UserScoreView>();

            inst.SetPlace(i + 1);
            inst.SetNickname(users[i].name);
            inst.SetScore(users[i].score);
            inst.SetEditMode(false);

            if (i % 2 != 0)
                inst.SetColor(EvenColor);

            instGO.SetActive(true);
            instGO.transform.SetParent(Container);
            instGO.transform.localScale = Vector3.one;

            userViews.Add(inst);
        }

        Debug.Log("leaderboard view filled");
    }
    /// <summary>
    /// Добавляет в лидерборд новую запись, готовую для изменения пользователем
    /// </summary>
    /// <param name="score"></param>
    public void AddNewUserScore(float score)
    {
        GameObject instGO = Instantiate(ContentExample.gameObject);
        UserScoreView inst = instGO.GetComponent<UserScoreView>();

        instGO.SetActive(true);
        instGO.transform.SetParent(Container);
        instGO.transform.localScale = Vector3.one;

        inst.SetScore(score);

        userViews.Add(inst);

        SortUsersByScore();

        inst.SetEditMode(true);

        ScrollToUserIndex(inst.Index);

        lastUser = inst;
    }

    public void SortUsersByScore()
    {
        userViews = new List<UserScoreView>(Util.StairSort(userViews, new Util.Func<UserScoreView, float>((arg) => { return arg.Score; })));
        userViews.Reverse();

        for (int i = 0; i < userViews.Count; i++)
        {
            userViews[i].SetIndex(i);
            userViews[i].SetPlace(i + 1);
            userViews[i].transform.SetSiblingIndex(i);

            if (i % 2 != 0)
                userViews[i].SetColor(EvenColor);
            else
                userViews[i].SetColor(DefaultColor);
        }
    }

    // scroll stuff
    public void OnScroll()
    {
        PageText.text = ScrollPage.ToString();
    }
    public void ScrollNextPage()
    {
        ScrollView.verticalNormalizedPosition -= ((userViews[0].transform as RectTransform).rect.height / Container.rect.height) * PageStep;

        //ScrollView.verticalNormalizedPosition -= 1f / Mathf.Round(userViews.Count / PageStep);
    }
    public void ScrollPrevPage()
    {
        ScrollView.verticalNormalizedPosition += ((userViews[0].transform as RectTransform).rect.height / Container.rect.height) * PageStep;

        //ScrollView.verticalNormalizedPosition += 1f / Mathf.Round(userViews.Count / PageStep);
    }

    private IEnumerator scroll(object elementIndex)
    {
        yield return new WaitForEndOfFrame();

        UserScoreView element = userViews[(int)elementIndex];

        Vector2 size = (element.transform as RectTransform).sizeDelta;

        float height = 0;
        float aboveHeightSum = 0;
        bool aboveHeightCalced = false;
        for (int i = 0; i < userViews.Count; i++)
        {
            height += (userViews[i].transform as RectTransform).sizeDelta.y;

            if (userViews[i] != element && aboveHeightCalced == false)
                aboveHeightSum += (userViews[i].transform as RectTransform).sizeDelta.y;
            else
                aboveHeightCalced = true;
        }
        //(ScrollView.transform as RectTransform).rect.height / 2
        ScrollView.verticalNormalizedPosition = 1 - ((aboveHeightSum) / height);

        //Canvas.ForceUpdateCanvases();
    }
    public void ScrollToUserElement(UserScoreView element)
    {
        for (int i = 0; i < userViews.Count; i++)
            if (userViews[i] == element)
            {
                StartCoroutine("scroll", i);
                return;
            }
    }
    public void ScrollToUserIndex(int index)
    {
        StartCoroutine("scroll", index);
    }

    public void Show()
    {
        if (!showed)
        {
            anim[anim.clip.name].speed = 1;
            Util.PlayAnimation(anim, anim.clip.name, 1, false);

            if (lastUser != null)
                lastUser.UserNickInput.Select();

            showed = true;
        }
    }
    public void Hide()
    {
        if (showed)
        {
            anim[anim.clip.name].speed = -1;
            Util.PlayAnimation(anim, anim.clip.name, 1, true);

            showed = false;
        }
    }
    public void SwitchShowHide()
    {
        if (showed)
            Hide();
        else
            Show();
    }

    public void WWW_GetUsersFromServer() {
        LeaderboardNet.Instance.OnGetLeaderboardDone += (leaderboard) => {

            leaderboard.leaderboard = Util.StairSort(leaderboard.leaderboard, new Util.Func<UserScore, float>((us) => { return us.score; }));
            Array.Reverse(leaderboard.leaderboard);

            SetLeaderboardContent(leaderboard);
        };
        LeaderboardNet.Instance.GetLeaderboard();
    }
}

[System.Serializable]
public class Leaderboard
{
    public UserScore[] leaderboard;
}
