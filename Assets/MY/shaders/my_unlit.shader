﻿Shader "Mobile/Unlit Color With Global Mask (Supports Lightmap)" {
Properties {
	_Color("Color", Color) = (0.5,0.5,0.5,1.0)
	_LMLightColor("Lightmap Light Color", Color) = (1,1,1,1)
	_LMShadeColor("Lightmap Shade Color", Color) = (0,0,0,1)
	_FadeStart("Fade start range", Float) = 10.0
	_FadeEnd("Fade end range", Float) = 15.0
}

SubShader {
	Tags { "RenderType"="Transparent" "Queue"="Transparent" }
	LOD 100
	Blend SrcAlpha OneMinusSrcAlpha
	Lighting Off
	ZTest Less
	
	Pass {
		//Tags { "LIGHTMODE"="VertexLMRGBM" }
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma target 2.0
		#include "UnityCG.cginc"

			// uniforms
			// vertex shader input data
			struct appdata
			{
				float4 pos : POSITION;
				float3 uv1 : TEXCOORD1;
			};

			// vertex-to-fragment interpolators
			struct v2f
			{
				fixed4 color : COLOR0;
				float2 uv0 : TEXCOORD0;
				float4 pos : SV_POSITION;
			};

			half _FadeStart;
			half _FadeEnd;

			// vertex shader
			v2f vert(appdata IN)
			{
				v2f o;

				half4 color = half4(0, 0, 0, 1.1);
				float3 eyePos = UnityObjectToViewPos(IN.pos);
				half3 viewDir = 0.0;
				o.color = saturate(color);

				half3 range = abs(mul(unity_ObjectToWorld, IN.pos).xyz);
				if (range.x > 40.0)
					o.color.a = 1.0;
				else
					o.color.a = 1.0 - (range.z - _FadeStart) / (_FadeEnd - _FadeStart);

				o.uv0 = IN.uv1.xy * unity_LightmapST.xy + unity_LightmapST.zw;

				o.pos = UnityObjectToClipPos(IN.pos);
				return o;
			}

			fixed4 _Color;
			fixed4 _LMLightColor;
			fixed4 _LMShadeColor;

			// textures

			// fragment shader
			fixed4 frag(v2f IN) : SV_Target
			{
				fixed4 col;

				// Fetch lightmap
				half4 bakedColorTex = lerp(_LMShadeColor,_LMLightColor,  UNITY_SAMPLE_TEX2D(unity_Lightmap, IN.uv0.xy));
				col.rgb = DecodeLightmap(bakedColorTex);

				// Fetch color texture
				col.rgb = col.rgb * _Color.rgb;
				col.a = IN.color.a;

				return col;
			}

		ENDCG
	}
}
}



