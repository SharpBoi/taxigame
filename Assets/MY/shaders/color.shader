﻿ Shader "Custom/Mike's Shader" {
     Properties {
         _MainTex ("Base (RGB)", 2D) = "white" {}
         _Color ("AlphaColor", Color) = (0,0,0,1)
		 _Alpha("Alpha", Range(0, 1)) = 1
     }
     SubShader {
         Tags { "RenderType"="transparent" }
         LOD 200
         
         CGPROGRAM
         #pragma surface surf Lambert alpha
 
         sampler2D _MainTex;
 
         struct Input {
             float2 uv_MainTex;
         };
         float4 _Color;
		 half _Alpha;
 
         void surf (Input IN, inout SurfaceOutput o) {
             half4 c = tex2D (_MainTex, IN.uv_MainTex);
             o.Albedo = _Color;
			 o.Alpha = _Alpha;
         }
         ENDCG
     } 
     FallBack "Diffuse"
 }